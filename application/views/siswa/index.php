<div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
        <p class="mb-4"><strong>Selamat Datang..</strong> <br>Website ini digunakan untuk keperluan Peserta Didik dalam pembalajaran ataupun kegiatan lainnya.</p>
        <!-- DataTales Example --> <br>
    
    <div class="row">
        <div class="col-lg-12">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        
                        <div class="card-body">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr align="center">
                                            <th scope="col">No</th>
                                            <th scope="col">Hari</th>
                                            <th width="140">Jam</th>
                                            <th scope="col">Mata Pelajaran</th>
                                            <th scope="col">Ruangan</th>
                                            <th scope="col">Tutor / Guru</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1;
                                        foreach($jadwal as $jdwl) : ?>
                                            <tr>
                                                <td align="center"><?= $no++; ?></td>
                                                <td align="center"><?= $jdwl->hari;?></td>
                                                <td align="center"><?= $jdwl->jam_awl; ?> - <?= $jdwl->jam_akhr; ?></td>
                                                <td><?= $jdwl->nama_mapel; ?></td>
                                                <td><?= $jdwl->nama_ruangan; ?></td>
                                                <td><?= $jdwl->nama_guru; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                        </div>
                    </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->