<!DOCTYPE html>
<html lang="en">
<head>
    <title>Pendaftaran</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/jpg" sizes="16x16" href="<?= base_url('assets/app-assets/img/'); ?>logo.png">
    <link rel="stylesheet" href="<?= base_url('assets/app-assets/daftar/') ?>bootstrap/css/bootstrap.min.css">
    <script src="<?= base_url('assets/app-assets/daftar/') ?>jquery/jquery-3.4.1.min.js"></script>
    <script src="<?= base_url('assets/app-assets/daftar/') ?>bootstrap/js/bootstrap.min.js"></script>
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?= base_url('assets/app-assets/'); ?>sweetalert2/dist/notiflix-2.6.0.min.css">
    <script src="<?= base_url('assets/app-assets/'); ?>sweetalert2/dist/notiflix-2.6.0.min.js"></script>
    <script src="<?= base_url('assets/app-assets/'); ?>sweetalert2/dist/sweetalert2.all.js"></script> 

</head>
<body>
    <?= $this->session->flashdata('pesan'); ?>
    <div class="container p-3 my-3 border">
    <h1 class="text-center">Form Pendaftaran Calon Peserta Didik Baru</h1>
        <form id="form" method="post" action="<?= base_url('daftar/prosesSubmit'); ?>">
            <div class="alert alert-primary">
                <strong>Data Diri</strong>
            </div>
            <div class="row">
                <div class="col-sm-7">

                    <div class="form-group">
                        <label>Nama Lengkap:</label>
                        <input type="text" name="namalengkap" class="form-control" placeholder="Masukan Nama Lengkap" value="" required>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Nomor Identitas (NIK):</label>
                        <!-- <input type="text" name="nik" class="form-control" placeholder="Masukan Nomor NIK KTP atau KK"> -->
                        <input type="number" oninput="this.value=this.value.slice(0,this.maxLength)" class="form-control" name="nik" min="0" maxlength="16" placeholder="Masukan Nomor NIK KTP atau KK" required>
                        <?= form_error('nik', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tempat Lahir:</label>
                        <input type="text" name="tempat_lahir" class="form-control" placeholder="Masukan Tempat Lahir" required>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Tanggal Lahir:</label>
                        <input type="date" name="tanggal_lahir" class="form-control" required>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Jenis Kelamin:</label>
                        <select class="form-control" name="jk" required>
                            <option>Pilih</option>
                            <option value="Laki-Laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Kewarganegaraan:</label>
                        <select class="form-control" name="kewarganegaraan" required>
                            <option>Pilih</option>
                            <option value="WNI">Warga Negara Indonesia</option>
                            <option value="WNA">Warga Negara Asing</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Agama:</label>
                        <select class="form-control" name="agama" required>
                            <option>Pilih</option>
                            <option value="Islam">Islam</option>
                            <option value="Kristen">Kristen</option>
                            <option value="Katolik">Katolik</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Budha">Budha</option>
                            <option value="Lainnya">Lainnya</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Email:</label>
                        <input type="email" name="email" class="form-control" placeholder="Masukan Email Aktif">
                        <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>No Telp:</label>
                        <!-- <input type="text" name="no_telp" class="form-control" placeholder="Masukan No Telp" required> -->
                        <input type="number" oninput="this.value=this.value.slice(0,this.maxLength)" class="form-control" name="no_telp" min="0" maxlength="14" placeholder="Masukan No Telp" required>
                    </div>
                </div>
            </div>
            <div class="alert alert-primary">
                <strong>Data Alamat Asal</strong>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Alamat:</label>
                        <textarea class="form-control" name="alamat" rows="2" id="alamat" required></textarea>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Kode Pos:</label>
                        <!-- <input type="text" name="kode_pos" class="form-control" placeholder="Kode Pos"> -->
                        <input type="number" oninput="this.value=this.value.slice(0,this.maxLength)" class="form-control" name="kode_pos" min="0" maxlength="5" placeholder="Kode Pos" required>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Provinsi:</label>
                        <input type="text" name="provinsi" class="form-control" required>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Kabupaten:</label>
                        <input type="text" name="kabupaten" class="form-control" required>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Kecamatan:</label>
                        <input type="text" name="kecamatan" class="form-control" required>
                    </div>
                </div>

            </div>
            <div class="alert alert-primary">
                <strong>Pemilihan Program PKBM</strong>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PAKET : </label>
                        <select class="form-control" name="nama_program" required>
                            <option value="">-- Pilih --</option>
                                <?php foreach($program as $data) { ?>
                                    <option value="<?= $data['kd_program']; ?>"><?= $data['nama_program'];  ?></option>
                                <?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm">
                    <button type="submit" name="Submit" id="Submit" class="btn btn-primary">Daftar</button>
                    <button type="reset" class="btn btn-secondary">Reset</button>
                    <a href="<?= base_url('landing'); ?>" class="btn btn-dark float-right">Kembali</a>
                </div>
            </div>
        </form>
    </div>
</body>
</html>