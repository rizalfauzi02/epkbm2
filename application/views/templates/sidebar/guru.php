<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
    <div class="sidebar-brand-icon">
        <!-- <i class="fas fa-laugh-wink"></i> -->
        <!-- <i class="fas fa-school"></i> -->
        <img src="<?= base_url('assets/app-assets/'); ?>img/logo.png" alt="" id="img-bg" class="img-thumbnail">
    </div>
    <div class="sidebar-brand-text mx-2">PKBM Ciptamekar</div>
</a>

<!-- Divider -->
<!-- <hr class="sidebar-divider my-0"> -->
<li class="nav-item <?= $menu['Absensi']; ?>">
    <a class="nav-link" href="<?= base_url('guru')?>">
        <i class="fas fa-pen"></i>
        <span>Absensi</span></a>
</li>

<!-- Divider -->
    <hr class="sidebar-divider">

<li class="nav-item <?= $menu['Nilai']; ?>">
    <a class="nav-link" href="<?= base_url('nilai')?>">
        <i class="fas fa-star"></i>
        <span>Nilai</span></a>
</li>
<!-- Divider -->
    <hr class="sidebar-divider">

<li class="nav-item <?= $menu_lapor['laporan_menu']; ?>">
            <a class="nav-link" href="#" data-toggle="collapse" data-target="#laporan"
                    aria-expanded="true" aria-controls="laporan">
                <i class="fas fa-fw fa-book"></i>
                <span>Laporan (dev)</span>
            </a>
    <div id="laporan" class="collapse <?= $menu_lapor['laporan_show']; ?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Laporan:</h6>
            <a class="collapse-item <?= $menu_lapor['laporan_absen']; ?>" href="<?= base_url('guru/absensiFil'); ?>">Rekap Absensi</a>
            <a class="collapse-item <?= $menu_lapor['laporan_nilai']; ?>" href="<?= base_url('guru/NilaiFil'); ?>">Rekap Nilai</a>
        </div>
    </div>
</li>

<!-- Divider -->
    <hr class="sidebar-divider">

<!-- !-- Nav Item - USER -->
<li class="nav-item <?= $menu['Profile']; ?>">
    <a class="nav-link" href="<?= base_url('guru/profile')?>">
        <i class="fas fa-user"></i>
        <span>Profile</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- !-- Nav Item - USER -->
<!-- <li class="nav-item <?= $menu['Absensidev']; ?>">
    <a class="nav-link" href="<?= base_url('employee/attendance')?>">
        <i class="fas fa-user"></i>
        <span>Absensi (dev)</span></a>
</li> -->

<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- <li class="nav-item">
    <a class="nav-link" href="<?= base_url('tbhorror') ?>">
        <i class="fas fa-chevron-circle-right"></i>
        <span>Horror</span></a>
</li> -->
<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">


<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>
<!-- End of Sidebar -->