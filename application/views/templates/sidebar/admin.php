<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
    <div class="sidebar-brand-icon">
        <!-- <i class="fas fa-laugh-wink"></i> -->
        <!-- <i class="fas fa-school"></i> -->
        <img src="<?= base_url('assets/app-assets/'); ?>img/logo.png" alt="" id="img-bg" class="img-thumbnail">
    </div>
    <div class="sidebar-brand-text mx-2">PKBM Ciptamekar</div>
</a>

<!-- Divider -->
<!-- <hr class="sidebar-divider my-0"> -->

<!-- Nav Item - Users -->
<li class="nav-item <?= $menu['Dashboard']; ?>">
    <a class="nav-link" href="<?= base_url('admin')?>">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<li class="nav-item <?= $menu_drop['user_menu']; ?>">
            <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-fw fa-user"></i>
                <span>User</span>
            </a>
    <div id="collapseUtilities" class="collapse <?= $menu_drop['user_show']; ?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Menu:</h6>
            <a class="collapse-item <?= $menu_drop['user_admin']; ?>" href="<?= base_url('admin/adm'); ?>">Admin</a>
            <a class="collapse-item <?= $menu_drop['user_guru']; ?>" href="<?= base_url('admin/guru'); ?>">Guru</a>
            <a class="collapse-item <?= $menu_drop['user_siswa']; ?>" href="<?= base_url('admin/siswa'); ?>">Siswa</a>
            <a class="collapse-item <?= $menu_drop['user_profile']; ?>" href="<?= base_url('admin/profile'); ?>">Profile</a>
        </div>
    </div>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<li class="nav-item <?= $menu_drop['master_menu']; ?>">
            <a class="nav-link" href="#" data-toggle="collapse" data-target="#master"
                    aria-expanded="true" aria-controls="master">
                <i class="fas fa-fw fa-database"></i>
                <span>Data Master</span>
            </a>
    <div id="master" class="collapse <?= $menu_drop['master_show']; ?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Data Master:</h6>
            <a class="collapse-item <?= $menu_drop['master_guru']; ?>" href="<?= base_url('master/guru'); ?>">Data Guru</a>
            <a class="collapse-item <?= $menu_drop['master_siswa']; ?>" href="<?= base_url('master/datsiswa'); ?>">Data Siswa</a>
            <a class="collapse-item <?= $menu_drop['master_mapel']; ?>" href="<?= base_url('mapel'); ?>">Mata Pelajaran</a>
            <a class="collapse-item <?= $menu_drop['master_ruangan']; ?>" href="<?= base_url('ruangan'); ?>">Ruangan Kelas</a>
            <a class="collapse-item <?= $menu_drop['master_program']; ?>" href="<?= base_url('master/program'); ?>">Program Paket</a>
            <a class="collapse-item <?= $menu_drop['master_ajaran']; ?>" href="<?= base_url('master/ajaran'); ?>">Tahun Ajaran</a>
        </div>
    </div>
</li>

<hr class="sidebar-divider">
<!-- Divider -->
<li class="nav-item <?= $menu_lapor['laporan_menu']; ?>">
            <a class="nav-link" href="#" data-toggle="collapse" data-target="#laporan"
                    aria-expanded="true" aria-controls="laporan">
                <i class="fas fa-fw fa-book"></i>
                <span>Laporan</span>
            </a>
    <div id="laporan" class="collapse <?= $menu_lapor['laporan_show']; ?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Data laporan:</h6>
            <a class="collapse-item <?= $menu_lapor['laporan_siswa']; ?>" href="<?= base_url('Laporan/siswaFil'); ?>">Data Siswa</a>
        </div>
    </div>
</li>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<li class="nav-item <?= $menu['Pendaftaran']; ?>">
    <a class="nav-link" href="<?= base_url('admin/daftar')?>">
        <i class="fas fa-list-ul"></i>
        <span>Data Pendaftaran</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<li class="nav-item <?= $menu['Jadwal']; ?>">
    <a class="nav-link" href="<?= base_url('admin/jadwal') ?>">
    <i class="fas fa-pen"></i>
        <span>Jadwal Pelajaran</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">


<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>
<!-- End of Sidebar -->