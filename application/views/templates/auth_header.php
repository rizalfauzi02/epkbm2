<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $title; ?></title>

    <!-- CSS IMG-->
    <style>
    .p-5 .avatar {
	position: fixed;
	margin: 0 auto;
	left: 0;
	right: 0;
	top: 5px;
    /* bottom: 350px; */
	width: 125px;
	height: 125px;
	border-radius: 50%;
	z-index: 9;
	background: white;
	padding: 10px;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
    }
    .p-5 .avatar img {
        width: 100%;
    }
    </style>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url('assets/app-assets/sb2/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url('assets/app-assets/sb2/'); ?>css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Custom -->
    <link rel="icon" type="image/jpg" sizes="16x16" href="<?= base_url('assets/app-assets/img/'); ?>logo.png">
    <script src="https://www.google.com/recaptcha/api.js?render=reCAPTCHA_site_key"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?= base_url('assets/app-assets/'); ?>sweetalert2/dist/notiflix-2.6.0.min.css">
    <script src="<?= base_url('assets/app-assets/'); ?>sweetalert2/dist/notiflix-2.6.0.min.js"></script>
    <script src="<?= base_url('assets/app-assets/'); ?>sweetalert2/dist/sweetalert2.all.js"></script>    

</head>

<body class="bg-gradient-primary">