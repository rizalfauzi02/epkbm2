<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $title; ?></title>

    <style>
            #gambar{
                height:100px;
            }
			#img-bg{
				border: 0;
  				background: transparent;
			}
    </style>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url('assets/app-assets/sb2/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url('assets/app-assets/sb2/'); ?>css/sb-admin-2.min.css" rel="stylesheet">

	<!-- SweetAlert2 -->
	<link rel="stylesheet" href="<?= base_url('assets/app-assets/sb2/vendor/'); ?>sweetalert2/dist/notiflix-2.6.0.min.css">
    <script src="<?= base_url('assets/app-assets/sb2/vendor/'); ?>sweetalert2/dist/notiflix-2.6.0.min.js"></script>
    <script src="<?= base_url('assets/app-assets/sb2/vendor/'); ?>sweetalert2/dist/sweetalert2.all.js"></script>
    
    <!-- LOGO PKBM -->
    <link rel="icon" type="image/jpg" sizes="16x16" href="<?= base_url('assets/app-assets/img/'); ?>logo.png">

    <!-- SCRIPT JS -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" />
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">