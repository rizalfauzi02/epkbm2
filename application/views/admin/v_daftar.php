<div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
                    <!-- DataTales Example --> <br>
    <div class="row">
        <div class="col-lg-11">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <!-- <div class="card-header py-3">
                            <!-- <h6 class="m-0 font-weight-bold text-primary"> Jadwal Pelajaran</h6>
                            <a href="" class="btn btn-primary float-right" data-toggle="modal" data-target="#newRoleModal">Tambah Admin</a>
                        </div> -->
                        
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr align="center">
                                            <th scope="col">No</th>
                                            <th scope="col">NIK</th>
                                            <th scope="col">Nama Lengkap</th>
                                            <th scope="col">TTL</th>
                                            <th scope="col">Jenis Kelamin</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1;
                                        foreach($pendaftar as $dftr) : ?>
                                            <tr>
                                                <input type="hidden" name="id_pendaftaran" value="<?= $dftr->id_pendaftaran;?>">
                                                <td align="center"><?= $no++; ?></td>
                                                <td><?= $dftr->nik;?></td>
                                                <td><?= $dftr->namalengkap; ?></td>
                                                <td><?= $dftr->tempat_lahir; ?>, <?= $dftr->tanggal_lahir; ?></td>
                                                <td><?= $dftr->jk; ?></td>
                                                <td align="center">
                                                <a href="<?= base_url('admin/detdaftar/'). $dftr->id_pendaftaran; ?>" style="text-decoration:none" class="btn-info btn-circle">
                                                    <i class="fas fa-info-circle"></i>
                                                </a>
                                                <a href="<?= base_url('admin/ddaftar/') . $dftr->id_pendaftaran; ?>" style="text-decoration:none" 
                                                    class="btn-danger btn-circle btn-hapus">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->