<div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
        <!-- DataTales Example --> <br>
    
        <div class="row">

            <div class="col-lg-5">
                <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary"> Filter Data</h6>
                        </div>

                        <!-- Form Input Jadwal Pelajaran -->
                        <div class="card-body">
                            <form action="" id="FormFilter">
                                <div class="form-row mb-2 ml-4">
                                    <div class="col-sm-8">
                                            <label for="program" class="form-label">Program Paket</label>
                                            <select name="nama_program" id="program" class="form-control">
                                                <option value="0">-- Pilih --</option>
                                                <?php foreach($program as $data) { ?>
                                                    <option value="<?= $data->kd_program; ?>"><?= $data->nama_program; ?></option>
                                                <?php } ?>
                                            </select>

                                    </div>
                                    <div class="col-sm-4">
                                        <button type="submit" class="btn btn-secondary" style="margin-top: 30px;">Tampilkan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
    <div class="col-lg-3">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="form-group">
                    <a href="<?= base_url('jadwal'); ?>" class="btn btn-primary float-left">Tambah Jadwal Pelajaran</a>
                </div>
            </div>
        </div>
    </div>
            <div class="col-lg-12">
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="form-group" id="result">
                                
                            </div>
                        </div>
                    </div>
            </div>
    </div>
</div>
<!-- /.container-fluid -->
<script>
    $(document).ready(function(){
        $("#FormFilter").submit(function(e) {
            e.preventDefault();
            var id = $("#program").val();
            var url = "<?= base_url('Admin/filterJadwal/') ?>" +  id
            // console.log(url);
            $("#result").load(url);
        });
    });
</script>