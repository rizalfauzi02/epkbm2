<div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
        <!-- DataTales Example --> <br>
    
        <div class="row">
        <div class="col-lg-6">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary"> Tambah Jadwal Pelajaran</h6>
                        </div>

                    <!-- Form Input Jadwal Pelajaran -->
                        <div class="card-body">
                            <form class="row g-3" method="POST" action="<?= base_url('jadwal/prosesSubmit'); ?>">
                                <div class="col-md-4">
                                    <label for="hari" class="form-label">Hari</label>
                                    <input type="text" class="form-control" name="hari" id="hari" value="<?= $row->hari; ?>" required>
                                </div>
                                <div class="col-md-3">
                                    <label for="jam" class="form-label">Dari jam</label>
                                    <input type="time" class="form-control" id="jam" name="jam_awl" value="<?= $row->jam_awl; ?>" required>
                                </div>
                                <div class="col-md-3">
                                    <label for="jam" class="form-label">Sampai jam</label>
                                    <input type="time" class="form-control" id="jam" name="jam_akhr" value="<?= $row->jam_akhr; ?>" required>
                                </div>
                                <div class="col-10">
                                    <label for="mapel" class="form-label">Mata Pelajaran</label>
                                    <select name="nama_mapel" id="mapel" class="form-control" required>
                                        <option>-- Pilih --</option>
                                        <?php foreach($mapel as $data) { ?>
                                            <option value="<?= $data['kd_mapel']; ?>"><?= $data['nama_mapel']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-10">
                                <label for="ruangan" class="form-label">Ruangan Kelas</label>
                                    <select name="nama_ruangan" id="ruangan" class="form-control" required>
                                        <option>-- Pilih --</option>
                                        <?php foreach($ruangan as $data) { ?>
                                            <option value="<?= $data['kd_ruangan']; ?>"><?= $data['nama_ruangan']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-10">
                                <label for="guru" class="form-label">Tutor / Guru</label>
                                    <select name="nama_guru" id="guru" class="form-control" required>
                                        <option>-- Pilih --</option>
                                        <?php foreach($guru as $data) { ?>
                                            <option value="<?= $data['id_guru']; ?>"><?= $data['nama_guru']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-10">
                                <label for="program" class="form-label">Program Paket</label>
                                    <select name="nama_program" id="program" class="form-control" required>
                                        <option>-- Pilih --</option>
                                        <?php foreach($program as $data) { ?>
                                            <option value="<?= $data['kd_program']; ?>"><?= $data['nama_program']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-10">
                                    <button type="reset" class="btn btn-secondary mt-3 float-left">Reset</button>
                                    <button type="submit" name="<?= $page; ?>" class="btn btn-primary ml-2 mt-3 float-left">Tambah</button>
                                    <a href="<?= base_url('admin/jadwal'); ?>" class="mt-3 btn btn-dark float-right">Kembali</a>
                                </div>
                            </form>
                        </div>
                    </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->