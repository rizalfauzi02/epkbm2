<a href="<?= base_url('laporan/cetakJadwal/' . $paket) ?>" class="btn btn-warning float-left mb-4" target="_blank">Export PDF</a>
<table class="table table-bordered">
        <tr align="center">
            <th>#</th>
            <th>Hari</th>
            <th>Jam</th>
            <th>Mata Pelajaran</th>
            <th>Ruangan</th>
            <th>Tutor / Guru</th>
            <th>Program Paket</th>
            <th>Action</th>
        </tr>
    <?php $no=1; foreach($jadwal as $row) : ?>
        <tr align="center">
            <td><?= $no++; ?></td>
            <td><?= $row->hari;?></td>
            <td><?= $row->jam_awl; ?> - <?= $row->jam_akhr; ?></td>
            <td><?= $row->nama_mapel; ?></td>
            <td><?= $row->nama_ruangan; ?></td>
            <td><?= $row->nama_guru; ?></td>
            <td><?= $row->nama_program; ?></td>
            <td>
                <a href="<?= base_url('admin/djadwal/') . $row->id_jadwal; ?>" style="text-decoration:none" 
                    class="btn-danger btn-circle btn-hapus">
                    <i class="fas fa-trash"></i>
                </a>
            </td>
        </tr>
    <?php endforeach; ?>
</table>

<script>
    $(document).ready(function(){
  $('.btn-hapus').on('click', function(e) {

    // menghentikan link ketika klik "delete"
    e.preventDefault();

    // mengambil link setelah di klik "OK"
    const href = $(this).attr('href');

    Swal.fire({
      title: 'Apakah Anda Yakin?',
      text: "Data akan dihapus dan tidak dapat kembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'OK'
    }).then((result) => {
      if (result.isConfirmed) {
        document.location.href = href;
      }
    })

  });
    });
</script>

