<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/jpg" sizes="16x16" href="<?= base_url('assets/app-assets/img/'); ?>logo.png">
    <link rel="stylesheet" href="<?= base_url('assets/app-assets/daftar/') ?>bootstrap/css/bootstrap.min.css">
    <script src="<?= base_url('assets/app-assets/daftar/') ?>jquery/jquery-3.4.1.min.js"></script>
    <script src="<?= base_url('assets/app-assets/daftar/') ?>bootstrap/js/bootstrap.min.js"></script>
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?= base_url('assets/app-assets/'); ?>sweetalert2/dist/notiflix-2.6.0.min.css">
    <script src="<?= base_url('assets/app-assets/'); ?>sweetalert2/dist/notiflix-2.6.0.min.js"></script>
    <script src="<?= base_url('assets/app-assets/'); ?>sweetalert2/dist/sweetalert2.all.js"></script>  

</head>
<body>
    <?= $this->session->flashdata('pesan'); ?>
    <div class="container p-3 my-3 border">
    <h1 class="text-center"><?= $judul; ?></h1>

        <!-- <form id="form" method="post" action="<?= base_url('daftar/prosesSubmit'); ?>"> -->
            <div class="alert alert-primary">
                <strong>Data Diri Calon Peserta Didik</strong>
            </div>
            <div class="row">
                <div class="col-sm-7">
                <?php foreach($detail as $det) : ?>
                    <div class="form-group">
                        <label>Nama Lengkap:</label>
                        <input type="text" value="<?= $det->namalengkap; ?>" class="form-control bold" readonly>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Nomor Identitas (NIK):</label>
                        <input type="text" value="<?= $det->nik; ?>" class="form-control bold" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tempat Lahir:</label>
                        <input type="text" value="<?= $det->tempat_lahir; ?>" class="form-control bold" readonly>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Tanggal Lahir:</label>
                        <input type="text" value="<?= $det->tanggal_lahir; ?>" class="form-control bold" readonly>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Jenis Kelamin:</label>
                        <input type="text" value="<?= $det->jk; ?>" class="form-control bold" readonly>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Kewarganegaraan:</label>
                        <input type="text" value="<?= $det->kewarganegaraan; ?>" class="form-control bold" readonly>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Agama:</label>
                        <input type="text" value="<?= $det->agama; ?>" class="form-control bold" readonly>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Email:</label>
                        <input type="text" value="<?= $det->email; ?>" class="form-control bold" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>No Telp:</label>
                        <input type="text" value="<?= $det->no_telp; ?>" class="form-control bold" readonly>
                    </div>
                </div>
            </div>
            <div class="alert alert-primary">
                <strong>Data Alamat Asal</strong>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Alamat:</label>
                        <input type="text" value="<?= $det->alamat; ?>" class="form-control bold" readonly>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Kode Pos:</label>
                        <input type="text" value="<?= $det->kode_pos; ?>" class="form-control bold" readonly>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Kecamatan:</label>
                        <input type="text" value="<?= $det->kecamatan; ?>" class="form-control bold" readonly>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Kabupaten:</label>
                        <input type="text" value="<?= $det->kabupaten; ?>" class="form-control bold" readonly>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Provinsi:</label>
                        <input type="text" value="<?= $det->provinsi; ?>" class="form-control bold" readonly>
                    </div>
                </div>
            </div>

            <div class="alert alert-primary">
                <strong>Program PKBM</strong>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PAKET : </label>
                        <input type="text" value="<?= $det->nama_program; ?>" class="form-control bold" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm">
                    <!-- <button type="submit" name="Submit" id="Submit" class="btn btn-primary">Daftar</button>
                    <button type="reset" class="btn btn-secondary">Reset</button> -->
                    <a href="<?= base_url('admin/daftar'); ?>" class="btn btn-dark float-right">Kembali</a>
                </div>
            </div>
        <?php endforeach; ?>
        <!-- </form> -->
    </div>
</body>
</html>