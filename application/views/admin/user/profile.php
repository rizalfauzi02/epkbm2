<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?= $judul; ?></h1>
</div>
<div class="row">

    <div class="col-lg-6">
        <?= $this->session->flashdata('pesan'); ?>
        <!-- Update Profile -->
        <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Update Profile</h6>
                </div>
                <div class="card-body">
                    <?= form_open_multipart('Admin/profile');?>
                        <div class="row">
                                <div class="col-sm-3">
                                    <img src="<?= base_url('assets/app-assets/img/profile/') . $user['gambar']; ?>" alt="epkbm" class="img-thumbnail">
                                </div>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-rounded" name="username" value="<?= $this->session->userdata('username'); ?>" readonly>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control input-default" name="namalengkap" placeholder="Nama Lengkap" value="<?= $user['namalengkap'] ?>">
                                        <?= form_error('namalengkap', '<div class="text-danger">', '</div>');; ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control input-rounded" name="email" value="<?= $user['email'] ?>">
                                    </div>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" name="gambar" class="custom-file-input">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <button type="submit" class="btn btn-primary mt-4 float-right">Update</button>
                    </form>
                </div>
        </div>

        <!-- Udpdate Password -->
        <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Ubah Password</h6>
                </div>
                <div class="card-body">
                        <form action="<?= base_url('Admin/changepassword'); ?>" method="POST">
                            <div class="form-group">
                                <input type="password" class="form-control input-default" name="currentpassword" placeholder="Password Lama" required>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control input-rounded" name="newpassword" placeholder="Password Baru" required>
                            </div>
                            <button type="submit" class="btn btn-primary float-right">Ubah</button>
                        </form>
                </div>
        </div>

    </div>

    
    <!-- TAMPILAN PROFILE -->
    <div class="col-lg-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">My Profile</h6>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <img src="<?= base_url('assets/app-assets/img/profile/') . $user['gambar']; ?>" alt="Epkbm" class="img-thumbnail">
                </div>
                <div class="form-group text-center">
                    <strong><?= $user['namalengkap']; ?></strong><br>
                    <span>Admin PKBM Ciptamekar</span><br>
                    <?= $user['username']; ?>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->