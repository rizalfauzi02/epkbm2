<div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
                    <!-- DataTales Example --> <br>
    <div class="row">
        <div class="col-lg-5">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        
                        <div class="card-body">
                            <form method="POST" action="<?= base_url('admin/fguru'); ?>" id="form">
                                <!-- <div class="mb-3">
                                    <label for="nama" class="form-label">Nama Lengkap</label>
                                    <input type="text" class="form-control" id="nama" name="namalengkap" required>
                                </div> -->
                                <div class="form-row mb-3">
                                    <div class="col-sm-10 mb-2">
                                        <label for="">Nama Lengkap</label>
                                        <input type="text" class="form-control" id="namalengkap" name="namalengkap" required readonly>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newRoleModal" style="margin-top: 30px;">Cari</button>
                                    </div>
                                    <div class="col-sm-10 mb-2">
                                        <label for="email" class="form-label">Email</label>
                                        <input type="email" class="form-control" id="email" name="email" required>
                                    </div>
                                    
                                    <div class="col-sm-10 mb-2">
                                        <label for="username" class="form-label">Username</label>
                                        <input type="text" class="form-control" id="username" name="username" required>
                                        <?= form_error('username', '<small class="text-danger pl-1">', '</small>'); ?>
                                    </div>
                                    <div class="col-sm-10 mb-2">
                                        <label for="password" class="form-label">Password</label>
                                        <input type="password" class="form-control" id="password" name="password" required>
                                    </div>
                                    <div class="col-sm-10 mb-2">
                                        <label for="program" class="form-label">Program Paket</label>
                                        <select name="nama_program" id="program" class="form-control" required>
                                            <option>-- Pilih --</option>
                                            <?php foreach($program as $data) { ?>
                                                <option value="<?= $data['kd_program']; ?>"><?= $data['nama_program']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary float-right">Tambah</button>
                                <a href="<?= base_url('admin/guru'); ?>" class="btn btn-dark float-left">Kembali</a>
                            </form>
                        </div>
                    </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
<!-- Modal New Role-->
<div class="modal fade" id="newRoleModal" tabindex="-1" aria-labelledby="newRoleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newRoleModalLabel">Cari Data Guru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
        <div class="table-responsive">
                    <table class="table table-responsive text-nowrap">
                        <thead>
                            <tr align="center">
                                <th style="width:80px;"><strong>#</strong></th>
                                <th><strong>NUPTK</strong></th>
                                <th><strong>NAMA LENGKAP</strong></th>
                                <th><strong></strong></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($guru as $data) : ?>
                                <tr>
                                    <td><strong><?= $no++; ?></strong></td>
                                    <td><?= $data->nuptk; ?></td>
                                    <td><?= $data->nama_guru; ?></td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-xs" id="select"
                                        data-nuptk="<?= $data->nuptk; ?>" 
                                        data-namaguru="<?= $data->nama_guru; ?>"
                                        
                                        data-dismiss="modal">Pilih</button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <!-- <button type="submit" class="btn btn-primary">Tambah</button> -->
        </div>
      <!-- </form> -->

    </div>
  </div>
</div>
<!-- END -->
<script>
$(document).ready(function(){
    // LOAD DATA BY NIK, DI SISWA FORM
    $(document).on('click', '#select', function(){
        var nuptk = $(this).data('nuptk');
        var nama_guru = $(this).data('namaguru');
            $('#namalengkap').val(nama_guru);
    });
});
</script>