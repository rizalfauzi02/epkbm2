<div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
                    <!-- DataTales Example --> <br>
    <div class="row">
        <div class="col-lg-8">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <!-- <h6 class="m-0 font-weight-bold text-primary"> Jadwal Pelajaran</h6> -->
                            <a href="<?= base_url('admin/fadmin'); ?>" class="btn btn-primary float-right">Tambah Admin</a>
                        </div>
                        
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr align="center">
                                            <th scope="col">No</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Username</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1;
                                        foreach($admin as $adm) : ?>
                                            <tr>
                                                <td align="center"><?= $no++; ?></td>
                                                <td><?= $adm->namalengkap;?></td>
                                                <td><?= $adm->username; ?></td>
                                                <td><?= $adm->email; ?></td>
                                                <td align="center">
                                                    <?php if($adm->username == $this->session->userdata('username')) : ?>
                                                        <span class="badge light badge-success">Anda</span>
                                                    <?php else : ?>
                                                      <!-- <button class="btn-info btn-circle btn-modal-admin" data-toggle="modal" data-target="#editAdmin" data-idu="<?= $adm->id_users; ?>">
                                                          <i class="fas fa-edit">
                                                        </button></i>  -->
                                                       <a href="" style="text-decoration:none" class="btn-info btn-circle btn-modal-admin" data-toggle="modal" 
                                                          data-target="#editAdmin" data-idu="<?= $adm->id_users; ?>">
                                                          <i class="fas fa-edit"></i>
                                                      </a>
                                                      <a href="<?= base_url('admin/dadmin/') . $adm->id_users; ?>" style="text-decoration:none" 
                                                        class="btn-danger btn-circle btn-hapus">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

<!-- Modal New Role-->
<div class="modal fade" id="editAdmin" tabindex="-1" aria-labelledby="newRoleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newRoleModalLabel">Form Update User Admin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="<?= base_url('admin/uadmin'); ?>" method="POST">
        <div class="modal-body">
          <div class="form-group">
            <input type="hidden" name="idusers">
            <label for="" class="form-label">Nama</label>
            <input type="text" class="form-control" name="namalengkapadmin">
          </div>

          <div class="form-group">
            <label for="" class="form-label">Email</label>
            <input type="email" class="form-control" name="emailadmin">
          </div>

          <div class="form-group">
            <label for="" class="form-label">Username</label>
            <input type="text" class="form-control" name="usernameadmin">
          </div>

          <div class="form-group">
            <label for="" class="form-label">Password</label>
            <input type="password" class="form-control" name="passwordadmin">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>

    </div>
  </div>
</div>
<!-- End Modal New Menu -->