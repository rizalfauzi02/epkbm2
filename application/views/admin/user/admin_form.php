<div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
                    <!-- DataTales Example --> <br>
    <div class="row">
        <div class="col-lg-6">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        
                        <div class="card-body">
                            <form method="POST" action="<?= base_url('admin/fadmin'); ?>" id="form">
                                <div class="mb-3">
                                    <label for="nama" class="form-label">Nama Lengkap</label>
                                    <input type="text" class="form-control" id="nama" name="namalengkap" required>
                                </div>
                                <div class="mb-3">
                                    <label for="email" class="form-label">Email</label>
                                    <input type="email" class="form-control" id="email" name="email" required>
                                </div>
                                <div class="mb-3">
                                    <label for="username" class="form-label">Username</label>
                                    <input type="text" class="form-control" id="username" name="username" required>
                                    <?= form_error('username', '<small class="text-danger pl-1">', '</small>'); ?>
                                </div>
                                <div class="mb-3">
                                    <label for="password" class="form-label">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" required>
                                </div>

                                <button type="submit" class="btn btn-primary float-right">Tambah</button>
                                <a href="<?= base_url('admin/adm'); ?>" class="btn btn-dark float-left">Kembali</a>
                            </form>
                        </div>
                    </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->