<div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
                    <!-- DataTales Example --> <br>
    <div class="row">
        <div class="col-lg-10">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <!-- <h6 class="m-0 font-weight-bold text-primary"> Jadwal Pelajaran</h6> -->
                            <a href="<?= base_url('admin/fguru'); ?>" class="btn btn-primary float-right">Tambah Guru</a>
                        </div>
                        
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr align="center">
                                            <th scope="col">No</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Username</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Program Paket</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1;
                                        foreach($guru as $row) : ?>
                                            <tr>
                                                <td align="center"><?= $no++; ?></td>
                                                <td><?= $row->namalengkap;?></td>
                                                <td align="center"><?= $row->username; ?></td>
                                                <td><?= $row->email; ?></td>
                                                <td align="center"><?= $row->nama_program ?></td>
                                                <td align="center">
                                                      <a href="<?= base_url('admin/detUser/'). $row->id_users; ?>" style="text-decoration:none" class="btn-secondary btn-circle">
                                                          <i class="fas fa-info-circle"></i>
                                                      </a>
                                                       <a href="" style="text-decoration:none" class="btn-info btn-circle btn-modal-Uguru" data-toggle="modal" 
                                                          data-target="#editUguru" data-idu="<?= $row->id_users; ?>">
                                                          <i class="fas fa-edit"></i>
                                                      </a>
                                                      <a href="<?= base_url('admin/dguruUser/') . $row->id_users; ?>" style="text-decoration:none" 
                                                        class="btn-danger btn-circle btn-hapus">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

<!-- Modal New Role-->
<div class="modal fade" id="editUguru" tabindex="-1" aria-labelledby="newRoleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newRoleModalLabel">Form Update User Admin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="<?= base_url('admin/uguru'); ?>" method="POST">
        <div class="modal-body">
          <div class="form-group">
            <input type="hidden" name="idusers">
            <label for="" class="form-label">Nama</label>
            <input type="text" class="form-control" name="namalengkapguru">
          </div>

          <div class="form-group">
            <label for="" class="form-label">Email</label>
            <input type="email" class="form-control" name="emailguru">
          </div>

          <div class="form-group">
            <label for="" class="form-label">Username</label>
            <input type="text" class="form-control" name="usernameguru">
          </div>

          <div class="form-group">
            <label for="" class="form-label">Password</label>
            <input type="password" class="form-control" name="passwordguru">
          </div>
          <div class="form-group">
            <label for="program" class="form-label">Program Paket</label>
                <select name="namaprogram" id="program" class="form-control" required>
                    <option>-- Pilih --</option>
                    <?php foreach($program as $data) { ?>
                        <option value="<?= $data['kd_program']; ?>"><?= $data['nama_program']; ?></option>
                    <?php } ?>
                </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>

    </div>
  </div>
</div>
<!-- End Modal New Menu -->
<script>
$(document).ready(function(){
      $('.btn-modal-Uguru').click(function () {
          const id_users = $(this).data('idu');
          alert
          $.ajax({
            type: 'POST',
            dataType: 'JSON',
            data: {
              id_users: id_users
            },
            url: "http://localhost/epkbm2/Admin/auguru",
            success: function(data) {
              $.each(data,function(id_users, namalengkap, username, password, email, gambar, akses, isActive, nama_program, created_at, updated_at){
                  $('input[name=idusers]').val(data.id_users);
                  $('input[name=namalengkapguru]').val(data.namalengkap);
                  $('input[name=emailguru]').val(data.email);
                  $('input[name=usernameguru]').val(data.username);
                  $('select[name=namaprogram]').val(data.nama_program);
                  });
                }
            });
      });
});
</script>