<!-- Earnings (Monthly) Card Example -->
<div class="container-fluid">
    <!-- JUDUL HALAMAN -->
<h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
    <!-- ADMIN Card Example -->
    <div class="row">
        <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-danger shadow h-100 py-2">
                                    <a href="<?= base_url('admin/adm'); ?>" style="text-decoration: none;">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                                        Total User - Admin</div>
                                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_admin; ?></div>
                                                    <!-- <div class="p mb-0 mt-1 text-center font-weight text-gray-500">Selengkapnya <i class="fas fa-arrow-right "></div></i> -->
                                                </div>
                                                <div class="col-auto">
                                                    <i class="fas fa-user-shield fa-2x text-gray-400"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
        </div>

    <!-- SISWA Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-success shadow h-100 py-2">
                                    <a href="<?= base_url('admin/siswa'); ?>" style="text-decoration: none;">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                        Total User - Siswa</div>
                                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_siswa; ?></div>
                                                </div>
                                                <div class="col-auto">
                                                    <i class="fas fa-comments fa-2x text-gray-300"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
        </div>
    <!-- PENDAFTAR Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-warning shadow h-100 py-2">
                                    <a href="<?= base_url('admin/daftar'); ?>" style="text-decoration: none;">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                        Total Pendaftar</div>
                                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_pendaftar; ?></div>
                                                </div>
                                                <div class="col-auto">
                                                    <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
        </div>
    <!-- JADWAL PELAJARAN Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <a href="<?= base_url('admin/jadwal'); ?>" style="text-decoration: none;">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                        Total Data Jadwal</div>
                                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_jadwal; ?></div>
                                                </div>
                                                <div class="col-auto">
                                                    <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
        </div>
        <!-- DATA MASTER - GURU -->
        <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-bottom-info shadow h-100 py-2">
                                    <a href="<?= base_url('master/guru'); ?>" style="text-decoration: none;">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                        Total Data Master - GURU</div>
                                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_mguru; ?></div>
                                                </div>
                                                <div class="col-auto">
                                                    <i class="fas fa-user-tie fa-2x text-gray-300"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
        </div>
        <!-- DATA MASTER - SISWA -->
        <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-bottom-secondary shadow h-100 py-2">
                                    <a href="<?= base_url('master/datsiswa'); ?>" style="text-decoration: none;">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">
                                                        Total Data Master - SISWA</div>
                                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_msiswa; ?></div>
                                                </div>
                                                <div class="col-auto">
                                                    <i class="fas fa-user-graduate fa-2x text-gray-300"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
        </div>
        <!-- DATA MASTER - MATA PELAJARAN -->
        <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-bottom-primary shadow h-100 py-2">
                                    <a href="<?= base_url('mapel'); ?>" style="text-decoration: none;">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                        Total Data Master - MATA PELAJARAN</div>
                                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_mmapel; ?></div>
                                                </div>
                                                <div class="col-auto">
                                                    <i class="fas fa-user-graduate fa-2x text-gray-300"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
        </div>
        <!-- DATA MASTER - RUANGAN KELAS -->
        <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-bottom-danger shadow h-100 py-2">
                                    <a href="<?= base_url('ruangan'); ?>" style="text-decoration: none;">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                                        Total Data Master - RUANGAN KELAS</div>
                                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_mruangan; ?></div>
                                                </div>
                                                <div class="col-auto">
                                                    <i class="fas fa-user-graduate fa-2x text-gray-300"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
        </div>
    </div>
</div>