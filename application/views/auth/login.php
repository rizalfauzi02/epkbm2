<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-lg-7">
        <?= $this->session->flashdata('pesan'); ?>

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <div class="avatar">
                                        <img src="<?= base_url('assets/app-assets/'); ?>img/logo.png">
                                </div>
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mt-5 mb-4">LOGIN</h1>
                                </div>
                                <form class="user" method="post" action="<?= base_url('auth/proseslogin'); ?>">
                                    <div class="form-group">
                                        <input name="username" type="text" class="form-control form-control-user" id="username" placeholder="Username" name="username">
                                    </div>
                                    <div class="form-group">
                                        <input name="password" type="password" class="form-control form-control-user" id="password" placeholder="Password" name="password">
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                        Login
                                    </button>
                                </form>
                                <hr>
                                <!-- <div class="text-center">
                                        <a class="small" href="<?= base_url('daftar'); ?>">Pendaftaran</a>
                                </div> -->
                                <!-- <div class="text-center">
                                    <a class="small" href="<?= base_url('auth/registration'); ?>">Registrasi!</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

</div>