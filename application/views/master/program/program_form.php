<div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
                    <!-- DataTales Example --> <br>
    <div class="row">
        <div class="col-lg-6">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        
                        <div class="card-body">
                            <form method="POST" action="<?= base_url('master/createProg'); ?>" id="form">
                                <div class="mb-3">
                                    <label for="kode" class="form-label">Kode Program Paket</label>
                                    <input type="number" class="form-control" id="kode" name="kd_program" oninput="this.value=this.value.slice(0,this.maxLength)" maxlength="6" placeholder="Contoh: PKTA01 (max 6 huruf)">
                                    <?= form_error('kd_program', '<small class="text-danger pl-1">', '</small>'); ?>
                                </div>
                                <div class="mb-3">
                                    <label for="nama" class="form-label">Nama Program Paket</label>
                                    <input type="text" class="form-control" id="nama" name="nama_program" placeholder="Contoh: PAKET A (SD). SD itu tingkatan paketnya">
                                </div>
                                <!-- <div class="mb-3">
                                    <label for="tingkatan" class="form-label">Tingkatan</label>
                                    <input type="text" class="form-control" id="tingkatan" name="tingkatan">
                                </div> -->

                                <button type="submit" class="btn btn-primary float-right">Tambah</button>
                                <a href="<?= base_url('master/program'); ?>" class="btn btn-dark float-left">Kembali</a>
                            </form>
                        </div>
                    </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->