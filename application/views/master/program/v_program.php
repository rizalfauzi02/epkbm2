<div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
                    <!-- DataTales Example --> <br>
    <div class="row">
        <div class="col-lg-9">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <!-- <h6 class="m-0 font-weight-bold text-primary"> Jadwal Pelajaran</h6> -->
                            <a href="<?= base_url('laporan/Program') ?>" class="btn btn-warning float-left" target="_blank">Export</a>
                            <a href="<?= base_url('Master/createProg'); ?>" class="btn btn-primary float-right">Tambah Data Program Paket</a>
                        </div>
                        
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr align="center">
                                            <th scope="col">No</th>
                                            <th scope="col">Kode Program</th>
                                            <th scope="col">Nama Program Paket</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1;
                                        foreach($program as $prog) : ?>
                                            <tr>
                                                <td align="center"><?= $no++; ?></td>
                                                <td align="center"><?= $prog->kd_program?></td>
                                                <td align="center"><?= $prog->nama_program; ?></td>
                                                <td align="center">
                                                    <a href="" style="text-decoration:none" class="btn-info btn-circle btn-modal-program" data-toggle="modal" 
                                                          data-target="#editProgram" data-idu="<?= $prog->kd_program; ?>">
                                                          <i class="fas fa-edit"></i>
                                                    </a>
                                                    <a href="<?= base_url('master/dprogram/') . $prog->kd_program; ?>" style="text-decoration:none" 
                                                        class="btn-danger btn-circle btn-hapus">
                                                        <i class="fas fa-trash"></i> <!-- Belum dibuat detail -->
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

<!-- Modal New Role-->
<div class="modal fade" id="editProgram" tabindex="-1" aria-labelledby="newRoleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newRoleModalLabel">Form Update Data Mata Pelajaran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="<?= base_url('master/uprogram'); ?>" method="POST">
        <div class="modal-body">
        <!-- <P align="center"> Pastikan data yang dibuat sesuai dengan data pendaftaran. </P> -->

          <div class="form-group">
            <label for="" class="form-label">Kode Program Paket</label>
            <input type="text" class="form-control" name="kdprogram" required readonly>
          </div>

          <div class="form-group">
            <label for="" class="form-label">Nama Mata Pelajaran</label>
            <input type="text" id="nik_siswa" class="form-control" name="namaprogram" required>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>

    </div>
  </div>
</div>
<!-- End Modal New Menu -->
<script>

</script>