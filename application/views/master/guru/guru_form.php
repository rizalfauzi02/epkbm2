<div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
                    <!-- DataTales Example --> <br>
    <div class="row">
        <div class="col-lg-6">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        
                        <div class="card-body">
                            <form method="POST" action="<?= base_url('master/fguru'); ?>" id="form">
                                <div class="mb-3">
                                    <label for="nama" class="form-label">NUPTK</label>
                                    <input type="number" id="nuptk" oninput="this.value=this.value.slice(0,this.maxLength)" class="form-control" name="nuptk" min="0" maxlength="11" placeholder="Masukan minimal 11 angka." required>
                                    <?= form_error('nuptk', '<small class="text-danger pl-1">', '</small>'); ?>
                                </div>
                                <div class="mb-3">
                                    <label for="email" class="form-label">Nama</label>
                                    <input type="text" class="form-control" name="nama_guru" required>
                                </div>
                                <div class="mb-3">
                                <label for="" class="form">Jenis Kelamin:</label>
                                        <select class="form-control" name="jk_guru" required>
                                            <option>-- Pilih --</option>
                                            <option value="Laki-Laki">Laki-laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                </div>

                                <button type="submit" class="btn btn-primary float-right">Tambah</button>
                                <a href="<?= base_url('master/guru'); ?>" class="btn btn-dark float-left">Kembali</a>
                            </form>
                        </div>
                    </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->