<div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
                    <!-- DataTales Example --> <br>
    <div class="row">
        <div class="col-lg-8">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <!-- <h6 class="m-0 font-weight-bold text-primary"> Jadwal Pelajaran</h6> -->
                            <a href="<?= base_url('laporan/Guru') ?>" class="btn btn-warning float-left" target="_blank">Export</a>
                            <a href="<?= base_url('master/fguru') ?>" class="btn btn-primary float-right">Tambah Guru</a>
                        </div>
                        
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr align="center">
                                            <th scope="col">No</th>
                                            <th scope="col">Nuptk</th>
                                            <th scope="col">Nama Guru</th>
                                            <th scope="col">Jenis Kelamin</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($guru as $g) : ?>
                                            <tr>
                                                <td align="center"><?= ++$start; ?></td>
                                                <td><?= $g->nuptk;?></td>
                                                <td><?= $g->nama_guru; ?></td>
                                                <td><?= $g->jk_guru; ?></td>
                                                <td align="center">
                                                    <a href="" style="text-decoration:none" class="btn-info btn-circle btn-modal-guru" data-toggle="modal" 
                                                        data-target="#editGuru" data-idu="<?= $g->id_guru; ?>">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                    <a href="<?= base_url('admin/dguru/') . $g->id_guru; ?>" style="text-decoration:none" 
                                                        class="btn-danger btn-circle btn-hapus">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <?= $this->pagination->create_links(); ?>
                        </div>
                    </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

<!-- Modal New Role-->
<div class="modal fade" id="editGuru" tabindex="-1" aria-labelledby="newRoleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newRoleModalLabel">Form Update Data Guru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="<?= base_url('master/uguru'); ?>" method="POST">
        <div class="modal-body">
          <div class="form-group">
            <input type="hidden" name="idguru">
            <label for="" class="form-label">NUPTK</label>
            <input type="number" oninput="this.value=this.value.slice(0,this.maxLength)" class="form-control" name="nuptk" maxlength="11" required>
          </div>

          <div class="form-group">
            <label for="" class="form-label">Nama</label>
            <input type="text" class="form-control" name="namaguru">
          </div>

          <div class="form-group">
          <label for="" class="form">Jenis Kelamin:</label>
                <select class="form-control" name="jkguru" required>
                    <option value="Laki-Laki">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                </select>
          </div>
                  <!-- KALO STUCK, GA USAH PAKE VALUE DI JK NYA -->
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>

    </div>
  </div>
</div>
<!-- End Modal New Menu -->