<div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
                    <!-- DataTales Example --> <br>
    <div class="row">
        <div class="col-lg-7">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <!-- <h6 class="m-0 font-weight-bold text-primary"> Jadwal Pelajaran</h6> -->
                            <a href="<?= base_url('laporan/Ruangan') ?>" class="btn btn-warning float-left" target="_blank">Export</a>
                            <a href="<?= base_url('ruangan/create'); ?>" class="btn btn-primary float-right">Tambah Data Ruangan</a>
                        </div>
                        
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr align="center">
                                            <th scope="col">No</th>
                                            <th scope="col">Kode Ruangan</th>
                                            <th scope="col">Nama Ruangan Kelas</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1;
                                        foreach($ruangan as $rng) : ?>
                                            <tr>
                                                <td align="center"><?= $no++; ?></td>
                                                <td align="center"><?= $rng->kd_ruangan;?></td>
                                                <td><?= $rng->nama_ruangan; ?></td>
                                                <td align="center">
                                                    <a href="" style="text-decoration:none" class="btn-info btn-circle btn-modal-ruangan" data-toggle="modal" 
                                                          data-target="#editRuangan" data-idu="<?= $rng->kd_ruangan; ?>">
                                                          <i class="fas fa-edit"></i>
                                                    </a>
                                                    <a href="<?= base_url('ruangan/delete/') . $rng->kd_ruangan; ?>" style="text-decoration:none" 
                                                        class="btn-danger btn-circle btn-hapus">
                                                        <i class="fas fa-trash"></i> <!-- Belum dibuat detail -->
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

<!-- Modal New Role-->
<div class="modal fade" id="editRuangan" tabindex="-1" aria-labelledby="newRoleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newRoleModalLabel">Form Update Data Ruangan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="<?= base_url('ruangan/uruangan'); ?>" method="POST">
        <div class="modal-body">
        <!-- <P align="center"> Pastikan data yang dibuat sesuai dengan data pendaftaran. </P> -->

          <div class="form-group">
            <label for="" class="form-label">Kode Ruangan</label>
            <input type="text" class="form-control" name="kdruangan" required readonly>
          </div>

          <div class="form-group">
            <label for="" class="form-label">Nama Ruangan Kelas</label>
            <input type="text" class="form-control" name="namaruangan" required>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Tambah</button>
        </div>
      </form>

    </div>
  </div>
</div>
<!-- End Modal New Menu -->