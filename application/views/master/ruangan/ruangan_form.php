<div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
                    <!-- DataTales Example --> <br>
    <div class="row">
        <div class="col-lg-6">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        
                        <div class="card-body">
                            <form method="POST" action="<?= base_url('ruangan/create'); ?>" id="form">
                                <div class="mb-3">
                                    <label for="kode" class="form-label">Kode Ruangan Kelas</label>
                                    <input type="text" class="form-control" id="kode" name="kd_ruangan" oninput="this.value=this.value.slice(0,this.maxLength)" maxlength="5" placeholder="Contoh: PKTA1 (max 5 huruf) artinya Ruangan Paket A - 1">
                                    <?= form_error('kd_ruangan', '<small class="text-danger pl-1">', '</small>'); ?>
                                </div>
                                <div class="mb-3">
                                    <label for="nama" class="form-label">Nama Ruangan Kelas</label>
                                    <input type="text" class="form-control" id="nama" name="nama_ruangan" placeholder="Contoh: Ruangan Kelas Paket A - 1">
                                </div>

                                <button type="submit" class="btn btn-primary float-right">Tambah</button>
                                <a href="<?= base_url('ruangan'); ?>" class="btn btn-dark float-left">Kembali</a>
                            </form>
                        </div>
                    </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->