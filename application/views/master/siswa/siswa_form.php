<div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
        <!-- DataTales Example --> <br>
    
        <div class="row">
        <div class="col-lg-9">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary"> Tambah Data Siswa</h6>
                        </div>

                    <!-- Form Input Jadwal Pelajaran -->
                        <div class="card-body">
                            <form class="row g-3" method="POST" action="<?= base_url('master/fsiswa'); ?>">
                                <div class="col-md-4">
                                    <label for="nis" class="form-label">NIS</label>
                                    <input type="number" id="nis" oninput="this.value=this.value.slice(0,this.maxLength)" class="form-control" name="nis" min="0" maxlength="11" placeholder="NIS" required>
                                    <?= form_error('nis', '<small class="text-danger pl-1">', '</small>'); ?>
                                </div>
                                <div class="form-row mb-2">
                                    <div class="col-sm-8">
                                        <label for="">NIK</label>
                                        <input type="text" class="form-control" name="nik_siswa" id="nik" value="" readonly>
                                        <?= form_error('nik_siswa', '<small class="text-danger pl-1">', '</small>'); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newRoleModal" style="margin-top: 30px;">Cari NIK</button>
                                    </div>
                                </div>
                                <div class="col-7 mb-2">
                                    <label for="namasiswa" class="form-label">Nama Siswa</label>
                                    <input type="text" id="namasiswa" class="form-control" name="nama_siswa" readonly>
                                </div>
                                <div class="col-7 mb-2">
                                    <label for="mapel" class="form-label">Tempat Lahir</label>
                                    <input type="text" id="tempat" class="form-control" name="lahir_siswa" readonly>
                                </div>
                                <div class="col-7 mb-2">
                                <label for="ruangan" class="form-label">Tanggal Lahir</label>
                                    <input type="date" id="tanggal" class="form-control" name="tanggal_siswa" readonly>
                                </div>
                                <div class="col-md-7">
                                <label for="kelamin" class="form-label">Jenis Kelamin</label>
                                    <input type="text" id="kelamin" class="form-control" name="jk_siswa" readonly>
                                </div>
                                <div class="col-md-7">
                                <label for="program" class="form-label">Program Paket</label>
                                    <input type="text" id="program" class="form-control" name="nama_program" readonly>
                                    <input type="hidden" id="kdprogram" name="kd_program" readonly>
                                </div>
                                <div class="col-7">
                                    <button type="reset" class="btn btn-secondary mt-3 float-left">Reset</button>
                                    <button type="submit" name="<?= $page; ?>" class="btn btn-primary ml-2 mt-3 float-left">Tambah</button>
                                    <a href="<?= base_url('master/datsiswa'); ?>" class="mt-3 btn btn-dark float-right">Kembali</a>
                                </div>
                            </form>
                        </div>
                    </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

<!-- Modal New Role-->
<div class="modal fade" id="newRoleModal" tabindex="-1" aria-labelledby="newRoleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newRoleModalLabel">Cari NIK</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <!-- <form action="<?= base_url('master/guru'); ?>" method="POST"> -->
        <div class="modal-body">
        <div class="table-responsive">
                    <table class="table table-responsive text-nowrap">
                        <thead>
                            <tr align="center">
                                <th style="width:80px;"><strong>#</strong></th>
                                <th><strong>NIK</strong></th>
                                <th><strong>NAMA LENGKAP</strong></th>
                                <th><strong>TEMPAT LAHIR</strong></th>
                                <th><strong>TANGGAL LAHIR</strong></th>
                                <th><strong>JENIS KELAMIN</strong></th>
                                <th><strong>PROGRAM PAKET</strong></th>
                                <th><strong></strong></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($pendaftaran as $data) : ?>
                                <tr>
                                    <td><strong><?= $no++; ?></strong></td>
                                    <td><?= $data->nik; ?></td>
                                    <td><?= $data->namalengkap; ?></td>
                                    <td><?= $data->tempat_lahir; ?></td>
                                    <td><?= $data->tanggal_lahir; ?></td>
                                    <td><?= $data->jk; ?></td>
                                    <td><?= $data->nama_program; ?></td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-xs" id="select"
                                        data-nik="<?= $data->nik; ?>" 
                                        data-namalengkap="<?= $data->namalengkap; ?>"
                                        data-tempat="<?= $data->tempat_lahir; ?>"
                                        data-tanggal="<?= $data->tanggal_lahir; ?>"
                                        data-jeniskelamin="<?= $data->jk; ?>"
                                        data-program="<?= $data->nama_program; ?>"
                                        data-kdprogram="<?= $data->kd_program; ?>"
                                        
                                        data-dismiss="modal">Pilih</button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <!-- <button type="submit" class="btn btn-primary">Tambah</button> -->
        </div>
      <!-- </form> -->

    </div>
  </div>
</div>
<!-- END -->