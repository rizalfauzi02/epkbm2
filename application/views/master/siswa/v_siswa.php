<div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
                    <!-- DataTales Example --> <br>
    <div class="row">
        <div class="col-lg-12">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <!-- <h6 class="m-0 font-weight-bold text-primary"> Jadwal Pelajaran</h6> -->
                            <a href="<?= base_url('laporan/Siswa') ?>" class="btn btn-warning float-left" target="_blank">Export</a>
                            <a href="<?= base_url('master/fsiswa') ?>" class="btn btn-primary float-right">Tambah Siswa</a>
                        </div>
                        
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr align="center">
                                            <th scope="col">No</th>
                                            <th scope="col">NIS</th>
                                            <th scope="col">NIK</th>
                                            <th scope="col">Nama Siswa</th>
                                            <th scope="col">Tempat Lahir</th>
                                            <th scope="col">Tanggal Lahir</th>
                                            <th scope="col">Jenis Kelamin</th>
                                            <th scope="col">Program Paket</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1;
                                        foreach($datsiswa as $sis) : ?>
                                            <tr>
                                                <td align="center"><?= $no++; ?></td>
                                                <td><?= $sis->nis;?></td>
                                                <td><?= $sis->nik_siswa; ?></td>
                                                <td><?= $sis->nama_siswa; ?></td>
                                                <td><?= $sis->lahir_siswa;?></td>
                                                <td><?= $sis->tanggal_siswa;?></td>
                                                <td><?= $sis->jk_siswa;?></td>
                                                <td><?= $sis->nama_program;?></td>
                                                <td align="center">
                                                    <!-- <a href="<?= base_url('admin/detdaftar/'). $dftr->id_pendaftaran; ?>" style="text-decoration:none" class="btn-info btn-circle">
                                                        <i class="fas fa-info-circle"></i>
                                                    </a> -->
                                                    <a href="<?= base_url('admin/delsiswa/') . $sis->id_siswa; ?>" style="text-decoration:none" 
                                                        class="btn-danger btn-circle btn-hapus">
                                                        <i class="fas fa-trash"></i> <!-- Belum dibuat detail -->
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->