<div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
                    <!-- DataTales Example --> <br>
    <div class="row">
        <div class="col-lg-6">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <form method="POST" action="<?= base_url('master/createAjaran'); ?>" id="form">
                                <div class="mb-3">
                                    <label for="kode" class="form-label">Kode Tahun Ajaran</label>
                                    <input type="text" class="form-control" id="kode" name="kd_ajaran" oninput="this.value=this.value.slice(0,this.maxLength)" min="0" maxlength="5" placeholder="Contoh: THN21 = Tahun 2021 (max 5 huruf)">
                                    <?= form_error('kd_ajaran', '<small class="text-danger pl-1">', '</small>'); ?>
                                </div>
                                <div class="mb-3">
                                    <label for="tahun" class="form-label">Tahun Ajaran</label>
                                    <input type="number" class="form-control" id="tahun" name="tahun_ajaran" placeholder="isi angka tahun saja" oninput="this.value=this.value.slice(0,this.maxLength)" min="0" maxlength="4">
                                </div>
                                <!-- <div class="mb-3">
                                    <label for="tingkatan" class="form-label">Tingkatan</label>
                                    <input type="text" class="form-control" id="tingkatan" name="tingkatan">
                                </div> -->

                                <button type="submit" class="btn btn-primary float-right">Tambah</button>
                                <a href="<?= base_url('master/ajaran'); ?>" class="btn btn-dark float-left">Kembali</a>
                            </form>
                        </div>
                    </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->