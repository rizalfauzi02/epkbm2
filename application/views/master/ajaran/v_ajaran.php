<div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
                    <!-- DataTales Example --> <br>
    <div class="row">
        <div class="col-lg-9">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <!-- <h6 class="m-0 font-weight-bold text-primary"> Jadwal Pelajaran</h6> -->
                            <!-- <a href="<?= base_url('laporan/Program') ?>" class="btn btn-warning float-left" target="_blank">Export</a> -->
                            <a href="<?= base_url('Master/createAjaran'); ?>" class="btn btn-primary float-right">Tambah Data Tahun Ajaran</a>
                        </div>
                        
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr align="center">
                                            <th scope="col">No</th>
                                            <th scope="col">Kode Tahun Ajaran</th>
                                            <th scope="col">Tahun Ajaran</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1;
                                        foreach($ajaran as $row) : ?>
                                            <tr>
                                                <td align="center"><?= $no++; ?></td>
                                                <td align="center"><?= $row->kd_ajaran?></td>
                                                <td align="center"><?= $row->tahun_ajaran; ?></td>
                                                <td align="center">
                                                    <a href="" style="text-decoration:none" class="btn-info btn-circle btn-modal-ajaran" data-toggle="modal" 
                                                          data-target="#editAjaran" data-idu="<?= $row->kd_ajaran; ?>">
                                                          <i class="fas fa-edit"></i>
                                                    </a>
                                                    <a href="<?= base_url('master/dajaran/') . $row->kd_ajaran; ?>" style="text-decoration:none" 
                                                        class="btn-danger btn-circle btn-hapus">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

<!-- Modal New Role-->
<div class="modal fade" id="editAjaran" tabindex="-1" aria-labelledby="newRoleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newRoleModalLabel">Form Update Tahun Ajaran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="<?= base_url('master/uajaran'); ?>" method="POST">
        <div class="modal-body">
        <!-- <P align="center"> Pastikan data yang dibuat sesuai dengan data pendaftaran. </P> -->

          <div class="form-group">
            <label for="" class="form-label">Kode Tahun Ajaran</label>
            <input type="text" class="form-control" name="kdajaran" required readonly>
          </div>

          <div class="form-group">
            <label for="" class="form-label">Tahun Ajaran</label>
            <input type="text" class="form-control" name="tahunajaran" required>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>

    </div>
  </div>
</div>
<!-- End Modal New Menu -->
<script>
$(document).ready(function(){
      // AJAX UPDATE TAHUN AJARAN
      $('.btn-modal-ajaran').click(function () {
          const kd_ajaran = $(this).data('idu');
          alert
          $.ajax({
            type: 'POST',
            dataType: 'JSON',
            data: {
              kd_ajaran: kd_ajaran
            },
            url: "http://localhost/epkbm2/Master/auajaran",
            success: function(data) {
              $.each(data,function(kd_ajaran, tahun_ajaran){
                  $('input[name=kdajaran]').val(data.kd_ajaran);
                  $('input[name=tahunajaran]').val(data.tahun_ajaran);
                  });
                }
            });
      });
});
</script>