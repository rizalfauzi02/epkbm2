<a href="<?= base_url('laporan/cetakAbsenMapel/' . $mpl) ?>" class="btn btn-warning float-left mb-4" target="_blank">Cetak</a>
<table class="table table-bordered">
        <tr align="center">
            <th>#</th>
            <th>NIS</th>
            <th>Mata Pelajaran</th>
            <th>Nama Siswa</th>
            <th>Tahun Ajaran</th>
            <!-- <th>Jenis Kelamin</th> -->
        </tr>
    <?php $no=1; foreach($mapel as $row) : ?>
        <tr>
            <td align="center"><?= $no++; ?></td>
            <td align="center"><?= $row->nis; ?></td>
            <td align="center"><?= $row->nama_mapel; ?></td>
            <td align="center"><?= $row->nama_siswa; ?></td>
            <td align="center"><?= $row->tahun_ajaran; ?></td>
            <!-- <td><?= $row->nama_siswa; ?></td>
            <td><?= $row->lahir_siswa; ?></td>
            <td align="center"><?= $row->tanggal_siswa; ?></td>
            <td align="center"><?= $row->jk_siswa; ?></td> -->
        </tr>
    <?php endforeach; ?>
</table>