<a href="<?= base_url('laporan/cetakNilaiMapel/' . $NilaiMap) ?>" class="btn btn-warning float-left mb-4" target="_blank">Cetak</a>
<table class="table table-bordered">
        <tr align="center">
            <th>#</th>
            <th>NIS</th>
            <th>Nama Siswa</th>
            <th>Mata Pelajaran</th>
            <th>Tipe</th>
            <th>Nilai</th>
            <!-- <th>Jenis Kelamin</th> -->
        </tr>
    <?php $no=1; foreach($nilaiFilMap as $row) : ?>
        <tr>
            <td align="center"><?= $no++; ?></td>
            <td align="center"><?= $row->nis; ?></td>
            <td><?= $row->siswa; ?></td>
            <td align="center"><?= $row->mapel; ?></td>
            <td align="center"><?= $row->tipe; ?></td>
            <td align="center" style="font-weight: bold;"><?= $row->nilai; ?></td>
            <!-- <td><?= $row->nama_siswa; ?></td>
            <td><?= $row->lahir_siswa; ?></td>
            <td align="center"><?= $row->tanggal_siswa; ?></td>
            <td align="center"><?= $row->jk_siswa; ?></td> -->
        </tr>
    <?php endforeach; ?>
</table>