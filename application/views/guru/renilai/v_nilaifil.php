<div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
        <!-- DataTales Example --> <br>
    
        <div class="row">
            <div class="col-lg-5">
                <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary"> Filter Data by Mata Pelajaran</h6>
                        </div>
                    <div class="card-body">
                        <form action="" id="FormFilterMapel">
                            <div class="form-row mb-2 ml-4">
                                <div class="col-sm-8">
                                    <label for="mapel" class="form-label">Mata Pelajaran</label>
                                    <select name="nama_mapel" id="mapel" class="form-control">
                                    <option value="0">-- Pilih --</option>
                                        <?php foreach($mapel as $data) { ?>
                                            <option value="<?= $data->kd_mapel; ?>"><?= $data->nama_mapel; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-primary" style="margin-top: 30px;">Tampilkan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary"> Filter Data by Tipe Ujian</h6>
                        </div>

                        <!-- Form Input Jadwal Pelajaran -->
                        <div class="card-body">
                            <form action="" id="FormFilter">
                                <div class="form-row mb-2 ml-4">
                                    <div class="col-sm-8">
                                            <label for="tipe" class="form-label">Tipe Ujian</label>
                                            <select class="form-control" name="tipe" id="tipe">
                                                <option value="0">-- Pilih --</option>
                                                <option value="UTS">UTS</option>
                                                <option value="UAS">UAS</option>
                                                <option value="ULANGAN">ULANGAN</option>
                                                <option value="TUGAS">TUGAS</option>
                                            </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="submit" class="btn btn-primary" style="margin-top: 30px;">Tampilkan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>

            <div class="col-lg-12">
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="form-group" id="result">
                                
                            </div>
                        </div>
                    </div>
            </div>
    </div>
</div>
<!-- /.container-fluid -->
<script>
    $(document).ready(function(){
        $("#FormFilterMapel").submit(function(e) {
            e.preventDefault();
            var id = $("#mapel").val();
            var url = "<?= base_url('Guru/filterNilaiMapel/') ?>" +  id
            // console.log(url);
            $("#result").load(url);
        });
        $("#FormFilter").submit(function(e) {
            e.preventDefault();
            var id = $("#tipe").val();
            var url = "<?= base_url('Guru/filterNilaiTipe/') ?>" +  id
            // console.log(url);
            $("#result").load(url);
        });
    });
</script>