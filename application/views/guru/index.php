<div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
        <!-- DataTales Example --> <br>
    
        <div class="row">

            <div class="col-lg-7">
                <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <form action="<?= base_url('employee/attendance') ?>" method="POST">
                                <div class="form-group">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label for="" class="form-label">Tahun Ajaran</label>
                                            <!-- <input type="password" class="form-control input-default" name="currentpassword" placeholder="Password Lama" required> -->
                                            <select name="tahun_ajaran" id="" class="form-control" required>
                                                <?php foreach($ajaran as $row) : ?>
                                                    <option value="<?= $row->kd_ajaran; ?>"><?= $row->tahun_ajaran; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="form-label">Mata Pelajaran</label>
                                            <select name="nama_mapel" id="" class="form-control" required>
                                                <?php foreach($mapel as $row) : ?>
                                                    <option value="<?= $row['kd_mapel']; ?>"><?= $row['nama_mapel']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <!-- <input type="password" class="form-control input-rounded" name="newpassword" placeholder="Password Baru" required> -->
                                        </div>
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                        <a href="<?= base_url('employee'); ?>" class="btn btn-warning float-right">Hasil Absen</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
    </div>
</div>
<!-- /.container-fluid -->