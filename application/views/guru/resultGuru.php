<table class="table table-bordered">
        <tr align="center">
            <th>#</th>
            <th>NIS</th>
            <th>NIK</th>
            <th>Nama Siswa</th>
            <th>Tempat Lahir</th>
            <th>Tanggal Lahir</th>
            <th>Jenis Kelamin</th>
        </tr>
    <?php $no=1; foreach($siswa as $row) : ?>
        <tr>
            <td align="center"><?= $no++; ?></td>
            <td align="center"><?= $row->nis; ?></td>
            <td align="center"><?= $row->nik_siswa; ?></td>
            <td><?= $row->nama_siswa; ?></td>
            <td><?= $row->lahir_siswa; ?></td>
            <td align="center"><?= $row->tanggal_siswa; ?></td>
            <td align="center"><?= $row->jk_siswa; ?></td>
        </tr>
    <?php endforeach; ?>
</table>