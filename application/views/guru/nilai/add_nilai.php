<div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
        <!-- DataTales Example --> <br>
    
        <div class="row">
            <div class="col-lg-8">
                    <div class="card shadow mb-4">
					<?= $this->session->flashdata('pesan'); ?>
					<form action="<?php echo base_url('nilai/add_nilai'); ?>" method="POST">
                       <!-- <div class="card-header py-3 btn-nilai">
							<button type="submit" class="btn btn-warning">
								<i class="fa fa-plus fa-fw" aria-hidden="true"></i> Input Nilai
							</button>
                        </div> -->
                        <div class="card-body">
                            <div class="form-group" id="result">
                                <table class="table table-bordered">
                                        <tr align="center">
                                            <th>NIS</th>
                                            <th>Nama</th>
                                            <th>Nilai</th>
                                        </tr>
										<?php
											$i = 0;
											foreach ($siswa as $row) {
											?>
											<tr>
												<td align="center"><?php echo $row->nis; ?></td>
												<td><?php echo $row->nama_siswa; ?></td>
												<td>
													<div class="form-group nilai">
														<input type="hidden" name="nis<?php echo $i; ?>" value="<?php echo $row->nis; ?>">
														<input type="text" pattern="^[10-100]{2-3}$" class="form-control" id="nilai" name="nilai<?php echo $i; ?>" required>
													</div>
												</td>
											</tr>
										<?php
										$i++;
										}
										?>
                                </table>
								<input type="hidden" name="kd_mapel" value="<?php echo $mapel->kd_mapel; ?>">
								<input type="hidden" name="id_guru" value="<?php echo $guru->id_guru; ?>">
								<input type="hidden" name="iterasi" value="<?php echo $i; ?>">
									<div class="btn-nilai">
										<button type="submit" class="btn btn-warning">
											<i class="fa fa-plus fa-fw" aria-hidden="true"></i> Input Nilai
										</button>
									</div>
                        	</div>
                    	</div>
					</form>
            </div>
    </div>
</div>
<!-- /.container-fluid -->