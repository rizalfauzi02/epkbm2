<div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
        <!-- DataTales Example --> <br>
    
        <div class="row">

            <div class="col-lg-5">
                <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <!-- <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary"> Filter Data</h6>
                        </div> -->

                        <!-- Form Input Jadwal Pelajaran -->
                        <div class="card-body">
							<form class="add-form" method="post" action="<?php echo base_url('nilai/set_add_nilai'); ?>">
								<div class="form-group">
									<label for="inputMapel">Mata Pelajaran</label>
									<select class="form-control" name="mapel">
										<?php
										foreach ($mapel as $row) {
											?>
											<option value="<?php echo $row['kd_mapel']; ?>">
												<?php echo $row['nama_mapel']; ?>
											</option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label for="inputGuru">Guru</label>
									<select class="form-control" name="guru">
										<?php
										foreach ($guru as $row) {
											?>
											<option value="<?php echo $row['id_guru']; ?>">
												<?php echo $row['nama_guru']; ?>
											</option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label for="inputProgram">Program</label>
									<select class="form-control" name="program">
										<?php
										foreach ($program as $row) {
											?>
											<option value="<?php echo $row['kd_program']; ?>">
												<?php echo $row['nama_program']; ?>
											</option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label for="inputTipe">Jenis Nilai</label>
									<select class="form-control" name="tipe">
										<option value="UTS">UTS</option>
										<option value="UAS">UAS</option>
										<option value="ULANGAN">ULANGAN</option>
										<option value="TUGAS">TUGAS</option>
									</select>
								</div>
								<button type="submit" class="btn btn-secondary">
									Selanjutnya <i class="fa fa-arrow-right fa-fw" aria-hidden="true"></i>
								</button>
							</form>
                        </div>
                    </div>
            </div>
    </div>
</div>
<!-- /.container-fluid -->