<div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
        <!-- DataTales Example --> <br>
    
        <div class="row">
            <div class="col-lg-12">
                    <div class="card shadow mb-4">
                       <div class="card-header py-3">
                            <a href="<?= base_url('nilai/set_add_nilai'); ?>" class="btn btn-warning"><i class="fa fa-plus fa-fw" aria-hidden="true"></i>Input Nilai</a>
                        </div>
                        <div class="card-body">
                            <div class="form-group" id="result">
                                <?= $this->session->flashdata('pesan'); ?>
                                <?php  if(count($nilai) != 0){ ?>
                                    <table class="table table-bordered">
                                            <tr align="center">
                                                <th>#</th>
                                                <th>NIS</th>
                                                <th>Nama</th>
                                                <th>Mata Pelajaran</th>
                                                <th>Tutor / Guru</th>
                                                <th>Program Paket</th>
                                                <th>Jenis Nilai</th>
                                                <th>Nilai</th>
                                                <th>Action</th>
                                            </tr>
                                        <?php $no=1; foreach($nilai as $row) : // var_dump($nilai); die; ?>
                                            
                                            <tr align="center">
                                                <td><?= $no++; ?></td>
                                                <td><?= $row->nis;?></td>
                                                <td><?= $row->siswa; ?></td>
                                                <td><?= $row->mapel; ?></td>
                                                <td><?= $row->guru; ?></td>
                                                <td><?= $row->program; ?></td>
                                                <td><?= $row->tipe; ?></td>
                                                <td><?= $row->nilai; ?></td>
                                                <td>
                                                    <a href="<?= base_url('nilai/dnilai/') . $row->id_nilai; ?>" style="text-decoration:none" 
                                                        class="btn-danger btn-circle btn-hapus">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                <?php    
                                } else {
                                    echo '<button class="btn btn-outline-danger btn-block">Tidak ada Data</button>';
                                } ?>
                            </div>
                        </div>
                    </div>
            </div>
    </div>
</div>
<!-- /.container-fluid -->