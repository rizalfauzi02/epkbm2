<div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
        <!-- DataTales Example --> <br>
    
        <div class="row">

            <div class="col-lg-9">
                <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">
                        <!-- Form Input Jadwal Pelajaran -->
                        <div class="card-body">
                            <form action="" id="FormFilter">
                                <div class="form-row ml-4">
                                    <div class="col-sm-9">
                                            <label for="program" class="form-label">Nama Siswa: <b><?php echo $employee->nama_siswa; ?></b></label>
                                    </div>
                                </div>
                                <div class="col-md-4 ml-3">
                                    <label for="hari" class="form-label">Absen untuk Bulan : </label>
                                    <input type="text" name="month" class="form-control" placeholder="MM" size="10" value="<?php echo $month != '' ? $month : date('m'); ?>">
                                </div>
                                <div class="col-md-4 ml-3 mt-2">
                                    <label for="hari" class="form-label">Tahun : </label>
                                    <input type="text" name="year" class="form-control" placeholder="YYYY" size="10" value="<?php echo $year != '' ? $year : date('Y'); ?>">
                                </div>
                                <div class="col-10 mt-4">
									<input type="hidden" name="empid" value="<?php echo $employee->id_siswa; ?>">
									<button type="submit" class="btn btn-primary" style="border-radius:0%;">Submit</button>
									<a href="<?php echo base_url(); ?>guru" class="btn btn-dark" style="border-radius:0%;">Kembali</a>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
            <div class="col-lg-12">
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="form-group" id="result">
								<table class="table table-bordered">
											<thead>
												<tr align="center">
													<th scope="col">Tanggal</th>
													<th scope="col">Hari</th>
													<th scope="col">Status</th>
													<th scope="col">Action</th>
												</tr>
											</thead>
											<tbody align="center">
												<?php echo form_open("employee/attupdate"); ?>
												<?php 
													$days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
													$arrIndex = 0;
													for ($i = 1; $i <= $days; $i++) {
														$weekday = date("l", strtotime(date($year . "-" . $month . "-" . $i)));
														$date_current = $year . "-" . $month . "-" . ( strlen($i) == 1 ? "0" . $i : $i );
												?>
												<tr <?php echo $weekday == "Sunday" ? "class='text-danger'" : ""; ?>>
													<th scope="row"><?php echo $i; ?></th>
													<td><?php echo $weekday; ?></td>
													<td>
														<?php if (strcmp($date_current, @$attendances[$arrIndex]->attdate) == 0) { ?>
															<select name="attstatus[]" class="custom-select <?php echo $weekday == 'Sunday' ? 'text-danger' : ''; ?>" id="inlineFormCustomSelectPref">
																<option value="P" <?php if (@$attendances[$arrIndex]->attstatus == "P") echo "selected"; ?>>Hadir</option>
																<option value="A" <?php if (@$attendances[$arrIndex]->attstatus == "A") echo "selected"; ?>>Alfa</option>
																<option value="S" <?php if (@$attendances[$arrIndex]->attstatus == "S") echo "selected"; ?>>Sakit</option>
																<option value="I" <?php if (@$attendances[$arrIndex]->attstatus == "I") echo "selected"; ?>>Izin</option>
																<option value="H" <?php if (@$attendances[$arrIndex]->attstatus == "H") echo "selected"; ?>>Libur</option>
															</select>
														<?php 
															$arrIndex++;
															} else { 
														?>
															<select name="attstatus[]" class="custom-select <?php echo $weekday == 'Sunday' ? 'text-danger' : ''; ?>" id="inlineFormCustomSelectPref">
																<option value="P">Hadir</option>
																<option value="A">Alfa</option>
																<option value="S">Sakit</option>
																<option value="I">Izin</option>
																<option value="H" <?php echo $weekday == "Sunday" ? "selected" : ""; ?>>Libur</option>
															</select>
														<?php } ?>
													</td>
													<td>
														<button type="submit" name="save[]" value="<?php echo strlen($i) == 1 ? "0" . $i : $i; ?>" class="btn btn-success" style="border-radius:70px;"><i class="fas fa-check"></i></button>
													</td>
												</tr>
												<?php } ?>
												<input type="hidden" name="empid" value="<?php echo $employee->id_siswa; ?>">
												<input type="hidden" name="year" value="<?php echo $year; ?>">
												<input type="hidden" name="month" value="<?php echo $month; ?>">
												<input type="hidden" name="tahun_ajaran" value="<?php echo $ajaran; ?>">
												<input type="hidden" name="nama_mapel" value="<?php echo $mapel; ?>">
												<?php echo form_close(); ?>
											</tbody>
								</table>
                            </div>
                        </div>
                    </div>
            </div>
    </div>
</div>
<!-- /.container-fluid -->