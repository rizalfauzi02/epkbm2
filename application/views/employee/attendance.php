<div class="card shadow mb-4">
<div class="container mt-4">

	<div class="card-body">
	<div class="row d-flex mb-2">
		<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
			<h3 style="color:#656565"><?= $judul; ?></h3>
		</div>
		<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
			<?php echo form_open("employee/attendance"); ?>
			<div class="form-row">
				<div class="col">
					<input type="text" name="year" class="form-control" placeholder="YYYY" value="<?php echo $year != '' ? $year : date('Y'); ?>">
				</div>
				<div class="col">
					<input type="text" name="month" class="form-control" placeholder="MM" value="<?php echo $month != '' ? $month : date('m'); ?>">
				</div>
				<div class="col-5">
					<button type="submit" class="btn btn-success" style="border-radius:0%;"><i class="fas fa-sort"></i> Filter Bulan</button>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
	</div>

	<div class="row table-responsive">
		<div class="col-10">
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr align="center">
						<th scope="col">#</th>
						<th scope="col">NIS</th>
						<th scope="col">Nama</th>
						<th scope="col">Hadir</th>
						<th scope="col">Alfa</th>
						<th scope="col">Sakit</th>
						<th scope="col">Izin</th>
						<th scope="col">Libur</th>
						<th scope="col">Absen</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						$count = 0;
						for ($i = 0; $i < count($data[0]); $i++) {
					?>
					<tr>
						<th scope="row" width="50"><?php echo ++$count; ?></th>
						<td align="center" width="50"><?php echo $data[1][$i]; ?></td>
						<td><?php echo $data[2][$i]; ?></td>
						<td align="center" width="50"><?php echo $data[3][$i]; ?></td> 
						<td align="center" width="50"><?php echo $data[4][$i]; ?></td>
						<td align="center" width="50"><?php echo $data[5][$i]; ?></td>
						<td align="center" width="50"><?php echo $data[6][$i]; ?></td>
						<td align="center" width="50"><?php echo $data[7][$i]; ?></td>
						<td align="center" width="50"><a href="<?php echo base_url(); ?>employee/attadd/<?php echo $data[0][$i]; ?>/<?php echo $year; ?>/<?php echo $month; ?>/<?php echo $data[8][$i]; ?>/<?php echo $data[9][$i]; ?>"><button class="btn btn-warning btn-sm" style="border-radius:0%;">Mengelola</button></a></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>

<script>
	$(document)
</script>