<div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
                    <!-- DataTales Example --> <br>
    <div class="row">
        <div class="col-lg-10">
        <?= $this->session->flashdata('pesan'); ?>
                    <div class="card shadow mb-4">                        
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
										<tr align="center">
											<th scope="col">#</th>
											<th scope="col">NIS</th>
											<th scope="col">Nama</th>
											<th scope="col">Hadir</th>
											<th scope="col">Alfa</th>
											<th scope="col">Sakit</th>
											<th scope="col">Izin</th>
											<th scope="col">Libur</th>
										</tr>
                                    </thead>
                                    <tbody>
											<?php 
												$count = 0;
												for ($i = 0; $i < count($data[0]); $i++) {
											?>
											<tr>
												<th scope="row" width="50"><?php echo ++$count; ?></th>
												<td align="center" width="50"><?php echo $data[1][$i]; ?></td>
												<td><?php echo $data[2][$i]; ?></td>
												<td align="center" width="50"><?php echo $data[3][$i]; ?></td> 
												<td align="center" width="50"><?php echo $data[4][$i]; ?></td>
												<td align="center" width="50"><?php echo $data[5][$i]; ?></td>
												<td align="center" width="50"><?php echo $data[6][$i]; ?></td>
												<td align="center" width="50"><?php echo $data[7][$i]; ?></td>
											</tr>
											<?php } ?>
                                    </tbody>
                                </table>
            						<a href="<?= base_url('guru'); ?>" class="btn btn-dark float-right mb-4">Kembali</a>
                            </div>
                        </div>
                    </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->