<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Konfirmasi Pendaftaran</title>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url('assets/app-assets/sb2/') ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url('assets/app-assets/sb2/') ?>css/sb-admin-2.min.css" rel="stylesheet">
        <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?= base_url('assets/app-assets/'); ?>sweetalert2/dist/notiflix-2.6.0.min.css">
    <script src="<?= base_url('assets/app-assets/'); ?>sweetalert2/dist/notiflix-2.6.0.min.js"></script>
    <script src="<?= base_url('assets/app-assets/'); ?>sweetalert2/dist/sweetalert2.all.js"></script> 

    <link rel="icon" type="image/jpg" sizes="16x16" href="<?= base_url('assets/app-assets/img/'); ?>logo.png">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Begin Page Content -->
                <div class="container-fluid mt-5">
            <?= $this->session->flashdata('pesan'); ?>
                    <!-- 404 Error Text -->
                    <div class="text-center">
                        <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">BERHASIL MENDAFTAR!</h4>
                        <p class="mb-0">Selanjutnya akan di informasikan melalui email yang sudah di daftarkan!</p>
                        <hr>
                        <p class="mb-0">Atau bisa konfirmasi Pendaftaran ke Nomor dibawah ini: </p>
                        <a href="https://bit.ly/3hqGM4v" target="_blank">+6289664091196</a>
                        </div>
                        <a href="<?= base_url('daftar'); ?>" >Kembali</a>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Rizal Fauzi | <?= date('Y'); ?></span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url('assets/app-assets/sb2/') ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url('assets/app-assets/sb2/') ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url('assets/app-assets/sb2/') ?>vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url('assets/app-assets/sb2/') ?>js/sb-admin-2.min.js"></script>

</body>

</html>