<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Siswa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_siswa');
        $this->load->model('M_jadwal');
        if (count($this->db->get_where('users', ['akses' => 1])->result()) == 0) {
            redirect('reg');
        }
        // mencegah user tidak login
        if($this->session->has_userdata('isLoggin') != true){
            redirect('auth');
        }
    }

    public function index()
    {
        // JUDUL PAGE
            $data['title'] = "Dashboard";
            $data['judul'] = "Dashboard";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => 'active',
                'Profile' => '',
            ];

            // ===> DropDown
            // User
            // $data['menu_drop'] = [
            //     'user_menu' => 'active'
            // ];

        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        // JADWAL
            // menampilkan data
            $data['jadwal'] = $this->M_jadwal->tampilJadwalNoId();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar/siswa', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('siswa/index', $data);
        $this->load->view('templates/footer');
    }

    // =========== USER PROFILE =======
    public function profile()
    {
    // JUDUL PAGE
        $data['title'] = "Profile";
        $data['judul'] = "My Profile";

    // DATA MENU
        // ===> No DropDown
        $data['menu'] = [
            'Dashboard' => '',
            'Profile' => 'active'
        ];

    // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
    $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

    // START PROSES UPDATE PROFILE
    $this->form_validation->set_rules('namalengkap', 'Nama Lengkap', 'required|trim');

    if ($this->form_validation->run() == false) {
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar/siswa', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('siswa/profile', $data);
        $this->load->view('templates/footer');
    } else {
        // Check jika ada gambar yang akan di upload
        $upload_image = $_FILES['gambar']['name'];
        // jika ada gambar yang di upload, maka di check
        if($upload_image) {
            $config['upload_path'] = './assets/img/profile/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|PNG|JPEG';
            $config['max_size']     = '3048';
            
            $this->load->library('upload', $config);
            // Jika gambar sudah lolos check, maka di upload
            if($this->upload->do_upload('gambar')){
                $old_image = $data['user']['gambar'];
                if($old_image != 'default.jpg') {
                    unlink(FCPATH . './assets/img/profile/' . $old_image);
                }
                $new_image = $this->upload->data('file_name');
                $this->db->set('gambar', $new_image);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $namalengkap = $this->input->post('namalengkap');
        $email = $this->input->post('email');
        $username    = $this->input->post('username');
        // $this->M_query->updateProfile($namalengkap, $username);

        $this->db->set('namalengkap', $namalengkap);
        $this->db->set('email', $email);
        $this->db->where('username', $username);
        $this->db->update('users');
        $this->session->set_flashdata('pesan', "
        <script>
            Swal.fire(
                'Berhasil!',
                'Data Berhasil Di Update!',
                'success'
                )
        </script>
        ");
        redirect('siswa/profile');
    }
    // END PROSES UPDATE PROFILE
    }

    public function changePassword()
    {
        $current_password = $this->input->post('currentpassword');
        $new_password = $this->input->post('newpassword');

        if($current_password) {
            // USER
            $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

            if(!password_verify($current_password, $user['password'])) {
                $this->session->set_flashdata('pesan', "
                <script>
                    Swal.fire(
                        'Gagal!',
                        'Password lama tidak sama!',
                        'error'
                        )
                </script>
                ");
                redirect('Siswa/profile');
            } else {
                if($current_password == $new_password){
                    $this->session->set_flashdata('pesan', "
                    <script>
                        Swal.fire(
                            'Gagal!',
                            'Password tidak boleh sama dengan sebelumnya!',
                            'error'
                            )
                    </script>
                    ");
                    redirect('Siswa/profile');
                } else {
                    // PASSWORD YANG BENAR
                    $password_hash = password_hash($new_password, PASSWORD_DEFAULT);
                    $this->db->set('password', $password_hash);
                    $this->db->where('username', $this->session->userdata('username'));
                    $this->db->update('users');
                    $this->session->set_flashdata('pesan', "
                    <script>
                        Swal.fire(
                            'Berhasil!',
                            'Password Berhasil Di Ubah!',
                            'success'
                            )
                    </script>
                    ");
                    redirect('Siswa/profile');
                }
            }
        }
    }
    // =========== END USER PROFILE =====
}