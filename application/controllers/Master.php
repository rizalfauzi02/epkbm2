<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_daftar');
        $this->load->model('M_query');
        $this->load->model('M_jadwal');
        $this->load->model('M_siswa');
        $this->load->model('M_program');
        $this->load->model('M_ajaran');
        $this->load->library('form_validation');
        is_admin();
        if (count($this->db->get_where('users', ['akses' => 1])->result()) == 0) {
            redirect('reg');
        }
        // mencegah user tidak login
        if($this->session->has_userdata('isLoggin') != true){
            redirect('auth');
        }
    }
    // GURU
        public function guru()
        {
            // JUDUL PAGE
                $data['title'] = "Data Master - Guru";
                $data['judul'] = "Data Guru";

            // DATA MENU
                // ===> No DropDown
                $data['menu'] = [
                    'Dashboard' => '',
                    'Pendaftaran' => '',
                    'Jadwal' =>''
                ];

                // ===> DropDown
                // User
                $data['menu_drop'] = [
                    'user_menu' => '',
                    'user_show' => '',
                    'user_admin' => '',
                    'user_guru' => '',
                    'user_siswa' => '',
                    'user_profile' => ''
                ];
                // Data Master
                $data['menu_drop'] += [
                    'master_menu'=> 'active',
                    'master_show' => 'show',
                    'master_guru' => 'active',
                    'master_siswa' => '',
                    'master_mapel' => '',
                    'master_ruangan' => '',
                    'master_program' => '',
                    'master_ajaran' => ''
                ];
                // Laporan
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        // AMBIL DATA
            // $data['guru'] = $this->db->get('guru')->result();
        // END AMBIL

        // PAGINATION 
            $config['base_url'] = 'http://localhost/epkbm2/Master/guru';
            $config['total_rows'] = $this->M_query->countGuru();
            $config['per_page'] = 5;

            // memasukan style pagination
            $config['full_tag_open'] = '<nav><ul class="pagination">';
            $config['full_tag_close'] = '</ul></nav>';

            $config['first_link'] = 'Pertama';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';

            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tag_close'] = '</li>';

            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
            $config['cur_tag_close'] = '</li>';

            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';

            $config['last_link'] = 'Terakhir';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';

            $config['attributes'] = array('class' => 'page-link');
            
            $data['start'] = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data['guru'] = $this->M_query->getPage($config['per_page'], $data['start']);
        // END PAGINATION 

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('master/guru/v_guru', $data);
            $this->load->view('templates/footer');
        }

        public function fguru()
        {
            // JUDUL PAGE
                $data['title'] = "Data Master - Guru";
                $data['judul'] = "Input Data Guru";

            // DATA MENU
                // ===> No DropDown
                $data['menu'] = [
                    'Dashboard' => '',
                    'Pendaftaran' => '',
                    'Jadwal' =>''
                ];

                // ===> DropDown
                // User
                $data['menu_drop'] = [
                    'user_menu' => '',
                    'user_show' => '',
                    'user_admin' => '',
                    'user_guru' => '',
                    'user_siswa' => '',
                    'user_profile' => ''
                ];
                // Data Master
                $data['menu_drop'] += [
                    'master_menu'=> 'active',
                    'master_show' => 'show',
                    'master_guru' => 'active',
                    'master_siswa' => '',
                    'master_mapel' => '',
                    'master_ruangan' => '',
                    'master_program' => '',
                    'master_ajaran' => ''
                ];
                // Laporan
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        // START INPUT FORM VALIDATION
        $this->form_validation->set_rules('nuptk', 'Nuptk', 'required|min_length[11]|is_unique[guru.nuptk]', [
            'is_unique' => 'NUPTK Sudah di daftarkan!',
            'min_length' => 'NUPTK terlalu pendek, Minimal 11!'
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('master/guru/guru_form', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'nuptk' => $this->input->post('nuptk', true),
                'nama_guru' => $this->input->post('nama_guru', true),
                'jk_guru' => $this->input->post('jk_guru', true)
            ];

            $query = $this->db->insert('guru', $data);

            if ($query) {
                $this->session->set_flashdata('pesan', "
                <script>
                    Swal.fire(
                        'Berhasil!',
                        'Data Berhasil Disimpan!',
                        'success'
                        )
                </script>
                ");
                redirect('master/guru');
            } else {
                $this->session->set_flashdata('pesan', "
                        <script>
                            Swal.fire(
                                'Oops!',
                                'Gagal Di simpan!',
                                'error'
                                )
                        </script>
                        ");
                redirect('master/guru');
            }
        }
        // END INPUT
        }

        // EDIT DATA GURU ===========
            // AJAX UPDATE GURU
        public function auguru()
        {
            if ($this->input->is_ajax_request()) {
                $id_guru = $this->input->post('id_guru');
                $guru = $this->db->get_where('guru', ['id_guru' => $id_guru])->row();
                echo json_encode($guru);
            } else {
                exit('No direct script access allowed');
            }
        }
        public function uGuru()
        {
            $data = [
                'nuptk' => $this->input->post('nuptk', true),
                'nama_guru' => $this->input->post('namaguru', true),
                'jk_guru' => $this->input->post('jkguru', true)
            ];

            $this->db->where('id_guru', $this->input->post('idguru'));
            $query = $this->db->update('guru', $data);

            if ($query) {
                $this->session->set_flashdata('pesan', "
                    <script>
                        Swal.fire(
                            'Berhasil!',
                            'Data Berhasil Di Update!',
                            'success'
                            )
                    </script>
                    ");
                redirect('Master/guru');
            } else {
                $this->session->set_flashdata('pesan', "
                    <script>
                        Swal.fire(
                            'Oopss!',
                            'Gagal Di Update!',
                            'error'
                            )
                    </script>
                    ");
                redirect('Master/guru');
            }
        }

    // END GURU

        // DATA MASTER - SISWA ======================================
        public function datsiswa()
        {
            // JUDUL PAGE
                $data['title'] = "Data Master - Siswa";
                $data['judul'] = "Data Siswa";

            // DATA MENU
                // ===> No DropDown
                $data['menu'] = [
                    'Dashboard' => '',
                    'Pendaftaran' => '',
                    'Jadwal' =>''
                ];

                // ===> DropDown
                // User
                $data['menu_drop'] = [
                    'user_menu' => '',
                    'user_show' => '',
                    'user_admin' => '',
                    'user_guru' => '',
                    'user_siswa' => '',
                    'user_profile' => ''
                ];
                // Data Master
                $data['menu_drop'] += [
                    'master_menu'=> 'active',
                    'master_show' => 'show',
                    'master_guru' => '',
                    'master_siswa' => 'active',
                    'master_mapel' => '',
                    'master_ruangan' => '',
                    'master_program' => '',
                    'master_ajaran' => ''
                ];
                // Laporan
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        
        // AMBIL DATA
            $data['siswa'] = $this->db->get('siswa')->result();
            $data['datsiswa'] = $this->M_query->tampilSiswa();
            // var_dump($data['datsiswa']); die;
        // END AMBIL

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('master/siswa/v_siswa', $data);
            $this->load->view('templates/footer');
        // END INPUT
        }

        public function fsiswa() // F = Form. Jadi Fsiswa = Form Siswa
        {
            // JUDUL PAGE
                $data['title'] = "Data Master - Siswa";
                $data['judul'] = "Input Data Siswa";

            // DATA MENU
                // ===> No DropDown
                $data['menu'] = [
                    'Dashboard' => '',
                    'Pendaftaran' => '',
                    'Jadwal' =>''
                ];

                // ===> DropDown
                // User
                $data['menu_drop'] = [
                    'user_menu' => '',
                    'user_show' => '',
                    'user_admin' => '',
                    'user_guru' => '',
                    'user_siswa' => '',
                    'user_profile' => ''
                ];
                // Data Master
                $data['menu_drop'] += [
                    'master_menu'=> 'active',
                    'master_show' => 'show',
                    'master_guru' => '',
                    'master_siswa' => 'active',
                    'master_mapel' => '',
                    'master_ruangan' => '',
                    'master_program' => '',
                    'master_ajaran' => ''
                ];
                // Laporan
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        
        // AMBIL DATA
            $data['siswa'] = $this->db->get('siswa')->result();
            $data['dsiswa'] = $this->M_query->tampilSiswa();
        // END AMBIL

            // class baru
            $siswa = new stdClass();
            $siswa->id_siswa = null;
            $siswa->nis = null;
            $siswa->nik_siswa = null;
            $siswa->nama_siswa = null;
            $siswa->lahir_siswa = null;
            $siswa->tanggal_siswa = null;
            $siswa->jk_siswa = null;
            
            // $daftar = $this->M_daftar->get_all();
            $daftar = $this->M_query->tampilDaftar()->result(); 
            // var_dump($daftar);die;
            $program = $this->M_program->get();

            $swa = array(
                'page'              => 'add',
                'row'               => $siswa,
                'pendaftaran'       => $daftar,
                'program'           => $program
            );
        
            // START INPUT FORM VALIDATION
            $this->form_validation->set_rules('nis', 'NIS', 'is_unique[siswa.nis]' , [
                'is_unique' => 'NIS Sudah Terdaftar!'
            ]);
            $this->form_validation->set_rules('nik_siswa', 'NIK', 'is_unique[siswa.nik_siswa]' , [
                'is_unique' => 'NIK Sudah Terdaftar!'
            ]);

            if ($this->form_validation->run() == false) {
                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar/admin', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('master/siswa/siswa_form', $swa);
                $this->load->view('templates/footer');
            } else {
                $post = $this->input->post(null, TRUE);

                if(isset($_POST['add'])) {
                    $this->M_query->addDatSiswa($post);
                } else {
                    $this->session->set_flashdata('pesan', "
                    <script>
                        Swal.fire(
                            'Oopss!!',
                            'Data Gagal Disimpan!',
                            'error'
                            )
                    </script>
                    ");
                    redirect('master/fsiswa');
                }
        
                if($this->db->affected_rows() > 0){ // PESAN BERHASIL YANG INI
                    $this->session->set_flashdata('pesan', "
                    <script>
                        Swal.fire(
                            'Berhasil!',
                            'Data Berhasil Disimpan!',
                            'success'
                            )
                    </script>
                    ");
                    redirect('master/fsiswa'); // DI BALIKAN KE INPUT FORM, SIAPA TAU USER MAU INPUT BANYAK DATA
                }
            }
            // END INPUT
        }
        // END DATA MASTER - SISWAAAAAAA ======================================
        
        // DATA MASTER - PROGRAM PAKET
        public function program()
        {
            // JUDUL PAGE
                $data['title'] = "Data Program Paket";
                $data['judul'] = "Data Program Paket";

            // DATA MENU
                // ===> No DropDown
                $data['menu'] = [
                    'Dashboard' => '',
                    'Pendaftaran' => '',
                    'Jadwal' =>''
                ];

                // ===> DropDown
                // User
                $data['menu_drop'] = [
                    'user_menu' => '',
                    'user_show' => '',
                    'user_admin' => '',
                    'user_guru' => '',
                    'user_siswa' => '',
                    'user_profile' => ''
                ];
                // Data Master
                $data['menu_drop'] += [
                    'master_menu'=> 'active',
                    'master_show' => 'show',
                    'master_guru' => '',
                    'master_siswa' => '',
                    'master_mapel' => '',
                    'master_ruangan' => '',
                    'master_program' => 'active',
                    'master_ajaran' => ''
                ];
                // Laporan
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        // AMBIL DATA
            $data['program'] = $this->db->get('d_program')->result();
        // END AMBIL

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('master/program/v_program', $data);
            $this->load->view('templates/footer');
        }
        
        public function createProg()
        {
        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        // AMBIL DATA
            $data['program'] = $this->db->get('d_program')->result();
        // END AMBIL
        
        // START FORM VALIDATION (INPUT)

            // NIK HARUS UNIK, TIDAK BOLEH SAMA. TAMBAHKAN NANTI

            $this->form_validation->set_rules('kd_program', 'Kode Program', 'required|is_unique[d_program.kd_program]', [
                'is_unique' => 'Kode Program Sudah Pernah digunakan!'
            ]);
            
            if ($this->form_validation->run() == false) {
            // JUDUL PAGE
                $data['title'] = "Data Program Paket";
                $data['judul'] = "Input Data Program Paket";

            // DATA MENU
                // ===> No DropDown
                $data['menu'] = [
                    'Dashboard' => '',
                    'Pendaftaran' => '',
                    'Jadwal' =>''
                ];

                // ===> DropDown
                // User
                $data['menu_drop'] = [
                    'user_menu' => '',
                    'user_show' => '',
                    'user_admin' => '',
                    'user_guru' => '',
                    'user_siswa' => '',
                    'user_profile' => ''
                ];
                // Data Master
                $data['menu_drop'] += [
                    'master_menu'=> 'active',
                    'master_show' => 'show',
                    'master_guru' => '',
                    'master_siswa' => '',
                    'master_mapel' => '',
                    'master_ruangan' => '',
                    'master_program' => 'active',
                    'master_ajaran' => ''
                ];
                    // Laporan
                    $data['menu_lapor'] = [
                        'laporan_menu'=> '',
                        'laporan_show' => '',
                        'laporan_siswa' => ''
                    ];

                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar/admin', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('master/program/program_form', $data);
                $this->load->view('templates/footer');
            } else {
                $data = array(
                    'kd_program' => $this->input->post('kd_program'),
                    'nama_program' => $this->input->post('nama_program')
                );
        
                $this->M_program->insertProgram($data);
                $this->session->set_flashdata('pesan', "
                    <script>
                    Swal.fire({
                            icon: 'success',
                            title: 'Berhasil!',
                            text: 'Data Berhasil Ditambahkan!',
                            })
                    </script>
                    ");
                redirect('master/program');
            }
        }

    // UPDATE PROGRAM
        // AJAX UPDATE
        public function auprogram()
        {
            if ($this->input->is_ajax_request()) {
                $kd_program = $this->input->post('kd_program');
                $d_program = $this->db->get_where('d_program', ['kd_program' => $kd_program])->row();
                echo json_encode($d_program);
            } else {
                exit('No direct script access allowed');
            }
        }
        // PROSES UPDATE
        public function uprogram()
        {
            $data = [
                    'kd_program' => $this->input->post('kdprogram', true),
                    'nama_program' => $this->input->post('namaprogram', true)
            ];

                $this->db->where('kd_program', $this->input->post('kdprogram'));
                $query = $this->db->update('d_program', $data);

                if ($query) {
                    $this->session->set_flashdata('pesan', "
                        <script>
                            Swal.fire(
                                'Berhasil!',
                                'Data Berhasil Di Update!',
                                'success'
                                )
                        </script>
                        ");
                    redirect('master/program');
                } else {
                    $this->session->set_flashdata('pesan', "
                        <script>
                            Swal.fire(
                                'Oopss!',
                                'Gagal Di Update!',
                                'error'
                                )
                        </script>
                        ");
                    redirect('master/program');
                }
        }
    // END UPDATE PROGRAM
        
        public function dprogram($kd_program)
        {
            $delete = $this->M_program->delete($kd_program);
            
            if($delete){
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Berhasil!',
                    'Data Berhasil Dihapus!',
                    'success'
                    )
            </script>
            ");
            redirect('master/program');
            } else {
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Oops!',
                    'Gagal Di hapus!',
                    'error'
                    )
            </script>
            ");
            redirect('master/program');
            }
        }
        // END PROGRAM PAKET 
    // END DATA MASTER

    // TAHUN AJARAN =======
    public function ajaran()
    {
            // JUDUL PAGE
                $data['title'] = "Data Tahun Ajaran";
                $data['judul'] = "Data Tahun Ajaran";

            // DATA MENU
                // ===> No DropDown
                $data['menu'] = [
                    'Dashboard' => '',
                    'Pendaftaran' => '',
                    'Jadwal' =>''
                ];

                // ===> DropDown
                // User
                $data['menu_drop'] = [
                    'user_menu' => '',
                    'user_show' => '',
                    'user_admin' => '',
                    'user_guru' => '',
                    'user_siswa' => '',
                    'user_profile' => ''
                ];
                // Data Master
                $data['menu_drop'] += [
                    'master_menu'=> 'active',
                    'master_show' => 'show',
                    'master_guru' => '',
                    'master_siswa' => '',
                    'master_mapel' => '',
                    'master_ruangan' => '',
                    'master_program' => '',
                    'master_ajaran' => 'active'
                ];
                // Laporan
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        $data['ajaran'] = $this->db->get('d_ajaran')->result();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar/admin', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('master/ajaran/v_ajaran', $data);
        $this->load->view('templates/footer');   
    }
        public function createAjaran()
        {
        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        
        // START FORM VALIDATION (INPUT)

            // NIK HARUS UNIK, TIDAK BOLEH SAMA. TAMBAHKAN NANTI

            $this->form_validation->set_rules('kd_ajaran', 'Kode Tahun Ajaran', 'required|is_unique[d_ajaran.kd_ajaran]', [
                'is_unique' => 'Kode Tahun Ajaran Sudah Pernah digunakan!'
            ]);
            
            if ($this->form_validation->run() == false) {
            // JUDUL PAGE
                $data['title'] = "Data Tahun Ajaran";
                $data['judul'] = "Input Data Tahun Ajaran";

            // DATA MENU
                // ===> No DropDown
                $data['menu'] = [
                    'Dashboard' => '',
                    'Pendaftaran' => '',
                    'Jadwal' =>''
                ];

                // ===> DropDown
                // User
                $data['menu_drop'] = [
                    'user_menu' => '',
                    'user_show' => '',
                    'user_admin' => '',
                    'user_guru' => '',
                    'user_siswa' => '',
                    'user_profile' => ''
                ];
                // Data Master
                $data['menu_drop'] += [
                    'master_menu'=> 'active',
                    'master_show' => 'show',
                    'master_guru' => '',
                    'master_siswa' => '',
                    'master_mapel' => '',
                    'master_ruangan' => '',
                    'master_program' => '',
                    'master_ajaran' => 'active'
                ];
                    // Laporan
                    $data['menu_lapor'] = [
                        'laporan_menu'=> '',
                        'laporan_show' => '',
                        'laporan_siswa' => ''
                    ];

                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar/admin', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('master/ajaran/ajaran_form', $data);
                $this->load->view('templates/footer');
            } else {
                $data = array(
                    'kd_ajaran' => $this->input->post('kd_ajaran'),
                    'tahun_ajaran' => $this->input->post('tahun_ajaran')
                );
        
                $this->M_ajaran->insertAjaran($data);
                $this->session->set_flashdata('pesan', "
                    <script>
                    Swal.fire({
                            icon: 'success',
                            title: 'Berhasil!',
                            text: 'Data Berhasil Ditambahkan!',
                            })
                    </script>
                    ");
                redirect('master/ajaran');
            }
        }
        // AJAX UPDATE
        public function auajaran()
        {
            if ($this->input->is_ajax_request()) {
                $kd_ajaran = $this->input->post('kd_ajaran');
                $d_ajaran = $this->db->get_where('d_ajaran', ['kd_ajaran' => $kd_ajaran])->row();
                echo json_encode($d_ajaran);
            } else {
                exit('No direct script access allowed');
            }
        }
        // PROSES UPDATE
        public function uajaran()
        {
            $data = [
                    'kd_ajaran' => $this->input->post('kdajaran', true),
                    'tahun_ajaran' => $this->input->post('tahunajaran', true)
            ];

                $this->db->where('kd_ajaran', $this->input->post('kdajaran'));
                $query = $this->db->update('d_ajaran', $data);

                if ($query) {
                    $this->session->set_flashdata('pesan', "
                        <script>
                            Swal.fire(
                                'Berhasil!',
                                'Data Berhasil Di Update!',
                                'success'
                                )
                        </script>
                        ");
                    redirect('master/ajaran');
                } else {
                    $this->session->set_flashdata('pesan', "
                        <script>
                            Swal.fire(
                                'Oopss!',
                                'Gagal Di Update!',
                                'error'
                                )
                        </script>
                        ");
                    redirect('master/ajaran');
                }
        }
    // END UPDATE PROGRAM
    // END TAHUN AJARAN


    // ====== DELETE ======
        // Pendaftaran
        public function ddaftar($id_pendaftaran) 
        {
           $delete = $this->M_daftar->ddaftar($id_pendaftaran);
           
           if($delete){
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Berhasil!',
                    'Data Berhasil Dihapus!',
                    'success'
                    )
            </script>
            ");
            redirect('Admin/daftar');
           } else {
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Oops!',
                    'Gagal Di hapus!',
                    'error'
                    )
            </script>
            ");
            redirect('Admin/daftar');
           }
        }
        // USER - Admin
        public function dadmin($id_users) 
        {
           $delete = $this->M_query->delete($id_users);
           
           if($delete){
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Berhasil!',
                    'Data Berhasil Dihapus!',
                    'success'
                    )
            </script>
            ");
            redirect('Admin/adm');
           } else {
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Oops!',
                    'Gagal Di hapus!',
                    'error'
                    )
            </script>
            ");
            redirect('Admin/adm');
           }
        }
        // USER - Siswa
        public function dsiswa($id_users) 
        {
           $delete = $this->M_query->delete($id_users);
           
           if($delete){
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Berhasil!',
                    'Data Berhasil Dihapus!',
                    'success'
                    )
            </script>
            ");
            redirect('Admin/siswa');
           } else {
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Oops!',
                    'Gagal Di hapus!',
                    'error'
                    )
            </script>
            ");
            redirect('Admin/siswa');
           }
        }
        // Jadwal
        public function djadwal($id_jadwal) 
        {
           $delete = $this->M_query->deljdwl($id_jadwal);
           
           if($delete){
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Berhasil!',
                    'Data Berhasil Dihapus!',
                    'success'
                    )
            </script>
            ");
            redirect('Admin/jadwal');
           } else {
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Oops!',
                    'Gagal Di hapus!',
                    'error'
                    )
            </script>
            ");
            redirect('Admin/jadwal');
           }
        }
        // DATA MASTER - GURU
        public function dguru($id_guru) 
        {
           $delete = $this->M_query->delguru($id_guru);
           
           if($delete){
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Berhasil!',
                    'Data Berhasil Dihapus!',
                    'success'
                    )
            </script>
            ");
            redirect('Admin/guru');
           } else {
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Oops!',
                    'Gagal Di hapus!',
                    'error'
                    )
            </script>
            ");
            redirect('Admin/guru');
           }
        }
        // DATA MASTER - SISWA
        public function delsiswa($id_siswa)
        {
            $delete = $this->M_query->delsiswa($id_siswa);
           
            if($delete){
             $this->session->set_flashdata('pesan', "
             <script>
                 Swal.fire(
                     'Berhasil!',
                     'Data Berhasil Dihapus!',
                     'success'
                     )
             </script>
             ");
             redirect('Admin/datsiswa');
            } else {
             $this->session->set_flashdata('pesan', "
             <script>
                 Swal.fire(
                     'Oops!',
                     'Gagal Di hapus!',
                     'error'
                     )
             </script>
             ");
             redirect('Admin/datsiswa');
            }
        }
        // DATA MASTER - TAHUN AJARAN
        public function dajaran($kd_ajaran)
        {
            $delete = $this->M_ajaran->dajaran($kd_ajaran);
           
            if($delete){
             $this->session->set_flashdata('pesan', "
             <script>
                 Swal.fire(
                     'Berhasil!',
                     'Data Berhasil Dihapus!',
                     'success'
                     )
             </script>
             ");
             redirect('master/ajaran');
            } else {
             $this->session->set_flashdata('pesan', "
             <script>
                 Swal.fire(
                     'Oops!',
                     'Gagal Di hapus!',
                     'error'
                     )
             </script>
             ");
             redirect('master/ajaran');
            }
        }
    // ====== END DELETE ======
}