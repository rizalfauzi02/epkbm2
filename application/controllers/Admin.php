<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_daftar');
        $this->load->model('M_query');
        $this->load->model('M_jadwal');
        $this->load->model('M_siswa');
        $this->load->model('M_program');
        $this->load->library('form_validation');
        is_admin();
        if (count($this->db->get_where('users', ['akses' => 1])->result()) == 0) {
            redirect('reg');
        }
        // mencegah user tidak login
        if($this->session->has_userdata('isLoggin') != true){
            redirect('auth');
        }
    }
    
    public function index()
    {
            // JUDUL PAGE
                $data['title'] = "Dashboard Admin";
                $data['judul'] = "Dashboard";

            // DATA MENU
                // ===> No DropDown
                $data['menu'] = [
                    'Dashboard' => 'active',
                    'Pendaftaran' => '',
                    'Jadwal' =>''
                ];

                // ===> DropDown
                // User
                $data['menu_drop'] = [
                    'user_menu' => '',
                    'user_show' => '',
                    'user_admin' => '',
                    'user_guru' => '',
                    'user_siswa' => '',
                    'user_profile' => ''
                ];
                // Data Master
                $data['menu_drop'] += [
                    'master_menu'=> '',
                    'master_show' => '',
                    'master_guru' => '',
                    'master_siswa' => '',
                    'master_mapel' => '',
                    'master_ruangan' => '',
                    'master_program' => '',
                    'master_ajaran' => ''
                ];
                // Laporan
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // Total Admin
            $data['count_admin'] = count($this->db->get_where('users', ['akses' => 1])->result());
        // TOTAL Siswa
            $data['count_siswa'] = count($this->db->get_where('users', ['akses' => 2])->result());
        // JUMLAH PENDAFTAR
            $data['count_pendaftar'] = count($this->db->get('pendaftaran')->result());
        // JUMLAH DATA JADWAL PELAJARAN
            $data['count_jadwal'] = count($this->db->get('jadwal')->result());
        // JUMLAH DATA MASTER - GURU
            $data['count_mguru'] = count($this->db->get('guru')->result());
        // JUMLAH DATA MASTER - SISWA
            $data['count_msiswa'] = count($this->db->get('siswa')->result());
        // JUMLAH DATA MASTER - MAPEL
            $data['count_mmapel'] = count($this->db->get('d_mapel')->result());
        // JUMLAH DATA MASTER - RUANGAN KELAS
            $data['count_mruangan'] = count($this->db->get('d_ruangan')->result());

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/index', $data);
            $this->load->view('templates/footer');
    }

    // VIEW DATA
    public function daftar()
    {
            // JUDUL PAGE
                $data['title'] = "Data Pendaftaran";
                $data['judul'] = "Data Pendaftaran";

            // DATA MENU
                // ===> No DropDown
                $data['menu'] = [
                    'Dashboard' => '',
                    'Pendaftaran' => 'active',
                    'Jadwal' =>''
                ];

                // ===> DropDown
                // User
                $data['menu_drop'] = [
                    'user_menu' => '',
                    'user_show' => '',
                    'user_admin' => '',
                    'user_guru' => '',
                    'user_siswa' => '',
                    'user_profile' => ''
                ];
                // Data Master
                $data['menu_drop'] += [
                    'master_menu'=> '',
                    'master_show' => '',
                    'master_guru' => '',
                    'master_siswa' => '',
                    'master_mapel' => '',
                    'master_ruangan' => '',
                    'master_program' => '',
                    'master_ajaran' => ''
                ];
                // LAPORAN
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        
        // AMBIL DATA
            $data['pendaftar'] = $this->db->get('pendaftaran')->result();
        // END AMBIL DATA

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/v_daftar', $data);
            $this->load->view('templates/footer');
    }
    // DETAIL DAFTAR
    public function detdaftar()
    {
            // JUDUL PAGE
            $data['title'] = "Detail Pendaftaran | PKBM Ciptamekar";
            $data['judul'] = "Detail Data Pendaftaran";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Pendaftaran' => '',
                'Jadwal' =>''
            ];

            // ===> DropDown
            // User
            $data['menu_drop'] = [
                'user_menu' => '',
                'user_show' => '',
                'user_admin' => '',
                'user_siswa' => '',
                'user_profile' => ''
            ];
            // Data Master
            $data['menu_drop'] += [
                'master_menu'=> '',
                'master_show' => '',
                'master_guru' => '',
                'master_siswa' => '',
                'master_mapel' => '',
                'master_ruangan' => '',
                'master_program' => '',
                'master_ajaran' => ''
            ];
            // LAPORAN
            $data['menu_lapor'] = [
                'laporan_menu'=> '',
                'laporan_show' => '',
                'laporan_siswa' => ''
            ];
    // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
    
    // AMBIL DATA
        $uri = $this->uri->segment(3);
        // $data['detail'] = $this->db->get_where('pendaftaran', ['id_pendaftaran' => $uri])->result();
        $data['detail'] = $this->M_query->getDaftarById($uri)->result();
    // END AMBIL DATA
        // $data['program'] = $this->M_query->tampilProgram();

        $this->load->view('admin/v_detdaftar', $data);
    }
    // END VIEW DATA


    // MANAGEMENT USER

    public function adm()
    {
        // JUDUL PAGE
            $data['title'] = "Data User - Admin";
            $data['judul'] = "Data User: Admin";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Pendaftaran' => '',
                'Jadwal' =>''
            ];
            // ===> DropDown
            // User
            $data['menu_drop'] = [
                'user_menu' => 'active',
                'user_show' => 'show',
                'user_admin' => 'active',
                'user_guru' => '',
                'user_siswa' => '',
                'user_profile' => ''
            ];
            // Data Master
            $data['menu_drop'] += [
                'master_menu'=> '',
                'master_show' => '',
                'master_guru' => '',
                'master_siswa' => '',
                'master_mapel' => '',
                'master_ruangan' => '',
                'master_program' => '',
                'master_ajaran' => ''
            ];
                // LAPORAN
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // END DATA MENU

        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        
        // START DATA ADMIN
            $data['admin'] = $this->db->get_where('users', ['akses' => 1])->result();
        // END DATA ADMIN

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/user/admin', $data);
            $this->load->view('templates/footer');
    }
    // INPUT FORM ADMIN
    public function fadmin()
    {
        // JUDUL PAGE
            $data['title'] = "Data User - Admin";
            $data['judul'] = "Tambah User Admin";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Pendaftaran' => '',
                'Jadwal' =>''
            ];
            // ===> DropDown
            // User
            $data['menu_drop'] = [
                'user_menu' => 'active',
                'user_show' => 'show',
                'user_admin' => 'active',
                'user_guru' => '',
                'user_siswa' => '',
                'user_profile' => ''
            ];
            // Data Master
            $data['menu_drop'] += [
                'master_menu'=> '',
                'master_show' => '',
                'master_guru' => '',
                'master_siswa' => '',
                'master_mapel' => '',
                'master_ruangan' => '',
                'master_program' => '',
                'master_ajaran' => ''
            ];
                // LAPORAN
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // END DATA MENU

        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        
        // START INPUT FORM VALIDATION
        $this->form_validation->set_rules('namalengkap', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]' , [
            'is_unique' => 'Username Sudah digunakan!'
        ]);
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/user/admin_form', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'namalengkap' => $this->input->post('namalengkap', true),
                'username' => $this->input->post('username', true),
                'password' => password_hash($this->input->post('password', true), PASSWORD_DEFAULT),
                'email' => $this->input->post('email', true),
                'akses' => 1,
                'gambar' => 'default.jpg',
                'isActive' => 1
            ];

            $query = $this->db->insert('users', $data);

            if ($query) {
                $this->session->set_flashdata('pesan', "
                <script>
                    Swal.fire(
                        'Berhasil!',
                        'Data Berhasil Disimpan!',
                        'success'
                        )
                </script>
                ");
                redirect('Admin/adm');
            } else {
                $this->session->set_flashdata('pesan', "
                        <script>
                            Swal.fire(
                                'Oops!',
                                'Gagal Di simpan!',
                                'error'
                                )
                        </script>
                        ");
                redirect('Admin/adm');
            }
        }
        // END INPUT
    }
    // END INPUT

    // SISWA
    public function siswa()
    {
        // JUDUL PAGE
            $data['title'] = "Data User - Siswa";
            $data['judul'] = "Data User: Siswa";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Pendaftaran' => '',
                'Jadwal' =>''
            ];
            // ===> DropDown
            // User
            $data['menu_drop'] = [
                'user_menu' => 'active',
                'user_show' => 'show',
                'user_admin' => '',
                'user_guru' => '',
                'user_siswa' => 'active',
                'user_profile' => ''
            ];
            // Data Master
            $data['menu_drop'] += [
                'master_menu'=> '',
                'master_show' => '',
                'master_guru' => '',
                'master_siswa' => '',
                'master_mapel' => '',
                'master_ruangan' => '',
                'master_program' => '',
                'master_ajaran' => ''
            ];
                // LAPORAN
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // END DATA MENU

        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN (WAJIB DI COPY SETIAP METHOD)
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        
        // START DATA SISWA
            $data['siswa'] = $this->db->get_where('users', ['akses' => 2])->result();
        // END DATA SISWA

        // START INPUT FORM VALIDATION
        $this->form_validation->set_rules('namalengkap', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/user/siswa', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'namalengkap' => $this->input->post('namalengkap', true),
                'username' => $this->input->post('username', true),
                'password' => password_hash($this->input->post('password', true), PASSWORD_DEFAULT),
                'email' => $this->input->post('email', true),
                'akses' => 2,
                'gambar' => 'default.jpg',
                'isActive' => 1
            ];

            $query = $this->db->insert('users', $data);

            if ($query) {
                $this->session->set_flashdata('pesan', "
                <script>
                    Swal.fire(
                        'Berhasil!',
                        'Data Berhasil Disimpan!',
                        'success'
                        )
                </script>
                ");
                redirect('Admin/siswa');
            } else {
                $this->session->set_flashdata('pesan', "
                        <script>
                            Swal.fire(
                                'Oops!',
                                'Gagal Di simpan!',
                                'error'
                                )
                        </script>
                        ");
                redirect('Admin/siswa');
            }
        }
        // END INPUT
    }
        // INPUT FORM SISWA
    public function fsiswa()
    {
        // JUDUL PAGE
            $data['title'] = "Data User - Siswa";
            $data['judul'] = "Tambah User Siswa";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Pendaftaran' => '',
                'Jadwal' =>''
            ];
            // ===> DropDown
            // User
            $data['menu_drop'] = [
                'user_menu' => 'active',
                'user_show' => 'show',
                'user_admin' => '',
                'user_guru' => '',
                'user_siswa' => 'active',
                'user_profile' => ''
            ];
            // Data Master
            $data['menu_drop'] += [
                'master_menu'=> '',
                'master_show' => '',
                'master_guru' => '',
                'master_siswa' => '',
                'master_mapel' => '',
                'master_ruangan' => '',
                'master_program' => '',
                'master_ajaran' => ''
            ];
                // LAPORAN
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // END DATA MENU

        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        
        // START INPUT FORM VALIDATION
        $this->form_validation->set_rules('namalengkap', 'Nama Lengkap', 'required');
                $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]' , [
            'is_unique' => 'Username Sudah digunakan!'
        ]);
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/user/siswa_form', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'namalengkap' => $this->input->post('namalengkap', true),
                'username' => $this->input->post('username', true),
                'password' => password_hash($this->input->post('password', true), PASSWORD_DEFAULT),
                'email' => $this->input->post('email', true),
                'akses' => 2,
                'gambar' => 'default.jpg',
                'isActive' => 1
            ];

            $query = $this->db->insert('users', $data);

            if ($query) {
                $this->session->set_flashdata('pesan', "
                <script>
                    Swal.fire(
                        'Berhasil!',
                        'Data Berhasil Disimpan!',
                        'success'
                        )
                </script>
                ");
                redirect('Admin/siswa');
            } else {
                $this->session->set_flashdata('pesan', "
                        <script>
                            Swal.fire(
                                'Oops!',
                                'Gagal Di simpan!',
                                'error'
                                )
                        </script>
                        ");
                redirect('Admin/siswa');
            }
        }
        // END INPUT
    }
    // END SISWA

    // GURU
    public function guru()
    {
            // JUDUL PAGE
                $data['title'] = "Data User - Guru";
                $data['judul'] = "Data User: Guru";

            // DATA MENU
                // ===> No DropDown
                $data['menu'] = [
                    'Dashboard' => '',
                    'Pendaftaran' => '',
                    'Jadwal' =>''
                ];
                // ===> DropDown
                // User
                $data['menu_drop'] = [
                    'user_menu' => 'active',
                    'user_show' => 'show',
                    'user_admin' => '',
                    'user_guru' => 'active',
                    'user_siswa' => '',
                    'user_profile' => ''
                ];
                // Data Master
                $data['menu_drop'] += [
                    'master_menu'=> '',
                    'master_show' => '',
                    'master_guru' => '',
                    'master_siswa' => '',
                    'master_mapel' => '',
                    'master_ruangan' => '',
                    'master_program' => '',
                    'master_ajaran' => ''
                ];
                    // LAPORAN
                    $data['menu_lapor'] = [
                        'laporan_menu'=> '',
                        'laporan_show' => '',
                        'laporan_siswa' => ''
                    ];
            // END DATA MENU

            // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN (WAJIB DI COPY SETIAP METHOD)
                $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
            
            // START DATA GURU
                // $data['guru'] = $this->db->get_where('users', ['akses' => 3])->result();
                // $id = 3;
                $data['guru'] = $this->M_query->tampilProgramUser()->result();
                $program = $this->M_program->get();

                $dat = array(
                    'program'   => $program
                );
            // END DATA GURU

                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar/admin', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('admin/user/guru', $dat);
                $this->load->view('templates/footer');
    }

    public function fguru()
    {
        // JUDUL PAGE
            $data['title'] = "Data User - Guru";
            $data['judul'] = "Tambah User Guru";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Pendaftaran' => '',
                'Jadwal' =>''
            ];
            // ===> DropDown
            // User
            $data['menu_drop'] = [
                'user_menu' => 'active',
                'user_show' => 'show',
                'user_admin' => '',
                'user_guru' => 'active',
                'user_siswa' => '',
                'user_profile' => ''
            ];
            // Data Master
            $data['menu_drop'] += [
                'master_menu'=> '',
                'master_show' => '',
                'master_guru' => '',
                'master_siswa' => '',
                'master_mapel' => '',
                'master_ruangan' => '',
                'master_program' => '',
                'master_ajaran' => ''
            ];
                // LAPORAN
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // END DATA MENU

        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();


            // $daftar = $this->M_daftar->get_all();
            $guru = $this->M_query->tampilGuruUser()->result();
            $program = $this->M_program->get();

            $gru= array(
                'guru'      => $guru,
                'program'   => $program
            );

        // START INPUT FORM VALIDATION
        $this->form_validation->set_rules('namalengkap', 'Nama Lengkap', 'required');
                $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]' , [
            'is_unique' => 'Username Sudah digunakan!'
        ]);
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/user/guru_form', $gru);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'namalengkap' => $this->input->post('namalengkap', true),
                'username' => $this->input->post('username', true),
                'password' => password_hash($this->input->post('password', true), PASSWORD_DEFAULT),
                'email' => $this->input->post('email', true),
                'nama_program' => $this->input->post('nama_program', true),
                'akses' => 3,
                'gambar' => 'default.jpg',
                'isActive' => 1,
            ];

            $query = $this->db->insert('users', $data);

            if ($query) {
                $this->session->set_flashdata('pesan', "
                <script>
                    Swal.fire(
                        'Berhasil!',
                        'Data Berhasil Disimpan!',
                        'success'
                        )
                </script>
                ");
                redirect('Admin/guru');
            } else {
                $this->session->set_flashdata('pesan', "
                        <script>
                            Swal.fire(
                                'Oops!',
                                'Gagal Di simpan!',
                                'error'
                                )
                        </script>
                        ");
                redirect('Admin/guru');
            }
        }
        // END INPUT
    }

    // END MANAGEMENT USER
    public function jadwal()
    {
        // JUDUL PAGE
            $data['title'] = "Jadwal Pelajaran";
            $data['judul'] = "Data Jadwal Pelajaran";
    
        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
               'Dashboard' => '',
               'Pendaftaran' => '',
               'Jadwal' =>'active'
            ];
    
           // ===> DropDown
           // User
            $data['menu_drop'] = [
               'user_menu' => '',
               'user_show' => '',
               'user_admin' => '',
               'user_guru' => '',
               'user_siswa' => '',
               'user_profile' => ''
            ];
            // Data Master
            $data['menu_drop'] += [
                'master_menu'=> '',
                'master_show' => '',
                'master_guru' => '',
                'master_siswa' => '',
                'master_mapel' => '',
                'master_ruangan' => '',
                'master_program' => '',
                'master_ajaran' => ''
            ];
                // LAPORAN
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN (WAJIB DI COPY SETIAP METHOD)
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        
        // AMBIL DATA
        // $data['jadwal'] = $this->db->get('jadwal')->result();
        // $data['jadwal'] = $this->M_jadwal->TampilJadwal();
        $data['program'] = $this->db->get('d_program')->result();
        // END AMBIL DATA
        
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar/admin', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/v_jadwal', $data);
        $this->load->view('templates/footer');
    }
    public function filterJadwal($id)
    {
        // $data['jadwal'] = $this->db->get_where('jadwal', ['nama_program' => $id])->result();
        $data['jadwal']  = $this->M_jadwal->tampilJadwal($id);
        $data['paket'] = $id;
        if(count($data['jadwal']) != 0){
            $this->load->view('Admin/resultJadwal', $data);
        } else {
            echo '<button class="btn btn-danger btn-block">Tidak ada Data</button>';
        }
    }

    // ========================= USER PROFILE ==============
    public function profile()
    {
        // JUDUL PAGE
            $data['title'] = "Profile";
            $data['judul'] = "My Profile";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Pendaftaran' => '',
                'Jadwal' =>''
            ];

            // ===> DropDown
            // User
            $data['menu_drop'] = [
                'user_menu' => 'active',
                'user_show' => 'show',
                'user_admin' => '',
                'user_guru' => '',
                'user_siswa' => '',
                'user_profile' => 'active'
            ];
            // Data Master
            $data['menu_drop'] += [
                'master_menu'=> '',
                'master_show' => '',
                'master_guru' => '',
                'master_siswa' => '',
                'master_mapel' => '',
                'master_ruangan' => '',
                'master_program' => '',
                'master_ajaran' => ''
            ];
                // LAPORAN
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
    // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        // START PROSES UPDATE PROFILE
        $this->form_validation->set_rules('namalengkap', 'Nama Lengkap', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/user/profile', $data);
            $this->load->view('templates/footer');
        } else {
            // Check jika ada gambar yang akan di upload
            $upload_image = $_FILES['gambar']['name'];
            // jika ada gambar yang di upload, maka di check
            if($upload_image) {
                $config['upload_path'] = './assets/app-assets/img/profile/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|PNG|JPEG';
                $config['max_size']     = '3048';
                
                $this->load->library('upload', $config);
                // Jika gambar sudah lolos check, maka di upload
                if($this->upload->do_upload('gambar')){
                    $old_image = $data['user']['gambar'];
                    if($old_image != 'default.jpg') {
                        unlink(FCPATH . './assets/app-assets/img/profile/' . $old_image);
                    }
                    $new_image = $this->upload->data('file_name');
                    $this->db->set('gambar', $new_image);
                } else {
                    echo $this->upload->display_errors();
                }
            }
            $namalengkap = $this->input->post('namalengkap');
            $email = $this->input->post('email');
            $username    = $this->input->post('username');
            // $this->M_query->updateProfile($namalengkap, $username);

            $this->db->set('namalengkap', $namalengkap);
            $this->db->set('email', $email);
            $this->db->where('username', $username);
            $this->db->update('users');
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Berhasil!',
                    'Data Berhasil Di Update!',
                    'success'
                    )
            </script>
            ");
            redirect('Admin/profile');
        }
        // END PROSES UPDATE PROFILE
    }
    public function changePassword()
    {
        $current_password = $this->input->post('currentpassword');
        $new_password = $this->input->post('newpassword');

        if($current_password) {
            // USER
            $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

            if(!password_verify($current_password, $user['password'])) {
                $this->session->set_flashdata('pesan', "
                <script>
                    Swal.fire(
                        'Gagal!',
                        'Password lama tidak sama!',
                        'error'
                        )
                </script>
                ");
                redirect('Admin/profile');
            } else {
                if($current_password == $new_password){
                    $this->session->set_flashdata('pesan', "
                    <script>
                        Swal.fire(
                            'Gagal!',
                            'Password tidak boleh sama dengan sebelumnya!',
                            'error'
                            )
                    </script>
                    ");
                    redirect('Admin/profile');
                } else {
                    // PASSWORD YANG BENAR
                    $password_hash = password_hash($new_password, PASSWORD_DEFAULT);
                    $this->db->set('password', $password_hash);
                    $this->db->where('username', $this->session->userdata('username'));
                    $this->db->update('users');
                    $this->session->set_flashdata('pesan', "
                    <script>
                        Swal.fire(
                            'Berhasil!',
                            'Password Berhasil Di Ubah!',
                            'success'
                            )
                    </script>
                    ");
                    redirect('Admin/profile');
                }
            }
        }
    }
    // ========================= END USER PROFILE ==============

    // ====== DELETE ======
        // Pendaftaran
        public function ddaftar($id_pendaftaran) 
        {
           $delete = $this->M_daftar->ddaftar($id_pendaftaran);
           
           if($delete){
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Berhasil!',
                    'Data Berhasil Dihapus!',
                    'success'
                    )
            </script>
            ");
            redirect('Admin/daftar');
           } else {
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Oops!',
                    'Gagal Di hapus!',
                    'error'
                    )
            </script>
            ");
            redirect('Admin/daftar');
           }
        }
        // USER - Admin
        public function dadmin($id_users) 
        {
           $delete = $this->M_query->delete($id_users);
           
           if($delete){
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Berhasil!',
                    'Data Berhasil Dihapus!',
                    'success'
                    )
            </script>
            ");
            redirect('Admin/adm');
           } else {
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Oops!',
                    'Gagal Di hapus!',
                    'error'
                    )
            </script>
            ");
            redirect('Admin/adm');
           }
        }
        // USER - Siswa
        public function dsiswa($id_users) 
        {
           $delete = $this->M_query->delete($id_users);
           
           if($delete){
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Berhasil!',
                    'Data Berhasil Dihapus!',
                    'success'
                    )
            </script>
            ");
            redirect('Admin/siswa');
           } else {
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Oops!',
                    'Gagal Di hapus!',
                    'error'
                    )
            </script>
            ");
            redirect('Admin/siswa');
           }
        }
        // USER - Guru
        public function dguruUser($id_users) 
        {
           $delete = $this->M_query->delete($id_users);
           
           if($delete){
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Berhasil!',
                    'Data Berhasil Dihapus!',
                    'success'
                    )
            </script>
            ");
            redirect('Admin/guru');
           } else {
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Oops!',
                    'Gagal Di hapus!',
                    'error'
                    )
            </script>
            ");
            redirect('Admin/guru');
           }
        }
        // Jadwal
        public function djadwal($id_jadwal) 
        {
           $delete = $this->M_query->deljdwl($id_jadwal);
           
           if($delete){
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Berhasil!',
                    'Data Berhasil Dihapus!',
                    'success'
                    )
            </script>
            ");
            redirect('Admin/jadwal');
           } else {
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Oops!',
                    'Gagal Di hapus!',
                    'error'
                    )
            </script>
            ");
            redirect('Admin/jadwal');
           }
        }
        // DATA MASTER - GURU
        public function dguru($id_guru) 
        {
           $delete = $this->M_query->delguru($id_guru);
           
           if($delete){
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Berhasil!',
                    'Data Berhasil Dihapus!',
                    'success'
                    )
            </script>
            ");
            redirect('master/guru');
           } else {
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Oops!',
                    'Gagal Di hapus!',
                    'error'
                    )
            </script>
            ");
            redirect('master/guru');
           }
        }
        // DATA MASTER - SISWA
        public function delsiswa($id_siswa)
        {
            $delete = $this->M_query->delsiswa($id_siswa);
           
            if($delete){
             $this->session->set_flashdata('pesan', "
             <script>
                 Swal.fire(
                     'Berhasil!',
                     'Data Berhasil Dihapus!',
                     'success'
                     )
             </script>
             ");
             redirect('master/datsiswa');
            } else {
             $this->session->set_flashdata('pesan', "
             <script>
                 Swal.fire(
                     'Oops!',
                     'Gagal Di hapus!',
                     'error'
                     )
             </script>
             ");
             redirect('master/datsiswa');
            }
        }
    // ====== END DELETE ======

    // UPDATE USER ========================
        // ADMIN
        
        // AJAX UPDATE ADMIN
    public function auadmin()
    {
        if ($this->input->is_ajax_request()) {
            $id_users = $this->input->post('id_users');
            $users = $this->db->get_where('users', ['id_users' => $id_users])->row();
            echo json_encode($users);
        } else {
            exit('No direct script access allowed');
        }
    }
        // PROSES UPDATE
    public function uadmin()
    {
        if ($this->input->post('passwordadmin')) {
            $data = [
                'namalengkap' => $this->input->post('namalengkapadmin', true),
                'email' => $this->input->post('emailadmin', true),
                'username' => $this->input->post('usernameadmin', true),
                'password' => password_hash($this->input->post('passwordadmin', true), PASSWORD_DEFAULT)
            ];
        } else {
            $data = [
                'namalengkap' => $this->input->post('namalengkapadmin', true),
                'username' => $this->input->post('usernameadmin', true),
                'email' => $this->input->post('emailadmin', true)
            ];
        }

        $this->db->where('id_users', $this->input->post('idusers'));
        $query = $this->db->update('users', $data);

        if ($query) {
            $this->session->set_flashdata('pesan', "
                <script>
                    Swal.fire(
                        'Berhasil!',
                        'Data Berhasil Di Update!',
                        'success'
                        )
                </script>
                ");
            redirect('Admin/adm');
        } else {
            $this->session->set_flashdata('pesan', "
                <script>
                    Swal.fire(
                        'Oopss!',
                        'Gagal Di Update!',
                        'error'
                        )
                </script>
                ");
            redirect('Admin/adm');
        }
    }


    // SISWA
        // AJAX UPDATE SISWA
    public function ausiswa()
    {
        if ($this->input->is_ajax_request()) {
            $id_users = $this->input->post('id_users');
            $users = $this->db->get_where('users', ['id_users' => $id_users])->row();
            echo json_encode($users);
        } else {
            exit('No direct script access allowed');
        }
    }
    public function usiswa()
    {
        if ($this->input->post('passwordsiswa')) {
            $data = [
                'namalengkap' => $this->input->post('namalengkapsiswa', true),
                'email' => $this->input->post('emailsiswa', true),
                'username' => $this->input->post('usernamesiswa', true),
                'password' => password_hash($this->input->post('passwordsiswa', true), PASSWORD_DEFAULT)
            ];
        } else {
            $data = [
                'namalengkap' => $this->input->post('namalengkapsiswa', true),
                'username' => $this->input->post('usernamesiswa', true),
                'email' => $this->input->post('emailsiswa', true)
            ];
        }

        $this->db->where('id_users', $this->input->post('idusers'));
        $query = $this->db->update('users', $data);

        if ($query) {
            $this->session->set_flashdata('pesan', "
                <script>
                    Swal.fire(
                        'Berhasil!',
                        'Data Berhasil Di Update!',
                        'success'
                        )
                </script>
                ");
            redirect('Admin/siswa');
        } else {
            $this->session->set_flashdata('pesan', "
                <script>
                    Swal.fire(
                        'Oopss!',
                        'Gagal Di Update!',
                        'error'
                        )
                </script>
                ");
            redirect('Admin/siswa');
        }
    }
    // GURU
        // AJAX UPDATE GURU
    public function auguru()
    {
        if ($this->input->is_ajax_request()) {
            $id_users = $this->input->post('id_users');
            $users = $this->db->get_where('users', ['id_users' => $id_users])->row();
            echo json_encode($users);
        } else {
            exit('No direct script access allowed');
        }
    }
    public function uguru()
    {
        if ($this->input->post('passwordguru')) {
            $data = [
                'namalengkap' => $this->input->post('namalengkapguru', true),
                'email' => $this->input->post('emailguru', true),
                'nama_program' => $this->input->post('namaprogram', true),
                'username' => $this->input->post('usernameguru', true),
                'password' => password_hash($this->input->post('passwordguru', true), PASSWORD_DEFAULT)
            ];
        } else {
            $data = [
                'namalengkap' => $this->input->post('namalengkapguru', true),
                'username' => $this->input->post('usernameguru', true),
                'email' => $this->input->post('emailguru', true),
                'nama_program' => $this->input->post('namaprogram', true)
            ];
        }

        $this->db->where('id_users', $this->input->post('idusers'));
        $query = $this->db->update('users', $data);

        if ($query) {
            $this->session->set_flashdata('pesan', "
                <script>
                    Swal.fire(
                        'Berhasil!',
                        'Data Berhasil Di Update!',
                        'success'
                        )
                </script>
                ");
            redirect('Admin/guru');
        } else {
            $this->session->set_flashdata('pesan', "
                <script>
                    Swal.fire(
                        'Oopss!',
                        'Gagal Di Update!',
                        'error'
                        )
                </script>
                ");
            redirect('Admin/guru');
        }
    }
    // END UPDATE USER =====
}