<?php
class Nilai extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('nilai_model');
		$this->load->model('M_mapel');
		$this->load->model('M_program');
		$this->load->model('M_query');
		// $this->load->model('matakuliah_model');
		// $this->load->model('mahasiswa_model');
	}

	public function index()
	{
        // JUDUL PAGE
            $data['title'] = "Data Nilai";
            $data['judul'] = "Kelola Nilai";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Profile'   => '',
                'Absensi'   => '',
                'Nilai'     => 'active'
            ];

            // ===> DropDown
            $data['menu_lapor'] = [
                'laporan_menu'=> '', // active
                'laporan_show' => '', // show
                'laporan_absen' => '', // active
                'laporan_nilai' => '' // active
            ];

        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

		$data['nilai'] = $this->nilai_model->get_nilai();
		// var_dump($data['nilai']); die;
		$data['title'] = 'Kelola Nilai';
		$this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar/guru', $data);
        $this->load->view('templates/topbar', $data);
		$this->load->view('guru/nilai/nilai', $data);
		$this->load->view('templates/footer');
	}

	public function set_add_nilai()
	{
		// if ($this->session->userdata('hak') != 'admin') {
		// 	redirect('login');
		// }
        // JUDUL PAGE
            $data['title'] = "Data Nilai";
            // $data['judul'] = "Kelola Nilai";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Profile'   => '',
                'Absensi'   => '',
                'Nilai'     => 'active'
            ];

            // ===> DropDown
            $data['menu_lapor'] = [
                'laporan_menu'=> '', // active
                'laporan_show' => '', // show
                'laporan_absen' => '', // active
                'laporan_nilai' => '' // active
            ];

        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

		$this->form_validation->set_rules('mapel', 'Mata Pelajaran', 'required');
		$this->form_validation->set_rules('guru', 'Guru', 'required');
		$this->form_validation->set_rules('program', 'Program Paket', 'required');

		if ($this->form_validation->run() === FALSE) {
			$data['mapel'] = $this->M_mapel->get();
			$data['guru'] = $this->M_query->get();
			$data['program'] = $this->M_program->get(); 
			$data['judul'] = 'Pengaturan Input Nilai';
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar/guru', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('guru/nilai/set_add_nilai', $data);
			$this->load->view('templates/footer');
		} else {
			//Start untuk pesan
			$kd_mapel = $this->input->post('mapel');
			$id_guru = $this->input->post('guru');
			$kd_program = $this->input->post('program');
			$tipe	= $this->input->post('tipe');
			// var_dump($kd_program);die;
			$data['mapel'] = $this->M_mapel->get_mapel_id($kd_mapel);
			$data['guru'] = $this->M_query->get_guru_id($id_guru);
			$data['program'] = $this->M_program->get_program_id($kd_program);

			//End untuk pesan

			$data['siswa'] = $this->nilai_model->set_add_nilai();
			// var_dump($data['siswa']);die;
			$data['judul'] = 'Input Nilai';
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar/guru', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('guru/nilai/add_nilai', $data);
			$this->load->view('templates/footer');
		}
	}

	public function add_nilai()
	{
		// var_dump($_POST); die;
		$kd_mapel = $this->input->post('kd_mapel');
		$id_guru = $this->input->post('id_guru');
		$iterasi = $this->input->post('iterasi');
		// var_dump($iterasi); die;

		for ($i=0; $i<$iterasi; $i++) {
			$var_nis = "nis".$i;
			$var_nilai = "nilai".$i;

			$nis = $this->input->post($var_nis);
			$nilai = $this->input->post($var_nilai);

			$this->nilai_model->add_nilai($nis, $kd_mapel, $id_guru, $nilai);
		}

		// $this->session->set_flashdata('input_nilai', 'Nilai berhasil diinput.');
        $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Berhasil!',
                    'Data Berhasil Disimpan!',
                    'success'
                    )
                </script>
          ");
		redirect('nilai');
	}

	public function edit_nilai($id)
	{
		// if ($this->session->userdata('hak') != 'admin') {
		// 	redirect('login');
		// }

		$this->form_validation->set_rules('nilai', 'Nilai', 'required');

		if ($this->form_validation->run() === FALSE) {
			$data['nilai'] = $this->nilai_model->get_nilai_id($id);
			$data['title'] = 'Edit Nilai';
			$this->load->view('templates/header', $data);
			$this->load->view('admin/menu', $data);
			$this->load->view('nilai/edit_nilai', $data);
			$this->load->view('templates/footer');
		} else {
			$this->nilai_model->edit_nilai($id);
			$this->session->set_flashdata('edit_nilai', 'Nilai berhasil diedit.');
			redirect('nilai');
		}
	}

	public function dnilai($id)
	{
		$this->nilai_model->delete_nilai($id);
		// $this->session->set_flashdata('delete_nilai', 'Nilai berhasil didelete.');
		redirect('nilai');
	}
}