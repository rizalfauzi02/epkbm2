<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Guru extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_siswa');
        $this->load->model('M_mapel');
        $this->load->model('M_query');
        $this->load->model('M_ajaran');
        $this->load->model('M_program');
        if (count($this->db->get_where('users', ['akses' => 1])->result()) == 0) {
            redirect('reg');
        }
        // mencegah user tidak login
        if($this->session->has_userdata('isLoggin') != true){
            redirect('auth');
        }
    }

    public function dashboard()
    {
        // JUDUL PAGE
            $data['title'] = "Guru | PKBM Ciptamekar";
            $data['judul'] = "SELAMAT DATANG";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Profile'   => '',
                'Absensi'   => '',
                'Nilai'     => ''
            ];

            // ===> DropDown
            $data['menu_lapor'] = [
                'laporan_menu'=> '', // active
                'laporan_show' => '', // show
                'laporan_absen' => '', // active
                'laporan_nilai' => '' // active
            ];

        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar/guru', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('guru/dash', $data);
        $this->load->view('templates/footer');
        // redirect('employee/attendance');
    }

    public function index()
    {
        // JUDUL PAGE
            $data['title'] = "Absensi | PKBM Ciptamekar";
            $data['judul'] = "INPUT DATA ABSENSI";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Profile'   => '',
                'Absensi'   => 'active',
                'Nilai'     => ''
            ];

            // ===> DropDown
             $data['menu_lapor'] = [
                'laporan_menu'=> '', // active
                'laporan_show' => '', // show
                'laporan_absen' => '', // active
                'laporan_nilai' => '' // active
            ];

        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        // JADWAL
            // menampilkan data
            // $data['jadwal'] = $this->M_jadwal->tampilJadwalNoId();
            // $data['datsiswa'] = $this->M_query->tampilSiswa();
            $data['mapel'] = $this->M_mapel->get();
            $data['ajaran'] = $this->M_ajaran->get();
            // var_dump($data['mapel']); die;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar/guru', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('guru/index', $data);
        $this->load->view('templates/footer');
        // redirect('employee/attendance');
        
    }

    // =========== USER PROFILE =======
    public function profile()
    {
    // JUDUL PAGE
        $data['title'] = "Profile";
        $data['judul'] = "My Profile";

    // DATA MENU
        // ===> No DropDown
        $data['menu'] = [
            'Dashboard' => '',
            'Profile'   => 'active',
            'Absensi'   => '',
            'Nilai'     => ''
        ];
            $data['menu_lapor'] = [
                'laporan_menu'=> '', // active
                'laporan_show' => '', // show
                'laporan_absen' => '', // active
                'laporan_nilai' => '' // active
            ];

    // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
    $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

    // START PROSES UPDATE PROFILE
    $this->form_validation->set_rules('namalengkap', 'Nama Lengkap', 'required|trim');

    if ($this->form_validation->run() == false) {
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar/guru', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('guru/profile', $data);
        $this->load->view('templates/footer');
    } else {
        // Check jika ada gambar yang akan di upload
        $upload_image = $_FILES['gambar']['name'];
        // jika ada gambar yang di upload, maka di check
        if($upload_image) {
            $config['upload_path'] = './assets/app-assets/img/profile/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|PNG|JPEG';
            $config['max_size']     = '3048';
            
            $this->load->library('upload', $config);
            // Jika gambar sudah lolos check, maka di upload
            if($this->upload->do_upload('gambar')){
                $old_image = $data['user']['gambar'];
                if($old_image != 'default.jpg') {
                    unlink(FCPATH . './assets/app-assets/img/profile/' . $old_image);
                }
                $new_image = $this->upload->data('file_name');
                $this->db->set('gambar', $new_image);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $namalengkap = $this->input->post('namalengkap');
        $email = $this->input->post('email');
        $username    = $this->input->post('username');
        // $this->M_query->updateProfile($namalengkap, $username);

        $this->db->set('namalengkap', $namalengkap);
        $this->db->set('email', $email);
        $this->db->where('username', $username);
        $this->db->update('users');
        $this->session->set_flashdata('pesan', "
        <script>
            Swal.fire(
                'Berhasil!',
                'Data Berhasil Di Update!',
                'success'
                )
        </script>
        ");
        redirect('Guru/profile');
    }
    // END PROSES UPDATE PROFILE
    }

    public function changePassword()
    {
        $current_password = $this->input->post('currentpassword');
        $new_password = $this->input->post('newpassword');

        if($current_password) {
            // USER
            $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

            if(!password_verify($current_password, $user['password'])) {
                $this->session->set_flashdata('pesan', "
                <script>
                    Swal.fire(
                        'Gagal!',
                        'Password lama tidak sama!',
                        'error'
                        )
                </script>
                ");
                redirect('Guru/profile');
            } else {
                if($current_password == $new_password){
                    $this->session->set_flashdata('pesan', "
                    <script>
                        Swal.fire(
                            'Gagal!',
                            'Password tidak boleh sama dengan sebelumnya!',
                            'error'
                            )
                    </script>
                    ");
                    redirect('Guru/profile');
                } else {
                    // PASSWORD YANG BENAR
                    $password_hash = password_hash($new_password, PASSWORD_DEFAULT);
                    $this->db->set('password', $password_hash);
                    $this->db->where('username', $this->session->userdata('username'));
                    $this->db->update('users');
                    $this->session->set_flashdata('pesan', "
                    <script>
                        Swal.fire(
                            'Berhasil!',
                            'Password Berhasil Di Ubah!',
                            'success'
                            )
                    </script>
                    ");
                    redirect('Guru/profile');
                }
            }
        }
    }
    // =========== END USER PROFILE =====

    // PROSES ABSENSI
    public function prosesAbsensi()
    {
        // var_dump($_POST); die;
        $data = array(
            'tahun_ajaran'   => $this->input->post('tahun_ajaran'),
            'nama_mapel'      => $this->input->post('nama_mapel')
        );
        // var_dump($data); die;
        $this->db->insert('attendance', $data);
        $this->session->set_flashdata('pesan', "
        <script>
            Swal.fire(
                'Berhasil!',
                'Data Absen Berhasil Disimpan!',
                'success'
                )
        </script>
        ");
        redirect('employee/attendance');
    }
    // public function prosesAbsensi()
    // {
    //     // var_dump($_POST); die;
    //     $data = array(
    //         'tanggal_absen'     => $this->input->post('tanggal_absen'),
    //         'hari_absen'        => $this->input->post('hari_absen'),
    //         'nis'               => $this->input->post('nis'),
    //         'nama_siswa'        => $this->input->post('nama_siswa'),
    //         'nama_program'      => $this->input->post('nama_program'),
    //         'status_absen'      => $this->input->post('status_absen')
    //     );
    //     // var_dump($data); die;
    //     $this->db->insert('absensi', $data);
    //     $this->session->set_flashdata('pesan', "
    //     <script>
    //         Swal.fire(
    //             'Berhasil!',
    //             'Data Absen Berhasil Disimpan!',
    //             'success'
    //             )
    //     </script>
    //     ");
    //     redirect('guru');
    // }

    // VIEW REKAP === ABSENSI
    public function absensiFil()
    {
        // JUDUL PAGE
            $data['title'] = "Rekap Absensi";
            $data['judul'] = "Rekap Data Absensi";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Profile'   => '',
                'Absensi'   => '',
                'Nilai'     => ''
            ];

            // ===> DropDown
            $data['menu_lapor'] = [
                'laporan_menu'=> 'active', // active
                'laporan_show' => 'show', // show
                'laporan_absen' => 'active', // active
                'laporan_nilai' => '' // active
            ];

        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        $data['ajaran'] = $this->db->get('d_ajaran')->result();
        $data['mapel'] = $this->db->get('d_mapel')->result();
        // $id = $this->db->get_where('users', ['nama_program' => $this->session->userdata('nama_program')])->row_array();
        // var_dump($user); die;
        // $data['mapel'] = $this->M_mapel->getWhere($id);
        // $data['program'] = $this->M_program->getWhere($id);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar/guru', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('guru/absensi/v_absenfil', $data);
        $this->load->view('templates/footer');
    }

    // FILTER AJARAN
    public function filterAjaran($id)
    {
    //   $data['ajaran'] = $this->db->get_where('attendance', ['tahun_ajaran' => $id])->result();
    //   $data['ajaran']= $this->Employee_model->attendanceAjaran($id);
        $this->db->select('*');
        $this->db->from('attendance');
        $this->db->join('d_mapel','d_mapel.kd_mapel = attendance.nama_mapel');		
        $this->db->join('d_ajaran','d_ajaran.kd_ajaran = attendance.tahun_ajaran');
        $this->db->join('siswa','siswa.id_siswa = attendance.empid');
        $this->db->where('attendance.tahun_ajaran', $id);
        $data['ajaran'] = $this->db->get()->result();
    //   var_dump($data['ajaran']); die;
      $data['thn'] = $id;
      if(count($data['ajaran']) != 0){
        $this->load->view('guru/absensi/resultAjaran', $data);
      } else {
        echo '<button class="btn btn-danger btn-block">Tidak ada Data</button>';
      }
    }

    // FILTER MAPEL
    public function filterMapel($id)
    {
    //   $data['ajaran'] = $this->db->get_where('attendance', ['tahun_ajaran' => $id])->result();
    //   $data['ajaran']= $this->Employee_model->attendanceAjaran($id);
        $this->db->select('*');
        $this->db->from('attendance');
        $this->db->join('d_mapel','d_mapel.kd_mapel = attendance.nama_mapel');		
        $this->db->join('d_ajaran','d_ajaran.kd_ajaran = attendance.tahun_ajaran');
        $this->db->join('siswa','siswa.id_siswa = attendance.empid');
        $this->db->where('attendance.nama_mapel', $id);
        $data['mapel'] = $this->db->get()->result();
        // var_dump($data['mapel']); die;
      $data['mpl'] = $id;
      if(count($data['mapel']) != 0){
        $this->load->view('guru/absensi/resultMapel', $data);
      } else {
        echo '<button class="btn btn-danger btn-block">Tidak ada Data</button>';
      }
    }
    // END REKAP === ABSENSI

    // VIEW REKAP === NILAI ==============================
    public function NilaiFil()
    {
        // JUDUL PAGE
            $data['title'] = "Rekap Nilai";
            $data['judul'] = "Rekap Data Nilai";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Profile'   => '',
                'Absensi'   => '',
                'Nilai'     => ''
            ];

            // ===> DropDown
            $data['menu_lapor'] = [
                'laporan_menu'=> 'active', // active
                'laporan_show' => 'show', // show
                'laporan_absen' => '', // active
                'laporan_nilai' => 'active' // active
            ];

        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        $data['ajaran'] = $this->db->get('d_ajaran')->result();
        $data['mapel'] = $this->db->get('d_mapel')->result();
        // $id = $this->db->get_where('users', ['nama_program' => $this->session->userdata('nama_program')])->row_array();
        // var_dump($user); die;
        // $data['mapel'] = $this->M_mapel->getWhere($id);
        // $data['program'] = $this->M_program->getWhere($id);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar/guru', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('guru/renilai/v_nilaifil', $data);
        $this->load->view('templates/footer');
    }

    // FILTER MAPEL
    public function filterNilaiMapel($id)
    {
        // $data['nilaiFilMap'] = $this->Nilai_model->get_nilai_byid($id);
		$this->db->select('nilai.*,
			siswa.nama_siswa as siswa,
			guru.nama_guru as guru,
			d_mapel.nama_mapel as mapel,
			d_program.nama_program as program');
		$this->db->from('nilai');
		$this->db->join('siswa', 'siswa.nis= nilai.nis');
		$this->db->join('d_mapel', 'd_mapel.kd_mapel = nilai.kd_mapel');
		$this->db->join('guru', 'guru.id_guru = nilai.id_guru');
		$this->db->join('d_program', 'd_program.kd_program = nilai.kd_program');
		$this->db->where('nilai.kd_mapel', $id);
		$data['nilaiFilMap'] = $this->db->get()->result();
        // var_dump($data['nilaiFilMap']); die;
      $data['NilaiMap'] = $id;
      if(count($data['nilaiFilMap']) != 0){
        $this->load->view('guru/renilai/resultNilaiMapel', $data);
      } else {
        echo '<button class="btn btn-danger btn-block">Tidak ada Data</button>';
      }
    }

    // FILTER TIPE
    public function filterNilaiTipe($id)
    {
		$this->db->select('nilai.*,
			siswa.nama_siswa as siswa,
			guru.nama_guru as guru,
			d_mapel.nama_mapel as mapel,
			d_program.nama_program as program');
		$this->db->from('nilai');
		$this->db->join('siswa', 'siswa.nis= nilai.nis');
		$this->db->join('d_mapel', 'd_mapel.kd_mapel = nilai.kd_mapel');
		$this->db->join('guru', 'guru.id_guru = nilai.id_guru');
		$this->db->join('d_program', 'd_program.kd_program = nilai.kd_program');
		$this->db->where('nilai.tipe', $id);
		$data['nilaiFilTip'] = $this->db->get()->result();
        // var_dump($data['nilaiFilTip']); die;
      $data['NilaiTip'] = $id;
      if(count($data['nilaiFilTip']) != 0){
        $this->load->view('guru/renilai/resultNilaiTipe', $data);
      } else {
        echo '<button class="btn btn-danger btn-block">Tidak ada Data</button>';
      }
    }
    // VIEW REKAP === NILAI ============================== END
}