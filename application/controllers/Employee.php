<?php 
class Employee extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Employee_model');
		$this->load->model('M_query');
        if (count($this->db->get_where('users', ['akses' => 1])->result()) == 0) {
            redirect('reg');
        }
        // mencegah user tidak login
        if($this->session->has_userdata('isLoggin') != true){
            redirect('auth');
        }
    }

	public function index()
	{
		$year = $this->input->post("year");
		$year = $year != "" ? $year : date("Y");
		$month = $this->input->post("month");
		$month = $month != "" ? $month : date("m");
		$emp_id = $this->input->post("empid");
		$ajaran = $this->input->post("tahun_ajaran");
		$mapel = $this->input->post("nama_mapel");
		$updatepage = $this->uri->segment(3);

		$data = $this->Employee_model->attendance();

        // JUDUL PAGE
		$data['title'] = "Absensi | PKBM Ciptamekar";
		$data['judul'] = "HASIL DATA ABSENSI KESELURUHAN";

	// DATA MENU
		// ===> No DropDown
		$data['menu'] = [
			'Dashboard' => '',
			'Profile' 	=> '',
			'Absensi'	=> 'active',
			'Nilai'		=> ''
		];
            $data['menu_lapor'] = [
                'laporan_menu'=> '', // active
                'laporan_show' => '', // show
                'laporan_absen' => '', // active
                'laporan_nilai' => '' // active
            ];

	// MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
		$data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
		
		$this->load->view("templates/header", $data);
		$this->load->view('templates/sidebar/guru', $data);
		$this->load->view('templates/topbar', $data);
		if ($updatepage == 1) {
			redirect("employee/attadd/$ajaran/$mapel/$emp_id/$year/$month", [ "year" => $year, "month" => $month, "data" => $data, "ajaran" => $ajaran, "mapel" => $mapel ]);
		} else {
			$this->load->view("employee/v_attendance", [ "year" => $year, "month" => $month, "data" => $data, "ajaran" => $ajaran, "mapel" => $mapel ]);
		}
		$this->load->view("templates/footer");
	}

	public function attendance()
	{
		$year = $this->input->post("year");
		$year = $year != "" ? $year : date("Y");
		$month = $this->input->post("month");
		$month = $month != "" ? $month : date("m");
		$emp_id = $this->input->post("empid");
		$ajaran = $this->input->post("tahun_ajaran");
		$mapel = $this->input->post("nama_mapel");
		$updatepage = $this->uri->segment(3);

		// $id = $this->db->get_where('users', ['nama_program' => $this->session->userdata('nama_program')])->row();
		// var_dump($id); die;
		$data = $this->Employee_model->attendance();

        // JUDUL PAGE
		$data['title'] = "Absensi | PKBM Ciptamekar";
		$data['judul'] = "INPUT DATA ABSENSI";

	// DATA MENU
		// ===> No DropDown
		$data['menu'] = [
			'Dashboard' => '',
			'Profile' 	=> '',
			'Absensi' 	=> 'active',
			'Nilai'		=> ''
		];
            $data['menu_lapor'] = [
                'laporan_menu'=> '', // active
                'laporan_show' => '', // show
                'laporan_absen' => '', // active
                'laporan_nilai' => '' // active
            ];

	// MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
		$data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
		
		$this->load->view("templates/header", $data);
		$this->load->view('templates/sidebar/guru', $data);
		$this->load->view('templates/topbar', $data);
		if ($updatepage == 1) {
			redirect("employee/attadd/$ajaran/$mapel/$emp_id/$year/$month", [ "year" => $year, "month" => $month, "data" => $data, "ajaran" => $ajaran, "mapel" => $mapel ]);
		} else {
			$this->load->view("employee/attendance", [ "year" => $year, "month" => $month, "data" => $data, "ajaran" => $ajaran, "mapel" => $mapel ]);
		}
		$this->load->view("templates/footer");
	}

	public function attsearch()
	{
		$emp_id = $this->uri->segment(3);
		$year = $this->uri->segment(4);
		$month = $this->uri->segment(5);

		$employee = $this->Employee_model->empsearch($emp_id);
		$attendances = $this->Employee_model->attsearch();
		
		$this->load->view("templates/header");
		redirect("employee/attadd/$emp_id/$year/$month", [ "year" => $year, "month" => $month, "employee" => $employee, "attendances" => $attendances ]);
		$this->load->view("templates/footer");
	}

	public function attadd()
	{
		$emp_id = $this->uri->segment(3);
		$year = $this->uri->segment(4);
		$month = $this->uri->segment(5);
		$data['ajaran'] = $this->uri->segment(6);
		$data['mapel'] = $this->uri->segment(7);
		// var_dump($data['mapel']);die;

		$employee = $this->Employee_model->empsearch($emp_id);
		$attendances = $this->Employee_model->attsearch();

        // JUDUL PAGE
		$data['title'] = "Absensi | PKBM Ciptamekar";
		$data['judul'] = "PENGELOLAAN ABSENSI";

	// DATA MENU
		// ===> No DropDown
		$data['menu'] = [
			'Dashboard' => '',
			'Profile'	=> '',
			'Absensi' 	=> 'active',
			'Nilai'		=> ''
		];
            $data['menu_lapor'] = [
                'laporan_menu'=> '', // active
                'laporan_show' => '', // show
                'laporan_absen' => '', // active
                'laporan_nilai' => '' // active
            ];

	// MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
		$data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
		
		$this->load->view("templates/header", $data);
		$this->load->view('templates/sidebar/guru', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view("employee/attadd", [ "year" => $year, "month" => $month, "employee" => $employee, "attendances" => $attendances ]);
		$this->load->view("templates/footer");
	}

	public function attupdate()
	{
		$ajaran = $this->input->post("tahun_ajaran");
		$mapel = $this->input->post("nama_mapel");
		// var_dump($mapel); die;
		$emp_id = $this->input->post("empid");
		$year = $this->input->post("year");
		$month = $this->input->post("month");

		$employee = $this->Employee_model->empsearch($emp_id);
		$data = $this->Employee_model->attupdate();
		$attendances = $this->Employee_model->attsearch();

		$this->load->view("templates/header");
		redirect("employee/attadd/$emp_id/$year/$month/$ajaran/$mapel", [ "year" => $year, "month" => $month, "employee" => $employee, "attendances" => $attendances ]);
		$this->load->view("templates/footer");
	}
}
