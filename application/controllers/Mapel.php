<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapel extends CI_Controller 
{
    function __construct()
    {
        parent::__construct();
        //checkAksesModule();
        $this->load->library('ssp');
        $this->load->model('M_mapel');
    }

    public function index()
    {
            // JUDUL PAGE
                $data['title'] = "Data Mata Pelajaran";
                $data['judul'] = "Data Mata Pelajaran";

            // DATA MENU
                // ===> No DropDown
                $data['menu'] = [
                    'Dashboard' => '',
                    'Pendaftaran' => '',
                    'Jadwal' =>''
                ];

                // ===> DropDown
                // User
                $data['menu_drop'] = [
                    'user_menu' => '',
                    'user_show' => '',
                    'user_admin' => '',
                    'user_guru' => '',
                    'user_siswa' => '',
                    'user_profile' => ''
                ];
                // Data Master
                $data['menu_drop'] += [
                    'master_menu'=> 'active',
                    'master_show' => 'show',
                    'master_guru' => '',
                    'master_siswa' => '',
                    'master_mapel' => 'active',
                    'master_ruangan' => '',
                    'master_program' => ''
                ];
                // Laporan
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        // AMBIL DATA
            $data['mapel'] = $this->db->get('d_mapel')->result();
        // END AMBIL

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('master/mapel/v_mapel', $data);
            $this->load->view('templates/footer');
    }

    public function create()
    {
    // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

    // AMBIL DATA
        $data['mapel'] = $this->db->get('d_mapel')->result();
    // END AMBIL
    
    // START FORM VALIDATION (INPUT)

        // NIK HARUS UNIK, TIDAK BOLEH SAMA. TAMBAHKAN NANTI

        $this->form_validation->set_rules('kd_mapel', 'Kode Mapel', 'required|is_unique[d_mapel.kd_mapel]', [
            'is_unique' => 'Kode Mapel Sudah Pernah digunakan!'
        ]);
        
        if ($this->form_validation->run() == false) {
        // JUDUL PAGE
            $data['title'] = "Data Mata Pelajaran";
            $data['judul'] = "Input Data Mata Pelajaran";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Pendaftaran' => '',
                'Jadwal' =>''
            ];

            // ===> DropDown
            // User
            $data['menu_drop'] = [
                'user_menu' => '',
                'user_show' => '',
                'user_admin' => '',
                'user_guru' => '',
                'user_siswa' => '',
                'user_profile' => ''
            ];
            // Data Master
            $data['menu_drop'] += [
                'master_menu'=> 'active',
                'master_show' => 'show',
                'master_guru' => '',
                'master_siswa' => '',
                'master_mapel' => 'active',
                'master_ruangan' => '',
                'master_program' => ''
            ];
                // Laporan
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('master/mapel/mapel_form', $data);
            $this->load->view('templates/footer');
        } else {
            $this->load->model('M_mapel');

            $data = array(
                'kd_mapel' => $this->input->post('kd_mapel'),
                'nama_mapel' => $this->input->post('nama_mapel')
            );
    
            $this->M_mapel->insertMapel($data);
            $this->session->set_flashdata('pesan', "
                <script>
                Swal.fire({
                        icon: 'success',
                        title: 'Berhasil!',
                        text: 'Data Berhasil Ditambahkan!',
                        })
                </script>
                ");
            redirect('mapel/create');
        }
    }

    // UPDATE MAPEL
        // AJAX UPDATE MAPEL
    public function aumapel()
    {
        if ($this->input->is_ajax_request()) {
            $kd_mapel = $this->input->post('kd_mapel');
            $d_mapel = $this->db->get_where('d_mapel', ['kd_mapel' => $kd_mapel])->row();
            echo json_encode($d_mapel);
        } else {
            exit('No direct script access allowed');
        }
    }
    public function umapel()
    {
        $data = [
                'kd_mapel' => $this->input->post('kdmapel', true),
                'nama_mapel' => $this->input->post('namamapel', true)
        ];

            $this->db->where('kd_mapel', $this->input->post('kdmapel'));
            $query = $this->db->update('d_mapel', $data);

            if ($query) {
                $this->session->set_flashdata('pesan', "
                    <script>
                        Swal.fire(
                            'Berhasil!',
                            'Data Berhasil Di Update!',
                            'success'
                            )
                    </script>
                    ");
                redirect('Mapel');
            } else {
                $this->session->set_flashdata('pesan', "
                    <script>
                        Swal.fire(
                            'Oopss!',
                            'Gagal Di Update!',
                            'error'
                            )
                    </script>
                    ");
                redirect('Mapel');
            }
    }
    // END UPDATE MAPEL

    public function delete($kd_mapel)
    {
        $delete = $this->M_mapel->delete($kd_mapel);
           
        if($delete){
         $this->session->set_flashdata('pesan', "
         <script>
             Swal.fire(
                 'Berhasil!',
                 'Data Berhasil Dihapus!',
                 'success'
                 )
         </script>
         ");
         redirect('mapel');
        } else {
         $this->session->set_flashdata('pesan', "
         <script>
             Swal.fire(
                 'Oops!',
                 'Gagal Di hapus!',
                 'error'
                 )
         </script>
         ");
         redirect('mapel');
        }
    }
}