<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_jadwal');
        $this->load->model('M_mapel');
        $this->load->model('M_ruangan');
        $this->load->model('M_query');
        $this->load->model('M_program');
        $this->load->library('form_validation');
        is_admin();
        if (count($this->db->get_where('users', ['akses' => 1])->result()) == 0) {
            redirect('reg');
        }
        // mencegah user tidak login
        if($this->session->has_userdata('isLoggin') != true){
            redirect('auth');
        }
    }

    public function index()
    {
            // JUDUL PAGE
                $data['title'] = "Jadwal Pelajaran";
                $data['judul'] = "Input Data Jadwal Pelajaran";

            // DATA MENU
                // ===> No DropDown
                $data['menu'] = [
                    'Dashboard' => '',
                    'Pendaftaran' => '',
                    'Jadwal' =>'active'
                ];

                // ===> DropDown
                // User
                $data['menu_drop'] = [
                    'user_menu' => '',
                    'user_show' => '',
                    'user_admin' => '',
                    'user_guru' => '',
                    'user_siswa' => '',
                    'user_profile' => ''
                ];
                // Data Master
                $data['menu_drop'] += [
                    'master_menu'=> '',
                    'master_show' => '',
                    'master_guru' => '',
                    'master_siswa' => '',
                    'master_mapel' => '',
                    'master_ruangan' => '',
                    'master_program' => ''
                ];
                // Laporan
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
            
        // class baru
        $jadwal = new stdClass();
        $jadwal->id_jadwal = null;
        $jadwal->hari = null;
        $jadwal->jam_awl = null;
        $jadwal->jam_akhr = null;
        // $jadwal->nama_mapel = null; KARENA UNTUK TIGA ITU, MENGGUNAKAN SELECT DARI TABEL LAIN
        // $jadwal->ruangan = null;
        // $jadwal->tutor = null;
        
        $mapel = $this->M_mapel->get();
        $guru = $this->M_query->get();
        $ruangan = $this->M_ruangan->get();
        $program = $this->M_program->get();

        $jdwl = array(
            'page'      => 'add',
            'row'       => $jadwal,
            'mapel'     => $mapel,
            'guru'      => $guru,
            'ruangan'   => $ruangan,
            'program'   => $program
        );

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/jadwal_form', $jdwl);
            $this->load->view('templates/footer');
    }

    public function prosesSubmit()
    {
        $post = $this->input->post(null, TRUE);

        if(isset($_POST['add'])) {
            $this->M_jadwal->add($post);
        } else {
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Oopss!!',
                    'Data Gagal Disimpan!',
                    'error'
                    )
            </script>
            ");
            redirect('jadwal');
        }

        if($this->db->affected_rows() > 0){
            $this->session->set_flashdata('pesan', "
            <script>
                Swal.fire(
                    'Berhasil!',
                    'Data Berhasil Disimpan!',
                    'success'
                    )
            </script>
            ");
            redirect('admin/jadwal');
        }
    }
}