<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ruangan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_ruangan');
        $this->load->model('M_query');
        $this->load->library('form_validation');
        is_admin();
        if (count($this->db->get_where('users', ['akses' => 1])->result()) == 0) {
            redirect('reg');
        }
        // mencegah user tidak login
        if($this->session->has_userdata('isLoggin') != true){
            redirect('auth');
        }
    }
    
    public function index()
    {
            // JUDUL PAGE
                $data['title'] = "Ruangan Kelas";
                $data['judul'] = "Data Ruangan Kelas";

            // DATA MENU
                // ===> No DropDown
                $data['menu'] = [
                    'Dashboard' => '',
                    'Pendaftaran' => '',
                    'Jadwal' =>''
                ];

                // ===> DropDown
                // User
                $data['menu_drop'] = [
                    'user_menu' => '',
                    'user_show' => '',
                    'user_admin' => '',
                    'user_guru' => '',
                    'user_siswa' => '',
                    'user_profile' => ''
                ];
                // Data Master
                $data['menu_drop'] += [
                    'master_menu'=> 'active',
                    'master_show' => 'show',
                    'master_guru' => '',
                    'master_siswa' => '',
                    'master_mapel' => '',
                    'master_ruangan' => 'active',
                    'master_program' => ''
                ];
                // Laporan
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];
        // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        
        // AMBIL DATA
        $data['ruangan'] = $this->db->get('d_ruangan')->result();
        // END AMBIL

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('master/ruangan/v_ruangan', $data);
            $this->load->view('templates/footer');
    }

    public function create()
    {
    // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

    // AMBIL DATA
        $data['ruangan'] = $this->db->get('d_ruangan')->result();
    // END AMBIL
    
    // START FORM VALIDATION (INPUT)

        // NIK HARUS UNIK, TIDAK BOLEH SAMA. TAMBAHKAN NANTI

        $this->form_validation->set_rules('kd_ruangan', 'Kode Ruangan', 'required|is_unique[d_ruangan.kd_ruangan]', [
            'is_unique' => 'Kode Ruangan Sudah Pernah digunakan!'
        ]);
        
        if ($this->form_validation->run() == false) {
        // JUDUL PAGE
            $data['title'] = "Data Ruangan Kelas";
            $data['judul'] = "Input Data Ruangan Kelas";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Pendaftaran' => '',
                'Jadwal' =>''
            ];

            // ===> DropDown
            // User
            $data['menu_drop'] = [
                'user_menu' => '',
                'user_show' => '',
                'user_admin' => '',
                'user_guru' => '',
                'user_siswa' => '',
                'user_profile' => ''
            ];
            // Data Master
            $data['menu_drop'] += [
                'master_menu'=> 'active',
                'master_show' => 'show',
                'master_guru' => '',
                'master_siswa' => '',
                'master_mapel' => '',
                'master_ruangan' => 'active',
                'master_program' => ''
            ];
                // Laporan
                $data['menu_lapor'] = [
                    'laporan_menu'=> '',
                    'laporan_show' => '',
                    'laporan_siswa' => ''
                ];

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar/admin', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('master/ruangan/ruangan_form', $data);
            $this->load->view('templates/footer');
        } else {

            $data = array(
                'kd_ruangan' => $this->input->post('kd_ruangan'),
                'nama_ruangan' => $this->input->post('nama_ruangan')
            );
    
            $this->M_ruangan->insertRuangan($data);
            $this->session->set_flashdata('pesan', "
                <script>
                Swal.fire({
                        icon: 'success',
                        title: 'Berhasil!',
                        text: 'Anda Berhasil Ditambahkan!',
                        })
                </script>
                ");
            redirect('ruangan/create');
        }
    }

    // UPDATE DATA RUANGAN
        // AJAX UPDATE RUANGAN
    public function auruangan()
    {
        if ($this->input->is_ajax_request()) {
            $kd_ruangan = $this->input->post('kd_ruangan');
            $d_ruangan = $this->db->get_where('d_ruangan', ['kd_ruangan' => $kd_ruangan])->row();
            echo json_encode($d_ruangan);
        } else {
            exit('No direct script access allowed');
        }
    }
    public function uruangan()
    {
        $data = [
                'kd_ruangan' => $this->input->post('kdruangan', true),
                'nama_ruangan' => $this->input->post('namaruangan', true)
        ];

            $this->db->where('kd_ruangan', $this->input->post('kdruangan'));
            $query = $this->db->update('d_ruangan', $data);

            if ($query) {
                $this->session->set_flashdata('pesan', "
                    <script>
                        Swal.fire(
                            'Berhasil!',
                            'Data Berhasil Di Update!',
                            'success'
                            )
                    </script>
                    ");
                redirect('Ruangan');
            } else {
                $this->session->set_flashdata('pesan', "
                    <script>
                        Swal.fire(
                            'Oopss!',
                            'Gagal Di Update!',
                            'error'
                            )
                    </script>
                    ");
                redirect('Ruangan');
            }
    }
    // END UPDATE DATA RUANGAN

    public function delete($kd_ruangan)
    {
        $delete = $this->M_ruangan->delete($kd_ruangan);
           
        if($delete){
         $this->session->set_flashdata('pesan', "
         <script>
             Swal.fire(
                 'Berhasil!',
                 'Data Berhasil Dihapus!',
                 'success'
                 )
         </script>
         ");
         redirect('ruangan');
        } else {
         $this->session->set_flashdata('pesan', "
         <script>
             Swal.fire(
                 'Oops!',
                 'Gagal Di hapus!',
                 'error'
                 )
         </script>
         ");
         redirect('ruangan');
        }
    }
}