<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        if (count($this->db->get_where('users', ['akses' => 1])->result()) == 0) {
            redirect('reg');
        }
        
    }

    public function index()
    {
        $data['title'] = 'Login | PKBM Ciptamekar';
        $this->load->view('templates/auth_header', $data);
        $this->load->view('auth/login');
        $this->load->view('templates/auth_footer');
    }

    public function ProsesLogin()
    {
       $username = $this->input->post('username');
       $password = $this->input->post('password');

        $this->load->model('M_login');

        $getUser = $this->M_login->getUser($username);
        // var_dump($getUser);die;
        
        // kondisi ketika login

        if(count($getUser) == 1){
            if(password_verify($password, $getUser[0]->password)){

                $dataSession = array(
                    "namalengkap" => $getUser[0]->namalengkap,
                    "username" => $getUser[0]->username,
                    "isActive" => $getUser[0]->isActive,
                    "akses" => $getUser[0]->akses,
                    "nama_program" => $getUser[0]->nama_program,
                    "isLoggin" => true
                    // true = 1
                );
                $this->session->set_userdata($dataSession);

                if($getUser[0]->isActive == 1){
                    if($getUser[0]->akses == 1){
                        redirect('admin');
                        // echo "berhasil masuk!";
                    } elseif($getUser[0]->akses == 2){
                        redirect('siswa');
                    } elseif($getUser[0]->akses == 3){
                        redirect('guru/dashboard');
                        // echo "berhasil masuk!";
                    }
                }
            }else{
                $data['title'] = 'Login | PKBM Ciptamekar';
                // $data['peringatan'] = "Password Anda salah!.";
                    
                // $this->load->view('templates/auth_header', $data);
                // $this->load->view('auth/v_gagallogin', $data);
                // $this->load->view('templates/auth_footer');
                // echo "<script>
                //     alert('Password Salah!');    
                //     window.location.href='". base_url() ."auth';
                // </script>";
                $this->session->set_flashdata('pesan', "
                <script>
                   Swal.fire({
                        icon: 'error',
                        title: 'Oops..!',
                        text: 'Password Salah!',
                        })
                </script>
                ");
                redirect('auth');
            }
        }elseif(count($getUser) == 0){
            $data['title'] = 'Login | PKBM Ciptamekar';
            // $data['peringatan'] = "Anda belum terdaftar!";
            
            // $this->load->view('templates/auth_header', $data);
            // $this->load->view('auth/v_gagallogin', $data);
            // $this->load->view('templates/auth_footer');
            $this->session->set_flashdata('pesan', "
            <script>
               Swal.fire({
                    icon: 'error',
                    title: 'Oops..!',
                    text: 'Akun Tidak ditemukan!',
                    })
            </script>
            ");
            redirect('auth');
        }
        else{
            $this->session->set_flashdata('pesan', "
            <script>
               Swal.fire({
                    icon: 'error',
                    title: 'Oops..!',
                    text: 'Username Tidak ditemukan!',
                    })
            </script>
            ");
            redirect('auth');
        }
        // var_dump($getUser);die;
    }

        public function logout()
    {
        $this->session->sess_destroy();
        $this->session->unset_userdata('id_users');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('akses');
        $this->session->set_flashdata('pesan', "
                <script>
                Swal.fire({
                        icon: 'success',
                        title: 'Berhasil!',
                        text: 'Anda Telah Logout!',
                        })
                </script>
                ");
        redirect('auth');
    }
}