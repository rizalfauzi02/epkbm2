<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reg extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }
    public function index()
    {
        $data['title'] = 'Daftar Admin | PKBM Ciptamekar';
        $this->load->view('reg', $data);
       
    }

    public function prosesReg()
    {
        // Rules
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.email]', [
            'is_unique' => 'Username Sudah digunakan!'
        ]);
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Daftar Admin | PKBM Ciptamekar';
            $this->load->view('reg', $data);
        } else {
            $this->load->model('M_register');

            $data['namalengkap'] = $this->input->post('namalengkap');
            $data['email'] = $this->input->post('email');
            $data['username'] = $this->input->post('username');
            $password = $this->input->post('password1');
            $data['akses'] = 1;
            $data['gambar'] = 'default.jpg';
            $data['isActive'] = 1;
            $password2 = $this->input->post('password2');
    
            // validasi
            if($password == $password2){
                // echo "Password sama!";
                $options = [
                    'cost' => 5,
                ];
                // echo password_hash($password, PASSWORD_DEFAULT, $options);
    
                $data['password'] = password_hash($password, PASSWORD_DEFAULT, $options);
                $this->M_register->insertUser($data);
                $this->session->set_flashdata('pesan', "
                    <script>
                    Swal.fire({
                            icon: 'success',
                            title: 'Berhasil!',
                            text: 'Anda Berhasil Mendaftar!',
                            })
                    </script>
                    ");
                redirect('auth');
                
            }
            else{
                $this->session->set_flashdata('pesan', "
                    <script>
                    Swal.fire({
                            icon: 'error',
                            title: 'Ooppss...!',
                            text: 'Password tidak sama!',
                            })
                    </script>
                    ");
                redirect('reg');
            }
        }

    }
}
