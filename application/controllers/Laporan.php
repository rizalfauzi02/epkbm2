<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_daftar');
        $this->load->model('M_query');
        $this->load->model('M_jadwal');
        $this->load->model('M_siswa');
        $this->load->model('M_ajaran');
        $this->load->model('M_mapel');
        $this->load->library('form_validation');
        // is_admin();
        if (count($this->db->get_where('users', ['akses' => 1])->result()) == 0) {
            redirect('reg');
        }
        // mencegah user tidak login
        if($this->session->has_userdata('isLoggin') != true){
            redirect('auth');
        }
    }

  public function guru()
  {
    $nama_file = 'Laporan-Guru';
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P']);
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'O');
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'E');

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic; border: none;">
            <tr border="0">
                <td width="33%" style="text-align: left; border: none;">{DATE j-m-Y}</td>
                <td width="33%" align="center" style="border: none;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right; border: none;">PKBM Ciptamekar</td>
            </tr>
        </table>');  // Note that the second parameter is optional : default = 'O' for ODD

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
            <tr border="0">
                <td width="33%"><span style="font-weight: bold; font-style: italic;">Rizal Fauzi</span></td>
                <td width="33%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: left; ">{DATE j-m-Y}</td>
            </tr>
        </table>', 'E');
    $data = $this->db->get('guru')->result();
    $html = '
                <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Laporan Data Guru</title>
                <div style="text-align: left; margin-left: 20px; font-weight: bold;">
                <img src="' . base_url('assets/img/') . 'logo.png" width="100px" alt="">
                </div>
                <style>
                    body{
                        font-family: sans-serif;
                    }
                    table{
                        border: 0.1px solid #708090;
                    }
                    tr:nth-child(event){
                        background-color: #ddd;
                    }
                    tr td{
                        text-align: center;
                        border: 0.1px solid #708090;
                        font-weight: 20;
                    }
                    tr th{
                        border: 0.1px solid #708090;
                    }
                    input[type=text] {
                        border: none;
                        background: transparent;
                    }
                </style>
            </head>
            <body>
                <h2 style="text-align: center;">PKBM CIPTAMEKAR</h2>
                <p style="text-align: center;">jln. Nakula RT017 RW 005 Tegal Buah Karawang</p>
                <hr>
                    <h4 style="text-align: center;">DATA GURU PKBM CIPTAMEKAR</h4>
                    <br>
                <table border="0.1" cellpadding="10" cellspacing="0" width="100%">
                    <tr>
                        <th>No</th>
                        <th>NUPTK</th>
                        <th>Nama Guru</th>
                        <th>Jenis Kelamin</th>
                    </tr>';
            $no=1;
            foreach ($data as $g) {
            $html .= '<tr>';
            $html .= '<td>' . $no++ . '</td>';
            $html .= '<td>' . $g->nuptk . '</td>';
            $html .= '<td>' . $g->nama_guru . '</td>';
            $html .= '<td>' . $g->jk_guru . '</td>';
            $html .= '</tr>';
        }
        $html .= '
                </table>
                </body>
                </html>';
    $mpdf->WriteHTML($html);
    $mpdf->Output("$nama_file.pdf", \Mpdf\Output\Destination::INLINE);
  }

  public function mapel()
  {
    $nama_file = 'Laporan-Mapel';
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P']);
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'O');
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'E');

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic; border: none;">
            <tr border="0">
                <td width="33%" style="text-align: left; border: none;">{DATE j-m-Y}</td>
                <td width="33%" align="center" style="border: none;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right; border: none;">PKBM Ciptamekar</td>
            </tr>
        </table>');  // Note that the second parameter is optional : default = 'O' for ODD

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
            <tr border="0">
                <td width="33%"><span style="font-weight: bold; font-style: italic;">Rizal Fauzi</span></td>
                <td width="33%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: left; ">{DATE j-m-Y}</td>
            </tr>
        </table>', 'E');
    $data = $this->db->get('d_mapel')->result();
    $html = '
                <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Laporan Mata Pelajaran</title>
                <style>
                    body{
                        font-family: sans-serif;
                    }
                    table{
                        border: 0.1px solid #708090;
                    }
                    tr:nth-child(event){
                        background-color: #ddd;
                    }
                    tr td{
                        text-align: center;
                        border: 0.1px solid #708090;
                        font-weight: 20;
                    }
                    tr th{
                        border: 0.1px solid #708090;
                    }
                    input[type=text] {
                        border: none;
                        background: transparent;
                    }
                </style>
            </head>
            <body>
                <h2 style="text-align: center;">PKBM CIPTAMEKAR</h2>
                <p style="text-align: center;">jln. Nakula RT017 RW 005 Tegal Buah Karawang</p>
                <hr>
                    <h4 style="text-align: center;">DATA MATA PELAJARAN PKBM CIPTAMEKAR</h4>
                    <br>
                <table border="0.1" cellpadding="10" cellspacing="0" width="100%">
                    <tr>
                        <th>No</th>
                        <th>Kode Mata Pelajaran</th>
                        <th>Nama Mata Pelajaran</th>
                    </tr>';
            $no=1;
            foreach ($data as $m) {
            $html .= '<tr>';
            $html .= '<td>' . $no++ . '</td>';
            $html .= '<td>' . $m->kd_mapel . '</td>';
            $html .= '<td>' . $m->nama_mapel . '</td>';
            $html .= '</tr>';
        }
        $html .= '
                </table>
                </body>
                </html>';
    $mpdf->WriteHTML($html);
    $mpdf->Output("$nama_file.pdf", \Mpdf\Output\Destination::INLINE);
  }

  public function ruangan()
  {
    $nama_file = 'Laporan-Ruangan-Kelas';
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P']);
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'O');
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'E');

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic; border: none;">
            <tr border="0">
                <td width="33%" style="text-align: left; border: none;">{DATE j-m-Y}</td>
                <td width="33%" align="center" style="border: none;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right; border: none;">PKBM Ciptamekar</td>
            </tr>
        </table>');  // Note that the second parameter is optional : default = 'O' for ODD

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
            <tr border="0">
                <td width="33%"><span style="font-weight: bold; font-style: italic;">Rizal Fauzi</span></td>
                <td width="33%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: left; ">{DATE j-m-Y}</td>
            </tr>
        </table>', 'E');
    $data = $this->db->get('d_ruangan')->result();
    $html = '
                <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Laporan Ruangan Kelas</title>
                <style>
                    body{
                        font-family: sans-serif;
                    }
                    table{
                        border: 0.1px solid #708090;
                    }
                    tr:nth-child(event){
                        background-color: #ddd;
                    }
                    tr td{
                        text-align: center;
                        border: 0.1px solid #708090;
                        font-weight: 20;
                    }
                    tr th{
                        border: 0.1px solid #708090;
                    }
                    input[type=text] {
                        border: none;
                        background: transparent;
                    }
                </style>
            </head>
            <body>
                <h2 style="text-align: center;">PKBM CIPTAMEKAR</h2>
                <p style="text-align: center;">jln. Nakula RT017 RW 005 Tegal Buah Karawang</p>
                <hr>
                    <h4 style="text-align: center;">DATA RUANGAN KELAS PKBM CIPTAMEKAR</h4>
                    <br>
                <table border="0.1" cellpadding="10" cellspacing="0" width="100%">
                    <tr>
                        <th>No</th>
                        <th>Kode Ruangan Kelas</th>
                        <th>Nama Ruangan Kelas</th>
                    </tr>';
            $no=1;
            foreach ($data as $r) {
            $html .= '<tr>';
            $html .= '<td>' . $no++ . '</td>';
            $html .= '<td>' . $r->kd_ruangan . '</td>';
            $html .= '<td>' . $r->nama_ruangan . '</td>';
            $html .= '</tr>';
        }
        $html .= '
                </table>
                </body>
                </html>';
    $mpdf->WriteHTML($html);
    $mpdf->Output("$nama_file.pdf", \Mpdf\Output\Destination::INLINE);
  }

  public function program()
  {
    $nama_file = 'Laporan-Program-Paket';
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P']);
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'O');
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'E');

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic; border: none;">
            <tr border="0">
                <td width="33%" style="text-align: left; border: none;">{DATE j-m-Y}</td>
                <td width="33%" align="center" style="border: none;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right; border: none;">PKBM Ciptamekar</td>
            </tr>
        </table>');  // Note that the second parameter is optional : default = 'O' for ODD

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
            <tr border="0">
                <td width="33%"><span style="font-weight: bold; font-style: italic;">Rizal Fauzi</span></td>
                <td width="33%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: left; ">{DATE j-m-Y}</td>
            </tr>
        </table>', 'E');
    $data = $this->db->get('d_program')->result();
    $html = '
                <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Laporan Program Paket</title>
                <style>
                    body{
                        font-family: sans-serif;
                    }
                    table{
                        border: 0.1px solid #708090;
                    }
                    tr:nth-child(event){
                        background-color: #ddd;
                    }
                    tr td{
                        text-align: center;
                        border: 0.1px solid #708090;
                        font-weight: 20;
                    }
                    tr th{
                        border: 0.1px solid #708090;
                    }
                    input[type=text] {
                        border: none;
                        background: transparent;
                    }
                </style>
            </head>
            <body>
                <h2 style="text-align: center;">PKBM CIPTAMEKAR</h2>
                <p style="text-align: center;">jln. Nakula RT017 RW 005 Tegal Buah Karawang</p>
                <hr>
                    <h4 style="text-align: center;">DATA PROGRAM PAKET PKBM CIPTAMEKAR</h4>
                    <br>
                <table border="0.1" cellpadding="10" cellspacing="0" width="100%">
                    <tr>
                        <th>No</th>
                        <th>Kode Program Paket</th>
                        <th>Nama Program Paket</th>
                    </tr>';
            $no=1;
            foreach ($data as $r) {
            $html .= '<tr>';
            $html .= '<td>' . $no++ . '</td>';
            $html .= '<td>' . $r->kd_program . '</td>';
            $html .= '<td>' . $r->nama_program . '</td>';
            $html .= '</tr>';
        }
        $html .= '
                </table>
                </body>
                </html>';
    $mpdf->WriteHTML($html);
    $mpdf->Output("$nama_file.pdf", \Mpdf\Output\Destination::INLINE);
  }
  
  public function Siswa()
  {
    $nama_file = 'Laporan-Data-Lengkap-Siswa';
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'O');
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'E');

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic; border: none;">
            <tr border="0">
                <td width="33%" style="text-align: left; border: none;">{DATE j-m-Y}</td>
                <td width="33%" align="center" style="border: none;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right; border: none;">PKBM Ciptamekar</td>
            </tr>
        </table>');  // Note that the second parameter is optional : default = 'O' for ODD

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
            <tr border="0">
                <td width="33%"><span style="font-weight: bold; font-style: italic;">Rizal Fauzi</span></td>
                <td width="33%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: left; ">{DATE j-m-Y}</td>
            </tr>
        </table>', 'E');
    // $siswa = $this->db->get_where('siswa', ['nama_program' => $id])->result();
    $siswa = $this->M_query->tampilSiswa();
    $html = '
                <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Laporan Data Lengkap Siswa</title>
                <style>
                    body{
                        font-family: sans-serif;
                    }
                    table{
                        border: 0.1px solid #708090;
                    }
                    tr:nth-child(event){
                        background-color: #ddd;
                    }
                    tr td{
                        text-align: center;
                        border: 0.1px solid #708090;
                        font-weight: 20;
                    }
                    tr th{
                        border: 0.1px solid #708090;
                    }
                    input[type=text] {
                        border: none;
                        background: transparent;
                    }
                </style>
            </head>
            <body>
                <h2 style="text-align: center;">PKBM CIPTAMEKAR</h2>
                <p style="text-align: center;">jln. Nakula RT017 RW 005 Tegal Buah Karawang</p>
                <hr>
                    <h4 style="text-align: center;">DATA SELURUH SISWA</h4>
                <table border="0.1" cellpadding="10" cellspacing="0" width="100%">
                    <tr>
                        <th>No</th>
                        <th>NIS</th>
                        <th>NIK</th>
                        <th>Nama Siswa</th>
                        <th>Tempat, Tanggal Lahir</th>
                        <th>Jenis Kelamin</th>
                        <th>Program Paket</th>
                    </tr>';
            $no=1;
            foreach ($siswa as $sis) {
            $html .= '<tr>';
            $html .= '<td>' . $no++ . '</td>';
            $html .= '<td>' . $sis->nis . '</td>';
            $html .= '<td>' . $sis->nik_siswa . '</td>';
            $html .= '<td>' . $sis->nama_siswa . '</td>';
            $html .= '<td>' . $sis->lahir_siswa . ', '. $sis->tanggal_siswa .'</td>';
            $html .= '<td>' . $sis->jk_siswa . '</td>';
            $html .= '<td>' . $sis->nama_program . '</td>';
            $html .= '</tr>';
        }
        $html .= '
                </table>
                </body>
                </html>';
    $mpdf->WriteHTML($html);
    $mpdf->Output("$nama_file.pdf", \Mpdf\Output\Destination::INLINE);
  }

  public function cetakJadwal($id)
  {
    $nama_file = 'Laporan-Jadwal-Pelajaran';
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P']);
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'O');
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'E');

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic; border: none;">
            <tr border="0">
                <td width="33%" style="text-align: left; border: none;">{DATE j-m-Y}</td>
                <td width="33%" align="center" style="border: none;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right; border: none;">PKBM Ciptamekar</td>
            </tr>
        </table>');  // Note that the second parameter is optional : default = 'O' for ODD

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
            <tr border="0">
                <td width="33%"><span style="font-weight: bold; font-style: italic;">Rizal Fauzi</span></td>
                <td width="33%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: left; ">{DATE j-m-Y}</td>
            </tr>
        </table>', 'E');
    // $siswa = $this->db->get_where('siswa', ['nama_program' => $id])->result();
    $jadwal = $this->M_jadwal->tampilJadwal($id);
    $html = '
                <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Laporan Jadwal Pelajaran</title>
                <style>
                    body{
                        font-family: sans-serif;
                    }
                    table{
                        border: 0.1px solid #708090;
                    }
                    tr:nth-child(event){
                        background-color: #ddd;
                    }
                    tr td{
                        text-align: center;
                        border: 0.1px solid #708090;
                        font-weight: 20;
                    }
                    tr th{
                        border: 0.1px solid #708090;
                    }
                    input[type=text] {
                        border: none;
                        background: transparent;
                    }
                </style>
            </head>
            <body>
                <h2 style="text-align: center;">PKBM CIPTAMEKAR</h2>
                <p style="text-align: center;">jln. Nakula RT017 RW 005 Tegal Buah Karawang</p>
                <hr>
                    <h4 style="text-align: center;">JADWAL PELAJARAN PER-PROGRAM PAKET</h4>
                <table border="0.1" cellpadding="10" cellspacing="0" width="100%">
                    <tr>
                        <th>No</th>
                        <th>Hari</th>
                        <th>Jam</th>
                        <th>Mata Pelajaran</th>
                        <th>Ruangan</th>
                        <th>Guru</th>
                        <th>Program Paket</th>
                    </tr>';
            $no=1;
            foreach ($jadwal as $row) {
            $html .= '<tr>';
            $html .= '<td>' . $no++ . '</td>';
            $html .= '<td>' . $row->hari . '</td>';
            $html .= '<td>' . $row->jam_awl . ', '. $row->jam_akhr .'</td>';
            $html .= '<td>' . $row->nama_mapel . '</td>';
            $html .= '<td>' . $row->nama_ruangan . '</td>';
            $html .= '<td>' . $row->nama_guru . '</td>';
            $html .= '<td>' . $row->nama_program . '</td>';
            $html .= '</tr>';
        }
        $html .= '
                </table>
                </body>
                </html>';
    $mpdf->WriteHTML($html);
    $mpdf->Output("$nama_file.pdf", \Mpdf\Output\Destination::INLINE);
  }

//   VIEW FILTER SISWA
  public function siswaFil()
  {
            // JUDUL PAGE
            $data['title'] = "Laporan - Siswa";
            $data['judul'] = "Laporan Data Siswa";

        // DATA MENU
            // ===> No DropDown
            $data['menu'] = [
                'Dashboard' => '',
                'Pendaftaran' => '',
                'Jadwal' =>''
            ];

            // ===> DropDown
            // User
            $data['menu_drop'] = [
                'user_menu' => '',
                'user_show' => '',
                'user_admin' => '',
                'user_guru' => '',
                'user_siswa' => '',
                'user_profile' => ''
            ];
            // Data Master
            $data['menu_drop'] += [
                'master_menu'=> '',
                'master_show' => '',
                'master_guru' => '',
                'master_siswa' => '',
                'master_mapel' => '',
                'master_ruangan' => '',
                'master_program' => '',
                'master_ajaran' => ''
            ];
            // LAPORAN
            $data['menu_lapor'] = [
                'laporan_menu'=> 'active',
                'laporan_show' => 'show',
                'laporan_siswa' => 'active'
            ];
    // MENGAMBIL DATA USER UNTUK NAMA USER YANG SEDANG LOGIN
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        
    // AMBIL DATA PROGRAM
        $data['program'] = $this->db->get('d_program')->result();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar/admin', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('laporan/siswa/v_siswafil', $data);
        $this->load->view('templates/footer');
  }

  public function filter($id)
  {
      $data['siswa'] = $this->db->get_where('siswa', ['nama_program' => $id])->result();
      $data['paket'] = $id;
      if(count($data['siswa']) != 0){
        $this->load->view('laporan/siswa/result', $data);
      } else {
        echo '<button class="btn btn-danger btn-block">Tidak ada Data</button>';
      }
  }
// END FILTER
  public function cetakMSiswa($id)
  {
    $nama_file = 'Laporan-Data-Siswa';
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P']);
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'O');
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'E');

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic; border: none;">
            <tr border="0">
                <td width="33%" style="text-align: left; border: none;">{DATE j-m-Y}</td>
                <td width="33%" align="center" style="border: none;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right; border: none;">PKBM Ciptamekar</td>
            </tr>
        </table>');  // Note that the second parameter is optional : default = 'O' for ODD

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
            <tr border="0">
                <td width="33%"><span style="font-weight: bold; font-style: italic;">Rizal Fauzi</span></td>
                <td width="33%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: left; ">{DATE j-m-Y}</td>
            </tr>
        </table>', 'E');
    // $siswa = $this->db->get_where('siswa', ['nama_program' => $id])->result();
    $siswa  = $this->M_query->siswaById($id);
    $html = '
                <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Laporan Data Siswa</title>
                <style>
                    body{
                        font-family: sans-serif;
                    }
                    table{
                        border: 0.1px solid #708090;
                    }
                    tr:nth-child(event){
                        background-color: #ddd;
                    }
                    tr td{
                        text-align: center;
                        border: 0.1px solid #708090;
                        font-weight: 20;
                    }
                    tr th{
                        border: 0.1px solid #708090;
                    }
                    input[type=text] {
                        border: none;
                        background: transparent;
                    }
                </style>
            </head>
            <body>
                <h2 style="text-align: center;">PKBM CIPTAMEKAR</h2>
                <p style="text-align: center;">jln. Nakula RT017 RW 005 Tegal Buah Karawang</p>
                <hr>
                    <h4 style="text-align: center;">DATA SISWA PER-PROGRAM PAKET</h4>
                    <table border="0.1" cellpadding="10" cellspacing="0" width="30%">
                        <tr>
                            <th>Program Paket</th>
                        </tr>
                        <tr>
                            <td style="background: trasparent">' . $siswa[0]->nama_program . '</td>
                        </tr>
                    </table><br>
                <table border="0.1" cellpadding="10" cellspacing="0" width="100%">
                    <tr>
                        <th>No</th>
                        <th>NIS</th>
                        <th>NIK</th>
                        <th>Nama Siswa</th>
                        <th>Tempat, Tanggal Lahir</th>
                        <th>Jenis Kelamin</th>
                    </tr>';
            $no=1;
            foreach ($siswa as $sis) {
            $html .= '<tr>';
            $html .= '<td>' . $no++ . '</td>';
            $html .= '<td>' . $sis->nis . '</td>';
            $html .= '<td>' . $sis->nik_siswa . '</td>';
            $html .= '<td>' . $sis->nama_siswa . '</td>';
            $html .= '<td>' . $sis->lahir_siswa . ', '. $sis->tanggal_siswa .'</td>';
            $html .= '<td>' . $sis->jk_siswa . '</td>';
            $html .= '</tr>';
        }
        $html .= '
                </table>
                </body>
                </html>';
    $mpdf->WriteHTML($html);
    $mpdf->Output("$nama_file.pdf", \Mpdf\Output\Destination::INLINE);
  }
// VIEW FILTER SISWA

// CETAK ABSENSI BY AJARAN
  public function cetakAbsenAjaran($id)
  {
    $ajaran  = $this->M_ajaran->ajaranById($id);
    $nama_file = 'Laporan-Absensi-Tahun-Ajaran-'. $ajaran[0]->tahun_ajaran .'';
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P']);
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'O');
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'E');

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic; border: none;">
            <tr border="0">
                <td width="33%" style="text-align: left; border: none;">{DATE j-m-Y}</td>
                <td width="33%" align="center" style="border: none;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right; border: none;">PKBM Ciptamekar</td>
            </tr>
        </table>');  // Note that the second parameter is optional : default = 'O' for ODD

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
            <tr border="0">
                <td width="33%"><span style="font-weight: bold; font-style: italic;">Rizal Fauzi</span></td>
                <td width="33%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: left; ">{DATE j-m-Y}</td>
            </tr>
        </table>', 'E');
    // $siswa = $this->db->get_where('siswa', ['nama_program' => $id])->result();
    $ajaran  = $this->M_ajaran->ajaranById($id);
    // var_dump($ajaran); die;
    $html = '
                <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Laporan Absensi</title>
                <style>
                    body{
                        font-family: sans-serif;
                    }
                    table{
                        border: 0.1px solid #708090;
                    }
                    tr:nth-child(event){
                        background-color: #ddd;
                    }
                    tr td{
                        text-align: center;
                        border: 0.1px solid #708090;
                        font-weight: 20;
                    }
                    tr th{
                        border: 0.1px solid #708090;
                    }
                    input[type=text] {
                        border: none;
                        background: transparent;
                    }
                </style>
            </head>
            <body>
                <h2 style="text-align: center;">PKBM CIPTAMEKAR</h2>
                <p style="text-align: center;">jln. Nakula RT017 RW 005 Tegal Buah Karawang</p>
                <hr>
                    <h4 style="text-align: center;">REKAP ABSENSI TAHUN AJARAN ' . $ajaran[0]->tahun_ajaran . '</h4><br>
                <table border="0.1" cellpadding="10" cellspacing="0" width="100%">
                    <tr>
                        <th>No</th>
                        <th>NIS</th>
                        <th>Nama Siswa</th>
                        <th>Absensi</th>
                        <th>Mata Pelajaran</th>
                        <th>Tahun Ajaran</th>
                        <th>Tanggal Input</th>
                    </tr>';
            $no=1;
            foreach ($ajaran as $row) {
            $html .= '<tr>';
            $html .= '<td>' . $no++ . '</td>';
            $html .= '<td>' . $row->nis . '</td>';
            $html .= '<td>' . $row->nama_siswa . '</td>';
            $html .= '<td>' . $row->attstatus . '</td>';
            $html .= '<td>' . $row->nama_mapel . '</td>';
            $html .= '<td>' . $row->tahun_ajaran . '</td>';
            $html .= '<td>' . $row->createdate . '</td>';
            $html .= '</tr>';
        }
        $html .= '
                </table><br>
                <table border="0.1" cellpadding="10" cellspacing="0" width="10%">
                    <tr>
                        <th>Keterangan</th>
                    </tr>
                    <tr>
                        <td style="background: trasparent">P : Hadir</td>
                    </tr>
                    <tr>
                        <td style="background: trasparent">A : Alpha</td>
                    </tr>
                    <tr>
                        <td style="background: trasparent">S : Sakit</td>
                    </tr>
                    <tr>
                        <td style="background: trasparent">I : Izin</td>
                    </tr>
                </table>
                </body>
                </html>';
    $mpdf->WriteHTML($html);
    $mpdf->Output("$nama_file.pdf", \Mpdf\Output\Destination::INLINE);
  }
// END CETAK ABSENSI BY AJARAN

// CETAK ABSENSI BY MATA PELAJARAN
  public function cetakAbsenMapel($id)
  {
    $nama_file = 'Laporan-Absensi-by-Mapel';
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P']);
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'O');
    $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
        <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
        </div>', 'E');

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic; border: none;">
            <tr border="0">
                <td width="33%" style="text-align: left; border: none;">{DATE j-m-Y}</td>
                <td width="33%" align="center" style="border: none;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right; border: none;">PKBM Ciptamekar</td>
            </tr>
        </table>');  // Note that the second parameter is optional : default = 'O' for ODD

    $mpdf->SetHTMLFooter('
        <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
            font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
            <tr border="0">
                <td width="33%"><span style="font-weight: bold; font-style: italic;">Rizal Fauzi</span></td>
                <td width="33%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: left; ">{DATE j-m-Y}</td>
            </tr>
        </table>', 'E');
    // $siswa = $this->db->get_where('siswa', ['nama_program' => $id])->result();
    $mapel  = $this->M_mapel->mapelById($id);
    // var_dump($mapel); die;
    $html = '
                <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Laporan Absensi</title>
                <style>
                    body{
                        font-family: sans-serif;
                    }
                    table{
                        border: 0.1px solid #708090;
                    }
                    tr:nth-child(event){
                        background-color: #ddd;
                    }
                    tr td{
                        text-align: center;
                        border: 0.1px solid #708090;
                        font-weight: 20;
                    }
                    tr th{
                        border: 0.1px solid #708090;
                    }
                    input[type=text] {
                        border: none;
                        background: transparent;
                    }
                </style>
            </head>
            <body>
                <h2 style="text-align: center;">PKBM CIPTAMEKAR</h2>
                <p style="text-align: center;">jln. Nakula RT017 RW 005 Tegal Buah Karawang</p>
                <hr>
                    <h4 style="text-align: center;">REKAP ABSENSI by Mata Pelajaran</h4><br>
                <table border="0.1" cellpadding="10" cellspacing="0" width="100%">
                    <tr>
                        <th>No</th>
                        <th>NIS</th>
                        <th>Nama Siswa</th>
                        <th>Absensi</th>
                        <th>Mata Pelajaran</th>
                        <th>Tahun Ajaran</th>
                        <th>Tanggal Input</th>
                    </tr>';
            $no=1;
            foreach ($mapel as $row) {
            $html .= '<tr>';
            $html .= '<td>' . $no++ . '</td>';
            $html .= '<td>' . $row->nis . '</td>';
            $html .= '<td>' . $row->nama_siswa . '</td>';
            $html .= '<td>' . $row->attstatus . '</td>';
            $html .= '<td>' . $row->nama_mapel . '</td>';
            $html .= '<td>' . $row->tahun_ajaran . '</td>';
            $html .= '<td>' . $row->createdate . '</td>';
            $html .= '</tr>';
        }
        $html .= '
                </table><br>
                    <table border="0.1" cellpadding="10" cellspacing="0" width="10%">
                        <tr>
                            <th>Keterangan</th>
                        </tr>
                        <tr>
                            <td style="background: trasparent">P : Hadir</td>
                        </tr>
                        <tr>
                            <td style="background: trasparent">A : Alpha</td>
                        </tr>
                        <tr>
                            <td style="background: trasparent">S : Sakit</td>
                        </tr>
                        <tr>
                            <td style="background: trasparent">I : Izin</td>
                        </tr>
                    </table>
                </body>
                </html>';
    $mpdf->WriteHTML($html);
    $mpdf->Output("$nama_file.pdf", \Mpdf\Output\Destination::INLINE);
  }
// END CETAK ABSENSI BY MATA PELAJARAN

// CETAK NILAI BY MATA PELAJARAN
public function cetakNilaiMapel($id)
{
  $nama_file = 'Laporan-Nilai-by-Mapel';
  $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P']);
  $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
      <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
      </div>', 'O');
  $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
      <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
      </div>', 'E');

  $mpdf->SetHTMLFooter('
      <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
          font-size: 8pt; color: #000000; font-weight: bold; font-style: italic; border: none;">
          <tr border="0">
              <td width="33%" style="text-align: left; border: none;">{DATE j-m-Y}</td>
              <td width="33%" align="center" style="border: none;">{PAGENO}/{nbpg}</td>
              <td width="33%" style="text-align: right; border: none;">PKBM Ciptamekar</td>
          </tr>
      </table>');  // Note that the second parameter is optional : default = 'O' for ODD

  $mpdf->SetHTMLFooter('
      <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
          font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
          <tr border="0">
              <td width="33%"><span style="font-weight: bold; font-style: italic;">Rizal Fauzi</span></td>
              <td width="33%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
              <td width="33%" style="text-align: left; ">{DATE j-m-Y}</td>
          </tr>
      </table>', 'E');
  // $siswa = $this->db->get_where('siswa', ['nama_program' => $id])->result();
  $mapelNilai  = $this->M_mapel->mapelNilaiById($id);
//   var_dump($mapelNilai); die;                                                                            
  $html = '
              <html lang="en">
          <head>
              <meta charset="UTF-8">
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <title>Laporan Absensi</title>
              <style>
                  body{
                      font-family: sans-serif;
                  }
                  table{
                      border: 0.1px solid #708090;
                  }
                  tr:nth-child(event){
                      background-color: #ddd;
                  }
                  tr td{
                      text-align: center;
                      border: 0.1px solid #708090;
                      font-weight: 20;
                  }
                  tr th{
                      border: 0.1px solid #708090;
                  }
                  input[type=text] {
                      border: none;
                      background: transparent;
                  }
              </style>
          </head>
          <body>
              <h2 style="text-align: center;">PKBM CIPTAMEKAR</h2>
              <p style="text-align: center;">jln. Nakula RT017 RW 005 Tegal Buah Karawang</p>
              <hr>
                  <h4 style="text-align: center;">REKAP NILAI by Mata Pelajaran</h4><br>
              <table border="0.1" cellpadding="10" cellspacing="0" width="100%">
                  <tr>
                      <th>No</th>
                      <th>NIS</th>
                      <th>Nama Siswa</th>
                      <th>Mata Pelajaran</th>
                      <th>Nilai</th>
                      <th>Guru</th>
                      <th>Tipe Nilai</th>
                  </tr>';
          $no=1;
          foreach ($mapelNilai as $row) {
          $html .= '<tr>';
          $html .= '<td>' . $no++ . '</td>';
          $html .= '<td>' . $row->nis . '</td>';
          $html .= '<td>' . $row->nama_siswa . '</td>';
          $html .= '<td>' . $row->nama_mapel . '</td>';
          $html .= '<td>' . $row->nilai . '</td>';
          $html .= '<td>' . $row->nama_guru . '</td>';
          $html .= '<td>' . $row->tipe . '</td>';
          $html .= '</tr>';
      }
      $html .= '
              </table>
              </body>
              </html>';
  $mpdf->WriteHTML($html);
  $mpdf->Output("$nama_file.pdf", \Mpdf\Output\Destination::INLINE);
}
// END CETAK NILAI BY MATA PELAJARAN

// CETAK NILAI BY TIPE UJIAN
public function cetakNilaiTipe($id)
{
  $nama_file = 'Laporan-Nilai-By-Tipe-Ujian';
  $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P']);
  $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
      <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
      </div>', 'O');
  $mpdf->SetHTMLHeader('<div style="text-align: left; margin-left: 20px; font-weight: bold;">
      <img src="' . base_url('assets/app-assets/img/') . 'logo.png" width="100px" alt="">
      </div>', 'E');

  $mpdf->SetHTMLFooter('
      <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
          font-size: 8pt; color: #000000; font-weight: bold; font-style: italic; border: none;">
          <tr border="0">
              <td width="33%" style="text-align: left; border: none;">{DATE j-m-Y}</td>
              <td width="33%" align="center" style="border: none;">{PAGENO}/{nbpg}</td>
              <td width="33%" style="text-align: right; border: none;">PKBM Ciptamekar</td>
          </tr>
      </table>');  // Note that the second parameter is optional : default = 'O' for ODD

  $mpdf->SetHTMLFooter('
      <table border="0" width="100%" style="vertical-align: bottom; font-family: serif; 
          font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
          <tr border="0">
              <td width="33%"><span style="font-weight: bold; font-style: italic;">Rizal Fauzi</span></td>
              <td width="33%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
              <td width="33%" style="text-align: left; ">{DATE j-m-Y}</td>
          </tr>
      </table>', 'E');
  // $siswa = $this->db->get_where('siswa', ['nama_program' => $id])->result();
  $tipeNilai  = $this->M_query->tipeNilaiById($id);
//   var_dump($tipeNilai); die;
  $html = '
              <html lang="en">
          <head>
              <meta charset="UTF-8">
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <title>Laporan Absensi</title>
              <style>
                  body{
                      font-family: sans-serif;
                  }
                  table{
                      border: 0.1px solid #708090;
                  }
                  tr:nth-child(event){
                      background-color: #ddd;
                  }
                  tr td{
                      text-align: center;
                      border: 0.1px solid #708090;
                      font-weight: 20;
                  }
                  tr th{
                      border: 0.1px solid #708090;
                  }
                  input[type=text] {
                      border: none;
                      background: transparent;
                  }
              </style>
          </head>
          <body>
              <h2 style="text-align: center;">PKBM CIPTAMEKAR</h2>
              <p style="text-align: center;">jln. Nakula RT017 RW 005 Tegal Buah Karawang</p>
              <hr>
                  <h4 style="text-align: center;">REKAP NILAI by Tipe Ujian</h4><br>
              <table border="0.1" cellpadding="10" cellspacing="0" width="100%">
                <tr>
                    <th>No</th>
                    <th>NIS</th>
                    <th>Nama Siswa</th>
                    <th>Mata Pelajaran</th>
                    <th>Nilai</th>
                    <th>Guru</th>
                    <th>Tipe Nilai</th>
                </tr>';
        $no=1;
        foreach ($tipeNilai as $row) {
        $html .= '<tr>';
        $html .= '<td>' . $no++ . '</td>';
        $html .= '<td>' . $row->nis . '</td>';
        $html .= '<td>' . $row->nama_siswa . '</td>';
        $html .= '<td>' . $row->nama_mapel . '</td>';
        $html .= '<td>' . $row->nilai . '</td>';
        $html .= '<td>' . $row->nama_guru . '</td>';
        $html .= '<td>' . $row->tipe . '</td>';
        $html .= '</tr>';
        }
      $html .= '
              </table>
              </body>
              </html>';
  $mpdf->WriteHTML($html);
  $mpdf->Output("$nama_file.pdf", \Mpdf\Output\Destination::INLINE);
}
// END CETAK NILAI BY TIPE UJIAN

}