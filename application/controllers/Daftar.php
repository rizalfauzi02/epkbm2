<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
        $this->load->model('M_program');
		$this->load->library('form_validation');
	}

	public function index()
	{
        $data['title'] = 'Pendaftaran';
        $data['judul'] = 'Pendaftaran';

        // class baru
        $pendaftaran = new stdClass();
        $pendaftaran->id_pendaftaran = null;
        $pendaftaran->namalengkap = null;
        $pendaftaran->nik = null;
        $pendaftaran->tempat_lahir = null;
        $pendaftaran->tanggal_lahir = null;
        $pendaftaran->jk = null;
        $pendaftaran->kewarganegaraan = null;
        $pendaftaran->agama = null;
        $pendaftaran->email = null;
        $pendaftaran->no_telp = null;
        $pendaftaran->alamat = null;
        $pendaftaran->kode_pos = null;
        $pendaftaran->provinsi = null;
        $pendaftaran->kabupaten = null;
        $pendaftaran->kecamatan = null;

        // $jadwal->nama_mapel = null; KARENA UNTUK TIGA ITU, MENGGUNAKAN SELECT DARI TABEL LAIN
        // $jadwal->ruangan = null;
        // $jadwal->tutor = null;
        
        $program = $this->M_program->get();

        $pendaftar = array(
            'page'      => 'add',
            'row'       => $pendaftaran,
            'program'   => $program
        );

        $this->load->view('daftar', $pendaftar);
	}

    public function prosesSubmit()
    {

        // NIK HARUS UNIK, TIDAK BOLEH SAMA. TAMBAHKAN NANTI

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[pendaftaran.email]', [
            'is_unique' => 'Email Sudah Pernah digunakan!'
        ]);
        $this->form_validation->set_rules('nik', 'Nik', 'required|min_length[16]|is_unique[pendaftaran.nik]', [
            'is_unique' => 'NIK Sudah Pernah digunakan!',
            'min_length' => 'NIK minimal 16 Digit!'
        ]);
        
        if ($this->form_validation->run() == false) {
            // class baru
            $pendaftaran = new stdClass();
            $pendaftaran->id_pendaftaran = null;
            $pendaftaran->namalengkap = null;
            $pendaftaran->nik = null;
            $pendaftaran->tempat_lahir = null;
            $pendaftaran->tanggal_lahir = null;
            $pendaftaran->jk = null;
            $pendaftaran->kewarganegaraan = null;
            $pendaftaran->agama = null;
            $pendaftaran->email = null;
            $pendaftaran->no_telp = null;
            $pendaftaran->alamat = null;
            $pendaftaran->kode_pos = null;
            $pendaftaran->provinsi = null;
            $pendaftaran->kabupaten = null;
            $pendaftaran->kecamatan = null;
            
            $program = $this->M_program->get();

            $pendaftar = array(
                'page'      => 'add',
                'row'       => $pendaftaran,
                'program'   => $program
            );
            $data['title'] = 'Pendaftaran';
            $this->load->view('daftar', $pendaftar);
        } else {
            $this->load->model('M_daftar');

            $data['namalengkap'] = $this->input->post('namalengkap');
            $data['nik'] = $this->input->post('nik');
            $data['tempat_lahir'] = $this->input->post('tempat_lahir');
            $data['tanggal_lahir'] = $this->input->post('tanggal_lahir');
            $data['jk'] = $this->input->post('jk');
            $data['kewarganegaraan'] = $this->input->post('kewarganegaraan');
            $data['agama'] = $this->input->post('agama');
            $data['email'] = $this->input->post('email');
            $data['no_telp'] = $this->input->post('no_telp');
            $data['alamat'] = $this->input->post('alamat');
            $data['kode_pos'] = $this->input->post('kode_pos');
            $data['provinsi'] = $this->input->post('provinsi');
            $data['kabupaten'] = $this->input->post('kabupaten');
            $data['kecamatan'] = $this->input->post('kecamatan');
            $data['nama_program'] = $this->input->post('nama_program');
    
            $this->M_daftar->insertDaftar($data);
            $this->session->set_flashdata('pesan', "
                <script>
                Swal.fire({
                        icon: 'success',
                        title: 'Berhasil!',
                        text: 'Anda Berhasil Mendaftar!',
                        })
                </script>
                ");
            redirect('konfirmasi');
        }

    }
}
