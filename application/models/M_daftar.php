<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_daftar extends CI_Model
{
    public $table = 'pendaftaran';
    public $id = 'id_pendaftaran';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // INSERT PENDAFTAR BARU KE DB
    function insertDaftar($data)
    {
        $this->db->insert('pendaftaran', $data);
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function ddaftar($id_pendaftaran){
        $this->db->where('id_pendaftaran',$id_pendaftaran);
        $this->db->delete('pendaftaran');
        return true;
    }
    // INNER JOIN
    function tampilProgram()
    {
        $this->db->order_by('id_pendaftaran', 'DESC');
        // return $this->db->from('jadwal')
        //   ->join('d_mapel', 'd_mapel.kd_mapel = jadwal.nama_mapel')->get()->result();
        //   ->join('d_ruangan', 'd_ruangan.kd_ruangan = jadwal.nama_ruangan')->get()->result();
        $this->db->select('*');
        $this->db->from('pendaftaran');
        $this->db->join('d_program','d_program.kd_program = pendaftaran.nama_program');
        $query = $this->db->get()->result();
        return $query;
    }

}