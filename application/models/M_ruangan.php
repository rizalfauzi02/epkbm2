<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_ruangan extends CI_Model
{
    function get()
    {
        $data = $this->db->query("SELECT * FROM d_ruangan");
		return $data->result_array();
    }
    // Delete
    function delete($kd_ruangan){
        $this->db->where('kd_ruangan',$kd_ruangan);
        $this->db->delete('d_ruangan');
        return true;
    }
    // insert
    function insertRuangan($data){
		$this->db->insert('d_ruangan', $data);
	}
}