<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_program extends CI_Model
{
    function delete($kd_program){
        $this->db->where('kd_program',$kd_program);
        $this->db->delete('d_program');
        return true;
    }
    // insert
    function insertProgram($data){
		$this->db->insert('d_program', $data);
	}

    function get()
    {
        $data = $this->db->query("SELECT * FROM d_program");
		return $data->result_array();
    }
    function get_program_id($id)
    {
		$query = $this->db->get_where('d_program', array('kd_program' => $id));
		return $query->row();
    }
}