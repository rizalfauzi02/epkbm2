<?php
class Nilai_model extends CI_Model {
	public function get_nilai()
	{
		$this->db->select('nilai.*,
			siswa.nama_siswa as siswa,
			guru.nama_guru as guru,
			d_mapel.nama_mapel as mapel,
			d_program.nama_program as program');
		$this->db->from('nilai');
		$this->db->join('siswa', 'siswa.nis= nilai.nis');
		$this->db->join('d_mapel', 'd_mapel.kd_mapel = nilai.kd_mapel');
		$this->db->join('guru', 'guru.id_guru = nilai.id_guru');
		$this->db->join('d_program', 'd_program.kd_program = nilai.kd_program');
		$query = $this->db->get();
		return $query->result();	
	}

	public function get_nilai_byid($id)
	{
		$this->db->select('nilai.*,
			siswa.nama_siswa as siswa,
			guru.nama_guru as guru,
			d_mapel.nama_mapel as mapel,
			d_program.nama_program as program');
		$this->db->from('nilai');
		$this->db->join('siswa', 'siswa.nis= nilai.nis');
		$this->db->join('d_mapel', 'd_mapel.kd_mapel = nilai.kd_mapel');
		$this->db->join('guru', 'guru.id_guru = nilai.id_guru');
		$this->db->join('d_program', 'd_program.kd_program = nilai.kd_program');
		$this->db->where('nilai.kd_mapel', $id);
		$query = $this->db->get();
		return $query->result();	
	}

	public function set_add_nilai()
	{
		$kd_mapel = $this->input->post('mapel');
		$id_guru = $this->input->post('guru');
		$kd_program = $this->input->post('program');
		$tipe	= $this->input->post('tipe');

		//Start untuk database nilai
		$this->db->select('siswa.nis');
		$this->db->from('siswa');
		$this->db->where('nama_program', $kd_program);
		$query = $this->db->get();
		$isi_nis = $query->result();
		$i_nis = 0;

		//Start isi nis ke array
		//Untuk mengetahui berapa jumlah mahasiswa yang berada di
		//$kd_program dan supaya pengaksesan nis dalam looping menjadi
		//lebih mudah
		foreach ($isi_nis as $isi_nis) {
			$nis[$i_nis] = $isi_nis->nis;
			$i_nis++;
		}
		//End isi nis ke array

		//Looping untuk pengecekan data di database nilai apakah
		//sudah ada data dengan nis = $nis[$i] dan kd_mapel =
		//$kd_mapel
		//***
		//Looping dilakukan sebanyak $i_nis yang didapat dari looping
		//isi_nis
		//***
		//Hal ini dilakukan agar tidak ada duplikasi data
		for ($i=0; $i<$i_nis; $i++) {
			$data_cek_nilai = array(
				'nis' => $nis[$i],
				'kd_mapel' => $kd_mapel
			);

			$query = $this->db->get_where('nilai', $data_cek_nilai);
			$cek_nilai = $query->num_rows();

			//Jika data dengan nis = $nis[i] dan kd_mapel =
			//$kd_mapel tidak ada di database maka data
			//diinputkan ke database
			if ($cek_nilai == 0) {
				$data = array(
					'id_nilai' => NULL,
					'nis' => $nis[$i],
					'kd_mapel' => $kd_mapel,
					'id_guru' => $id_guru,
					'kd_program' => $kd_program,
					'tipe'	=> $tipe
				);

				$this->db->insert('nilai', $data);
			}
		}
		//End untuk database nilai

		//Start untuk view add_nilai
		$view_add_nilai = $this->db->get_where('siswa', array('nama_program' => $kd_program));
		return $view_add_nilai->result();
		//End untuk view add_nilai
	}

	public function add_nilai($nis, $kd_mapel, $id_guru, $nilai)
	{
		$this->db->where('nis', $nis);
		$this->db->where('kd_mapel', $kd_mapel);
		$this->db->where('id_guru', $id_guru);
		$this->db->update('nilai', array('nilai' => $nilai));
	}

	public function get_nilai_id($id)
	{
		$this->db->where('id_nilai', $id);
		$this->db->select('nilai.*,
			mahasiswa.nama as mahasiswa,
			dosen.nama as dosen,
			matakuliah.matakuliah');
		$this->db->from('nilai');
		$this->db->join('mahasiswa', 'mahasiswa.nim = nilai.nim');
		$this->db->join('matakuliah', 'matakuliah.id_matakuliah = nilai.id_matakuliah');
		$this->db->join('dosen', 'dosen.id_dosen = nilai.id_dosen');
		$query = $this->db->get();
		return $query->row();
	}

	public function edit_nilai($id)
	{
		$nilai = $this->input->post('nilai');
		$this->db->where('id_nilai', $id);
		$this->db->update('nilai', array('nilai' => $nilai));
	}

	public function delete_nilai($id)
	{
		$this->db->delete('nilai', array('id_nilai' => $id));
	}
}