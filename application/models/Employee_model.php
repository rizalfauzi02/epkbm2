<?php 
class Employee_model extends CI_Model
{

	public function empsearch($emp_id)
	{
		$query = $this->db->get_where("siswa", [ "id_siswa" => $emp_id ]);
		return $query->row();
	}

	public function attendance()
	{
		// $ajaran= $this->input->post("tahun_ajaran");
		// $mapel= $this->input->post("nama_mapel");
		// var_dump($mapel); die;
		$emp_id = $this->input->post("empid");
		$year = $this->input->post("year");
		$year = $year != "" ? $year : date("Y");
		$month = $this->input->post("month");
		$month = $month != "" ? $month : date("m");

		/*$this->db->select("emp.name, emp.mobile, emp.email, att.attdate, att.attstatus");
		$this->db->from("employees emp");
		$this->db->join("attendance att", "emp.id = att.empid", "left");
		$this->db->like([ "att.attdate" => $year . "-" . $month . "-" ]);
		$this->db->order_by("emp.name, att.attdate");
		$query = $this->db->get();
		return $query->result();*/

		$like_date = $year . "-" . $month . "-";

		$empid = [];
		$name = [];
		$present = [];
		$absent = [];
		$holiday = [];

		$this->db->select("id_siswa, nis, nama_siswa, jk_siswa");
		$this->db->where("nama_program", "PKTA01"); // Nama Program by User Guru yang login
		$this->db->from("siswa");
		$this->db->order_by("nis");
		$query = $this->db->get();
		$result = $query->result();

		for ($i = 0; $i < count($result); $i++) {
			$this->db->select("attdate, attstatus");
			$this->db->where([ "empid" => $result[$i]->id_siswa, "attstatus" => "P" ]); // HADIR
			$this->db->like([ "attdate" => $like_date ]);
			$this->db->from("attendance");
			$query2 = $this->db->get();
			$result2 = $query2->result();

			$this->db->select("attdate, attstatus");
			$this->db->where([ "empid" => $result[$i]->id_siswa, "attstatus" => "A" ]); // ALFA
			$this->db->like([ "attdate" => $like_date ]);
			$this->db->from("attendance");
			$query3 = $this->db->get();
			$result3 = $query3->result();
			
			$this->db->select("attdate, attstatus");
			$this->db->where([ "empid" => $result[$i]->id_siswa, "attstatus" => "S" ]); // SAKIT
			$this->db->like([ "attdate" => $like_date ]);
			$this->db->from("attendance");
			$query4 = $this->db->get();
			$result4 = $query4->result();

			$this->db->select("attdate, attstatus");
			$this->db->where([ "empid" => $result[$i]->id_siswa, "attstatus" => "I" ]); // IZIN
			$this->db->like([ "attdate" => $like_date ]);
			$this->db->from("attendance");
			$query5 = $this->db->get();
			$result5 = $query5->result();

			$this->db->select("attdate, attstatus");
			$this->db->where([ "empid" => $result[$i]->id_siswa, "attstatus" => "H" ]); // LIBUR
			$this->db->like([ "attdate" => $like_date ]);
			$this->db->from("attendance");
			$query6 = $this->db->get();
			$result6 = $query6->result();


			$empid[] = $result[$i]->id_siswa;
			$nis[] = $result[$i]->nis;
			$name[] = $result[$i]->nama_siswa;
			$hadir[] = count($result2);
			$alfa[] = count($result3);
			$sakit[] = count($result4);
			$izin[] = count($result5);
			$libur[] = count($result6);
			$ajaran[] = $this->input->post("tahun_ajaran");
			$mapel[] = $this->input->post("nama_mapel");
		}

		$data = [ $empid, $nis, $name, $hadir, $alfa, $sakit, $izin, $libur, $ajaran, $mapel ];

		return $data;
	}

	public function attsearch()
	{
		$emp_id = $this->uri->segment(3);
		$year = $this->uri->segment(4);
		$month = $this->uri->segment(5);

		$this->db->like([ "attdate" => $year . "-" . $month . "-" ]);
		$this->db->where("empid", $emp_id);
		$this->db->from("attendance");
		$this->db->order_by("attdate");
		$query = $this->db->get();
		return $query->result();
	}

	public function attupdate()
	{
		$ajaran = $this->input->post("tahun_ajaran");
		$mapel = $this->input->post("nama_mapel");
		$empid = $this->input->post("empid");
		$year = $this->input->post("year");
		$month = $this->input->post("month");
		$attstatus = $this->input->post("attstatus");
		$save = $this->input->post("save")[0];

		$attdate = $attstatus[$save - 1];
		$attdate = $year . "-" . $month . "-" . $save;

		$attstatus = $attstatus[$save - 1];

		if ($attstatus == "") {
			return false;
		} else {
			// $query = $this->db->get_where("attendance", [ "empid" => $empid, "attdate" => $attdate ]);
				// $data = [
				// 	"empid"			=> $empid, 
				// 	"attdate"		=> $attdate, 
				// 	"attstatus"		=> $attstatus, 

					
				// ];
				// return $this->db->insert("attendance", $data);
			$query = $this->db->get_where("attendance", [ "empid" => $empid, "attdate" => $attdate ]);

			if ($query->result()) {
				$data = [
					"attstatus"	=> $attstatus, 
				];

				$this->db->where([ "empid" => $empid, "attdate" => $attdate ]);
				return $this->db->update("attendance", $data);
			} else {
				$data = [
					"empid"		=> $empid, 
					"attdate"	=> $attdate, 
					"attstatus"	=> $attstatus,
					"tahun_ajaran" 	=> $ajaran,
					"nama_mapel" 	=> $mapel,
				];

				return $this->db->insert("attendance", $data);
			}
		}
	}

	public function attendanceAjaran($id)
	{
        $this->db->select('*');
        $this->db->from('attendance');
        $this->db->join('d_mapel','d_mapel.kd_mapel = attendance.nama_mapel');		
        $this->db->join('d_ajaran','d_ajaran.kd_ajaran = attendance.tahun_ajaran');
        $this->db->join('siswa','siswa.nis = attendance.empid');
        $this->db->where('attendance.tahun_ajaran', $id);
        $query = $this->db->get()->result();
        return $query;
	}
}
