<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_query extends CI_Model
{
    function get()
    {
        $data = $this->db->query("SELECT * FROM guru");
		return $data->result_array();
    }
    // Delete User
    function delete($id_users){
        $this->db->where('id_users',$id_users);
        $this->db->delete('users');
        return true;
    }
    // Delete Jadwal
    function deljdwl($id_jadwal){
        $this->db->where('id_jadwal',$id_jadwal);
        $this->db->delete('jadwal');
        return true;
    }
    // Delete DATA MASTER - Guru
    function delguru($id_guru){
        $this->db->where('id_guru',$id_guru);
        $this->db->delete('guru');
        return true;
    }
    // Delete DATA MASTER - Guru
    function delsiswa($id_siswa){
        $this->db->where('id_siswa',$id_siswa);
        $this->db->delete('siswa');
        return true;
    }

    // Update Profile
    // function updateProfile($username, $namalengkap)
    // {
    //     $this->db->set('namalengkap', $namalengkap);
    //     $this->db->where('username', $username);
    //     $this->db->update('users');
    //     return true;
    // }

    // ========= DATA MASTER - SISWA ======
    function tampilSiswa()
    {
        $this->db->order_by('nis', 'ASC');
        // return $this->db->from('jadwal')
        //   ->join('d_mapel', 'd_mapel.kd_mapel = jadwal.nama_mapel')->get()->result();
        //   ->join('d_ruangan', 'd_ruangan.kd_ruangan = jadwal.nama_ruangan')->get()->result();
        $this->db->select('*');
        $this->db->from('siswa');
        // $this->db->join('pendaftaran','pendaftaran.id_pendaftaran = siswa.id_siswa');
        $this->db->join('d_program','d_program.kd_program = siswa.nama_program');
        $query = $this->db->get()->result();
        return $query;
    }

    function tampilDaftar()
    {
        $this->db->order_by('id_pendaftaran', 'ASC');
        // return $this->db->from('jadwal')
        //   ->join('d_mapel', 'd_mapel.kd_mapel = jadwal.nama_mapel')->get()->result();
        //   ->join('d_ruangan', 'd_ruangan.kd_ruangan = jadwal.nama_ruangan')->get()->result();
        $this->db->select('*');
        $this->db->from('pendaftaran');
        // $this->db->join('pendaftaran','pendaftaran.id_pendaftaran = siswa.id_siswa');
        $this->db->join('d_program','d_program.kd_program = pendaftaran.nama_program');
        $query = $this->db->get();
        return $query;
    }

    function tampilGuruUser()
    {
        $this->db->order_by('nuptk', 'ASC');
        $this->db->select('*');
        $this->db->from('guru');
        $query = $this->db->get();
        return $query;
    }

    function tampilProgramUser()
    {
        $this->db->order_by('id_users', 'ASC');
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('d_program','d_program.kd_program = users.nama_program');
        $query = $this->db->get();
        return $query;
    }
    
    function tampilProgram()
    {
        $this->db->order_by('id_pendaftaran', 'ASC');
        // return $this->db->from('jadwal')
        //   ->join('d_mapel', 'd_mapel.kd_mapel = jadwal.nama_mapel')->get()->result();
        //   ->join('d_ruangan', 'd_ruangan.kd_ruangan = jadwal.nama_ruangan')->get()->result();
        $this->db->select('*');
        $this->db->from('pendaftaran');
        $this->db->join('d_program','d_program.kd_program = pendaftaran.nama_program');
        $query = $this->db->get()->result();
        return $query;
    }

    function getDaftarById($id)
    {
        // $this->db->order_by('id_pendaftaran', 'ASC');
        // return $this->db->from('jadwal')
        //   ->join('d_mapel', 'd_mapel.kd_mapel = jadwal.nama_mapel')->get()->result();
        //   ->join('d_ruangan', 'd_ruangan.kd_ruangan = jadwal.nama_ruangan')->get()->result();
        $this->db->select('*');
        $this->db->from('pendaftaran');
        $this->db->join('d_program','d_program.kd_program = pendaftaran.nama_program');
        $this->db->where('id_pendaftaran', $id);
        $query = $this->db->get();
        return $query;
    }

    public function addDatSiswa($post)
    {
        $params = [
            'nis'  => $post['nis'],
            'nik_siswa'  => $post['nik_siswa'],
            'nama_siswa'  => $post['nama_siswa'],
            'lahir_siswa'  => $post['lahir_siswa'],
            'tanggal_siswa'  => $post['tanggal_siswa'],
            'jk_siswa'  => $post['jk_siswa'],
            'nama_program'  => $post['kd_program']
        ];
        $this->db->insert('siswa', $params);
    }
    // ========= END - DATA MASTER - SISWA ======
    
    // ========= USER UPDATE ======
    function edit_admin($where, $table)
    {
        return $this->db->get_where($table, $where);
    }
    // ========= END - USER UPDATE ======

    // PAGINATION 
    function getPage($limit, $start)
    {
        return $this->db->get('guru', $limit, $start)->result();
    }
    function countGuru()
    {
        $query = $this->db->query('SELECT * FROM guru');
        return $query->num_rows();
    }
    // END PAGINATION
    
    // CETAK DATA FILTER BY ID
    function siswaById($id)
    {
        $this->db->select('*');
        $this->db->from('siswa');
        $this->db->join('d_program','d_program.kd_program = siswa.nama_program');
        $this->db->where('siswa.nama_program', $id);
        return $this->db->get()->result();
    }
    // END CETAK FILTER BY ID

    // CONTROLLER NILAI - GURU
    function get_guru_id($id)
    {
		$query = $this->db->get_where('guru', array('id_guru' => $id));
		return $query->row();
    }

    // CETAK DATA NILAI BY TIPE NILAI
    function tipeNilaiById($id)
    {
        $this->db->select('*');
        $this->db->from('nilai');
        $this->db->join('d_mapel','d_mapel.kd_mapel = nilai.kd_mapel');		
        $this->db->join('d_program','d_program.kd_program = nilai.kd_program');
        $this->db->join('guru','guru.id_guru = nilai.id_guru');
        $this->db->join('siswa','siswa.nis = nilai.nis');
        $this->db->where('nilai.tipe', $id);
        return $this->db->get()->result();
    }
}   