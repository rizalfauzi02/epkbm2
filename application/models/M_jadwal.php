<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_jadwal extends CI_Model
{
    public function add($post)
    {
        $params = [
            'hari'  => $post['hari'],
            'jam_awl'  => $post['jam_awl'],
            'jam_akhr'  => $post['jam_akhr'],
            'nama_mapel'  => $post['nama_mapel'],
            'nama_ruangan'  => $post['nama_ruangan'],
            'nama_guru'  => $post['nama_guru'],
            'nama_program'  => $post['nama_program']
        ];
        $this->db->insert('jadwal', $params);
    }
    // INNER JOIN
    function tampilJadwal($id)
    {
        // $this->db->order_by('hari', 'DESC');
        // return $this->db->from('jadwal')
        //   ->join('d_mapel', 'd_mapel.kd_mapel = jadwal.nama_mapel')->get()->result();
        //   ->join('d_ruangan', 'd_ruangan.kd_ruangan = jadwal.nama_ruangan')->get()->result();
        $this->db->select('*');
        $this->db->from('jadwal');
        $this->db->join('d_mapel','d_mapel.kd_mapel = jadwal.nama_mapel');		
        $this->db->join('d_ruangan','d_ruangan.kd_ruangan = jadwal.nama_ruangan');
        $this->db->join('d_program','d_program.kd_program = jadwal.nama_program');
        $this->db->join('guru','guru.id_guru = jadwal.nama_guru');
        $this->db->where('jadwal.nama_program', $id);
        $query = $this->db->get()->result();
        return $query;
    }

    public function tampilJadwalNoId()
    {
        // $this->db->order_by('hari', 'DESC');
        // return $this->db->from('jadwal')
        //   ->join('d_mapel', 'd_mapel.kd_mapel = jadwal.nama_mapel')->get()->result();
        //   ->join('d_ruangan', 'd_ruangan.kd_ruangan = jadwal.nama_ruangan')->get()->result();
        $this->db->select('*');
        $this->db->from('jadwal');
        $this->db->join('d_mapel','d_mapel.kd_mapel = jadwal.nama_mapel');		
        $this->db->join('d_ruangan','d_ruangan.kd_ruangan = jadwal.nama_ruangan');
        $this->db->join('d_program','d_program.kd_program = jadwal.nama_program');
        $this->db->join('guru','guru.id_guru = jadwal.nama_guru');
        $query = $this->db->get()->result();
        return $query;
    }
}