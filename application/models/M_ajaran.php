<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_ajaran extends CI_Model
{
    function get()
    {
        $data = $this->db->query("SELECT * FROM d_ajaran");
		return $data->result();
    }
    // insert
    function insertAjaran($data){
		$this->db->insert('d_ajaran', $data);
	}
    // Delete
    function dajaran($kd_ajaran){
        $this->db->where('kd_ajaran',$kd_ajaran);
        $this->db->delete('d_ajaran');
        return true;
    }
    // CETAK DATA ABSENSI BY AJARAN
    function ajaranById($id)
    {
        $this->db->select('*');
        $this->db->from('attendance');
        $this->db->join('d_mapel','d_mapel.kd_mapel = attendance.nama_mapel');		
        $this->db->join('d_ajaran','d_ajaran.kd_ajaran = attendance.tahun_ajaran');
        $this->db->join('siswa','siswa.id_siswa = attendance.empid');
        $this->db->where('attendance.tahun_ajaran', $id);
        return $this->db->get()->result();
    }
}