<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_mapel extends CI_Model
{
    function delete($kd_mapel){
        $this->db->where('kd_mapel',$kd_mapel);
        $this->db->delete('d_mapel');
        return true;
    }
    // insert
    function insertMapel($data){
		$this->db->insert('d_mapel', $data);
	}

    function get()
    {
        $data = $this->db->query("SELECT * FROM d_mapel");
		return $data->result_array();
    }
    function get_mapel_id($id)
    {
		$query = $this->db->get_where('d_mapel', array('kd_mapel' => $id));
		return $query->row();
    }
    // CETAK DATA ABSENSI BY MAPEL
    function mapelById($id)
    {
        $this->db->select('*');
        $this->db->from('attendance');
        $this->db->join('d_mapel','d_mapel.kd_mapel = attendance.nama_mapel');		
        $this->db->join('d_ajaran','d_ajaran.kd_ajaran = attendance.tahun_ajaran');
        $this->db->join('siswa','siswa.id_siswa = attendance.empid');
        $this->db->where('attendance.nama_mapel', $id);
        return $this->db->get()->result();
    }
    // CETAK DATA NILAI BY MAPEL
    function mapelNilaiById($id)
    {
        $this->db->select('*');
        $this->db->from('nilai');
        $this->db->join('d_mapel','d_mapel.kd_mapel = nilai.kd_mapel');		
        $this->db->join('d_program','d_program.kd_program = nilai.kd_program');
        $this->db->join('guru','guru.id_guru = nilai.id_guru');
        $this->db->join('siswa','siswa.nis = nilai.nis');
        $this->db->where('nilai.kd_mapel', $id);
        return $this->db->get()->result();
    }
}