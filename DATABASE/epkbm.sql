-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 24 Jul 2021 pada 07.34
-- Versi server: 10.4.19-MariaDB
-- Versi PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `epkbm`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `attendance`
--

CREATE TABLE `attendance` (
  `id_absensi` int(11) NOT NULL,
  `tahun_ajaran` varchar(5) NOT NULL,
  `nama_mapel` varchar(50) NOT NULL,
  `empid` int(11) NOT NULL,
  `attdate` varchar(10) NOT NULL,
  `attstatus` varchar(1) NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `attendance`
--

INSERT INTO `attendance` (`id_absensi`, `tahun_ajaran`, `nama_mapel`, `empid`, `attdate`, `attstatus`, `createdate`) VALUES
(4, 'THN21', 'IND01', 16, '2021-07-02', 'A', '2021-07-15 06:41:32'),
(3, 'THN21', 'ING04', 16, '2021-07-01', 'P', '2021-07-15 06:37:23'),
(5, 'THN21', 'ING04', 18, '2021-07-01', 'P', '2021-07-15 06:58:50'),
(8, 'THN21', 'ING04', 19, '2021-07-02', 'P', '2021-07-15 07:04:35'),
(9, 'THN21', 'IND02', 19, '2021-07-05', 'P', '2021-07-15 07:24:47'),
(11, 'THN19', 'IND01', 19, '2021-07-07', 'A', '2021-07-15 07:25:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `d_ajaran`
--

CREATE TABLE `d_ajaran` (
  `kd_ajaran` varchar(5) NOT NULL,
  `tahun_ajaran` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `d_ajaran`
--

INSERT INTO `d_ajaran` (`kd_ajaran`, `tahun_ajaran`) VALUES
('THN19', 2019),
('THN20', 2020),
('THN21', 2021);

-- --------------------------------------------------------

--
-- Struktur dari tabel `d_mapel`
--

CREATE TABLE `d_mapel` (
  `kd_mapel` varchar(5) NOT NULL,
  `nama_mapel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `d_mapel`
--

INSERT INTO `d_mapel` (`kd_mapel`, `nama_mapel`) VALUES
('IND01', 'Bahasa Indonesia'),
('IND02', 'Bahasa Indonesi 2'),
('ING04', 'Bahasa Inggris');

-- --------------------------------------------------------

--
-- Struktur dari tabel `d_program`
--

CREATE TABLE `d_program` (
  `kd_program` varchar(6) NOT NULL,
  `nama_program` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `d_program`
--

INSERT INTO `d_program` (`kd_program`, `nama_program`) VALUES
('PKTA01', 'PAKET A (SD)'),
('PKTB01', 'PAKET B (SMP)'),
('PKTC01', 'PAKET C (SMA)');

-- --------------------------------------------------------

--
-- Struktur dari tabel `d_ruangan`
--

CREATE TABLE `d_ruangan` (
  `kd_ruangan` varchar(5) NOT NULL,
  `nama_ruangan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `d_ruangan`
--

INSERT INTO `d_ruangan` (`kd_ruangan`, `nama_ruangan`) VALUES
('PKTB1', 'Ruangan Kelas Paket A - 1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(250) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `dob` varchar(10) NOT NULL,
  `doj` varchar(10) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '0' COMMENT '1 - Active, 0 - Not Active',
  `createdate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `employees`
--

INSERT INTO `employees` (`id`, `name`, `address`, `email`, `mobile`, `dob`, `doj`, `status`, `createdate`) VALUES
(9, 'Christine', 'Kelley Road', 'moorechrist@gmail.com', '3545789650', '1990-12-02', '2020-12-04', '1', '2021-04-18 16:19:07'),
(10, 'Bruno Den', '1363  Conaway Street', 'brunoden@gmail.com', '3245854500', '1995-03-13', '2021-04-01', '1', '2021-04-18 17:42:58'),
(11, 'Melissa Price', '2459  Rivendell Drive', 'melissa@gmail.com', '4789654120', '1989-03-13', '2020-04-01', '1', '2021-04-18 18:52:38'),
(12, 'John Ambrose', '3189  Waterview Lane', 'ambrosejo@gmail.com', '4973124579', '1995-12-12', '2018-02-13', '0', '2021-04-18 18:53:27'),
(13, 'Donald Davis', '3461  Sycamore Street', 'davis696dol@gmail.com', '4236785550', '1985-03-23', '2019-10-11', '1', '2021-04-18 18:54:19'),
(14, 'Steven Jarrell', '2864  Golden Street', 'stevenjarell@gmail.com', '6547896320', '1986-03-12', '2017-12-12', '0', '2021-04-18 18:55:06'),
(15, 'Katherine Mercado', '703  Eagle Street', 'katherinem@gmail.com', '3457854440', '1992-05-25', '2020-04-16', '1', '2021-04-18 18:57:26'),
(16, 'A Rizal', '1044  Godfrey Road', 'andre0w@gmail.com', '4785552269', '1989-03-23', '2021-01-03', '1', '2021-04-18 18:58:09'),
(17, 'Lewis Villa', '734  Fire Access Road', 'lewisvill@gmail.com', '8520258523', '1995-06-16', '2021-04-02', '1', '2021-04-18 18:58:53'),
(18, 'John Vargas', '1090  Barnes Street', 'vargasjohn@gmail.com', '6872214580', '1996-09-29', '2021-02-15', '1', '2021-04-18 18:59:32'),
(19, 'John Sosa', '3024  Goff Avenue', 'sosjohn66@gmail.com', '7531598500', '1985-01-01', '2018-12-03', '1', '2021-04-18 19:00:18'),
(22, 'RIZAL FAUZI', 'Perum Gading Elok 1', 'rizalfauzi1551@gmail.com', '4860001249', '2001-03-02', '2021-07-12', '1', '2021-07-12 05:14:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(11) UNSIGNED NOT NULL,
  `nuptk` char(11) NOT NULL,
  `nama_guru` varchar(100) NOT NULL,
  `jk_guru` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`id_guru`, `nuptk`, `nama_guru`, `jk_guru`) VALUES
(4, '12345678942', 'Prof. Trent Wyman DDS', 'Perempuan'),
(5, '12345678950', 'Darian Yosudiarso', 'Laki-Laki'),
(7, '12345678911', 'Miss Prudence Friesen', 'Perempuan'),
(8, '12345678930', 'Hanna Schmidt', 'Laki-Laki'),
(9, '12345678912', 'Constance Moen', 'Perempuan'),
(10, '12345678935', 'Cali Towne', 'Laki-Laki'),
(11, '12345678927', 'Walker VonRueden', 'Perempuan'),
(12, '12345678944', 'Vivianne Cole', 'Laki-Laki'),
(13, '12345678903', 'Julian Ondricka', 'Laki-Laki'),
(14, '12345678915', 'Mr. Rhiannon Corwin', 'Perempuan'),
(15, '12345678946', 'Lonzo Nitzsche', 'Laki-Laki'),
(16, '12345678939', 'Mr. Gino Spencer', 'Laki-Laki'),
(17, '12345678916', 'Cordelia Zulauf', 'Perempuan'),
(18, '12345678908', 'Zena Simonis', 'Laki-Laki'),
(19, '12345678915', 'Kirsten Goyette', 'Laki-Laki'),
(20, '12345678923', 'Ernie Marquardt', 'Laki-Laki'),
(21, '12345678919', 'Ansley Harvey', 'Perempuan'),
(22, '12345678930', 'Devyn Roob', 'Mrs.'),
(23, '12345678945', 'Bridget Turner', 'Mrs.'),
(24, '12345678901', 'Mattie Fisher', 'Mr.'),
(25, '12345678927', 'Robert Predovic', 'Prof.'),
(26, '12345678906', 'Watson Koss II', 'Dr.'),
(27, '12345678926', 'Adelia Hammes', 'Miss'),
(28, '12345678911', 'Peggie Hansen', 'Prof.'),
(29, '12345678928', 'Bria Bergstrom DVM', 'Prof.'),
(30, '12345678902', 'Prof. Jensen Kuvalis', 'Mrs.'),
(31, '12345678931', 'Prof. Stephan Hackett', 'Miss'),
(32, '12345678944', 'Frederik Harber', 'Miss'),
(33, '12345678921', 'Hazel Gerhold', 'Dr.'),
(34, '12345678929', 'Vidal Windler', 'Dr.'),
(35, '12345678924', 'Lorenza Marvin', 'Prof.'),
(36, '12345678926', 'Francesco Kub', 'Dr.'),
(37, '12345678933', 'Burdette Lind PhD', 'Dr.'),
(38, '12345678948', 'Norbert Bradtke', 'Mrs.'),
(39, '12345678907', 'Aidan Nienow MD', 'Prof.'),
(40, '12345678904', 'Eda Runolfsdottir', 'Mr.'),
(41, '12345678939', 'Genevieve Hilll I', 'Miss'),
(42, '12345678907', 'Rudolph Nader', 'Mr.'),
(43, '12345678907', 'Keshaun Weimann', 'Prof.'),
(44, '12345678943', 'Prof. Isadore Padberg', 'Mrs.'),
(45, '12345678924', 'Wilfredo Beer DDS', 'Mr.'),
(46, '12345678923', 'Ricky Spinka Jr.', 'Ms.'),
(47, '12345678936', 'Dr. Tamara Gaylord', 'Prof.'),
(48, '12345678933', 'Ms. Zoey Moore', 'Dr.'),
(49, '12345678917', 'Neha Botsford', 'Dr.'),
(50, '12345678921', 'Brittany Nader', 'Mrs.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `hari` varchar(15) NOT NULL,
  `jam_awl` char(50) NOT NULL,
  `jam_akhr` char(50) NOT NULL,
  `nama_mapel` varchar(50) NOT NULL,
  `nama_ruangan` varchar(50) NOT NULL,
  `nama_guru` varchar(100) NOT NULL,
  `nama_program` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `hari`, `jam_awl`, `jam_akhr`, `nama_mapel`, `nama_ruangan`, `nama_guru`, `nama_program`) VALUES
(13, 'Senin', '08:00', '12:00', 'ING04', 'PKTB1', '19', 'PKTA01'),
(15, 'Senin', '08:00', '12:00', 'IND02', 'PKTB1', '18', 'PKTC01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai`
--

CREATE TABLE `nilai` (
  `id_nilai` int(11) NOT NULL,
  `nis` char(11) NOT NULL,
  `kd_mapel` varchar(5) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `kd_program` varchar(6) NOT NULL,
  `tipe` varchar(15) NOT NULL,
  `nilai` char(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `nilai`
--

INSERT INTO `nilai` (`id_nilai`, `nis`, `kd_mapel`, `id_guru`, `kd_program`, `tipe`, `nilai`) VALUES
(1, '12121212121', 'IND01', 4, 'PKTA01', 'UTS', '100'),
(2, '13131313131', 'IND01', 4, 'PKTA01', 'UTS', '80'),
(3, '14141414141', 'IND01', 4, 'PKTA01', 'UTS', '90'),
(6, '12121212121', 'ING04', 4, 'PKTA01', 'UAS', '85'),
(7, '13131313131', 'ING04', 4, 'PKTA01', 'UAS', '80'),
(8, '14141414141', 'ING04', 4, 'PKTA01', 'UAS', '90');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendaftaran`
--

CREATE TABLE `pendaftaran` (
  `id_pendaftaran` int(11) NOT NULL,
  `namalengkap` varchar(100) NOT NULL,
  `nik` char(16) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jk` varchar(11) NOT NULL,
  `kewarganegaraan` char(3) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_telp` char(14) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `kode_pos` char(5) NOT NULL,
  `provinsi` varchar(20) NOT NULL,
  `kabupaten` varchar(20) NOT NULL,
  `kecamatan` varchar(20) NOT NULL,
  `nama_program` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pendaftaran`
--

INSERT INTO `pendaftaran` (`id_pendaftaran`, `namalengkap`, `nik`, `tempat_lahir`, `tanggal_lahir`, `jk`, `kewarganegaraan`, `agama`, `email`, `no_telp`, `alamat`, `kode_pos`, `provinsi`, `kabupaten`, `kecamatan`, `nama_program`, `created_at`, `updated_at`) VALUES
(30, 'RIZAL FAUZI', '3215260203010004', 'KARAWANG', '2001-03-02', 'Laki-Laki', 'WNI', 'Hindu', 'rizalfauzi1551@gmail.com', '089664091196', 'Perum Gading Elok 1', '41314', 'JAWA BARAT', 'KAB. KARAWANG', 'Karawang Timur', 'PKTA01', '2021-07-03 03:40:36', '2021-07-03 03:40:36'),
(31, 'Indri Taulany', '3215260203010008', 'Subang', '2021-07-12', 'Perempuan', 'WNI', 'Kristen', 'sucialifba@gmail.com', '089664097656', 'Perum Gading Elok 1', '41314', 'Jawa Barat', 'KAB. KARAWANG', 'Karawang Timur', 'PKTC01', '2021-07-03 04:29:34', '2021-07-03 04:29:34'),
(32, 'Abdus Somad', '3215260203010001', 'Aceh', '2021-06-30', 'Laki-Laki', 'WNA', 'Katolik', 'random01@gmail.com', '089664090001', 'ACEH BARAT', '41314', 'BANDA ACEH', 'KAB. ACEH', 'Aceh Aja', 'PKTB01', '2021-07-03 21:04:32', '2021-07-03 21:04:32'),
(33, 'Agus Yogi', '3215260203010002', 'Banda Aceh', '2021-03-10', 'Perempuan', 'WNI', 'Budha', 'random02@gmail.com', '089664090002', 'ACEH MANA AJA', '41314', 'BANDA ACEH', 'KAB. ACEH', 'Aceh Aja', 'PKTA01', '2021-07-03 21:05:27', '2021-07-03 21:05:27'),
(34, 'Andre Hadyansyah', '3215260203010005', 'Banda Aceh', '2021-07-01', 'Laki-Laki', 'WNI', 'Budha', 'random04@gmail.com', '089664090004', 'Perum Gading Elok 1', '41314', 'JAWA BARAT', 'KAB. KARAWANG', 'subang jaya', 'PKTA01', '2021-07-03 21:23:12', '2021-07-03 21:23:12'),
(35, 'Asih Yuliyani', '3215260203010003', 'KARAWANG', '2021-06-09', 'Perempuan', 'WNI', 'Hindu', 'random03@gmail.com', '089664090003', 'Perum Gading Elok 1', '41314', 'JAWA BARAT', 'KAB. KARAWANG', 'Bandung Barat', 'PKTA01', '2021-07-03 21:24:25', '2021-07-03 21:24:25'),
(36, 'Debi Mulyani', '3215260203010006', 'Subang', '2021-05-11', 'Perempuan', 'WNA', 'Kristen', 'random05@gmail.com', '089664090005', 'Perum Gading Elok 1', '41314', 'JAWA BARAT', 'KAB. KARAWANG', 'Bandung Barat', 'PKTA01', '2021-07-03 21:27:02', '2021-07-03 21:27:02'),
(37, 'Febriyanti Halimah', '3215260203010007', 'Subang', '2021-05-12', 'Perempuan', 'WNI', 'Katolik', 'random06@gmail.com', '089664090006', 'Perum Gading Elok 1', '41314', 'JAWA BARAT', 'KAB. KARAWANG', 'subang jaya', 'PKTA01', '2021-07-03 21:28:37', '2021-07-03 21:28:37'),
(38, 'Feby Elsa Mutiara', '3215260203010009', 'KARAWANG', '2021-06-30', 'Perempuan', 'WNI', 'Kristen', 'random07@gmail.com', '089664090007', 'Perum Gading Elok 1', '41314', 'JAWA BARAT', 'KAB. KARAWANG', 'Bandung Barat', 'PKTA01', '2021-07-03 21:33:02', '2021-07-03 21:33:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `nis` char(11) NOT NULL,
  `nik_siswa` char(16) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `lahir_siswa` varchar(50) NOT NULL,
  `tanggal_siswa` date NOT NULL,
  `jk_siswa` varchar(11) NOT NULL,
  `nama_program` varchar(50) NOT NULL,
  `kd_ajaran` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `nis`, `nik_siswa`, `nama_siswa`, `lahir_siswa`, `tanggal_siswa`, `jk_siswa`, `nama_program`, `kd_ajaran`) VALUES
(14, '98989898988', '3215260203010008', 'Indri Taulany', 'Subang', '2021-07-12', 'Perempuan', 'PKTC01', '0'),
(16, '12121212121', '3215260203010004', 'RIZAL FAUZI', 'KARAWANG', '2001-03-02', 'Laki-Laki', 'PKTA01', '0'),
(18, '13131313131', '3215260203010002', 'Agus Yogi', 'Banda Aceh', '2021-03-10', 'Perempuan', 'PKTA01', '0'),
(19, '14141414141', '3215260203010005', 'Andre Hadyansyah', 'Banda Aceh', '2021-07-01', 'Laki-Laki', 'PKTA01', 'THN21'),
(20, '09090909099', '3215260203010001', 'Abdus Somad', 'Aceh', '2021-06-30', 'Laki-Laki', 'PKTB01', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_users` int(11) NOT NULL,
  `namalengkap` varchar(128) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `akses` int(11) DEFAULT NULL,
  `isActive` int(5) NOT NULL,
  `nama_program` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_users`, `namalengkap`, `username`, `password`, `email`, `gambar`, `akses`, `isActive`, `nama_program`, `created_at`, `updated_at`) VALUES
(21, 'RIZAL FAUZI', 'admin', '$2y$10$SJYSV..CktMjj8Dc8/vqruqc06gwmb2r9TT0D91JNcyCeZIYF1sIm', 'rizalfauzi1551@gmail.com', 'zoro-chubi.jpg', 1, 1, '', '2021-06-11 00:16:49', '2021-07-07 20:53:18'),
(22, 'SISWA RIZAL', 'siswa', '$2y$10$aWfaVfgSqEM1ZJ8aLrDzr.UA191hXn3VpPAaaHdWdZKF3ZYi2nM9O', 'testing6@test.com', 'logo_R.jpg', 2, 1, '', '2021-06-11 01:37:09', '2021-06-23 03:50:38'),
(29, 'Alfian Ibrahim', 'alfian', '$2y$10$CStuA0fs0eBb.QgRVqf3fOSW/C0nzgx0J5eb/dv68O3scDape2FeO', 'alfian15@gmail.com', 'default.jpg', 1, 1, '', '2021-06-18 10:02:53', '2021-07-01 15:07:17'),
(36, 'RIZAL FAUZI YUSUF', 'rizal', '$2y$10$spMI35KEPa5Qb9rv2EFIkOak7JehumW.YbEeENzQA6yht5dBhtv5u', 'rizalfauzi1551@gmail.com', 'default.jpg', 2, 1, '', '2021-07-01 15:40:45', NULL),
(39, 'Luthfi Ihsan', 'luthfi', '$2y$10$yOpMj3DBmoEk7WeSM5bIOO2HTCO5OS8HP2KlVhi1jeaZN9Hr0yswu', 'luthfi@gmail.com', 'default.jpg', 1, 1, '', '2021-07-06 21:05:39', '2021-07-06 21:11:25'),
(44, 'Prof. Jensen Kuvalis', 'jensen', '$2y$10$JRWx5T2Jy0F6oqhcpgrCe.QB11tmD5/K2zMU/seDMzHi/AbfN18Gm', 'testing1@test.com', 'default.jpg', 3, 1, 'PKTA01', '2021-07-10 01:17:07', NULL),
(45, 'Mattie Fisher', 'mattie', '$2y$10$6wIjGAv80.jSStvd0JMdBuRmpL41mQt4MQ4iXkDBVeGy508wDWeGq', 'mattiefisher@gmail.com', 'default.jpg', 3, 1, 'PKTB01', '2021-07-10 01:31:32', '2021-07-10 01:48:08');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id_absensi`);

--
-- Indeks untuk tabel `d_ajaran`
--
ALTER TABLE `d_ajaran`
  ADD PRIMARY KEY (`kd_ajaran`);

--
-- Indeks untuk tabel `d_mapel`
--
ALTER TABLE `d_mapel`
  ADD PRIMARY KEY (`kd_mapel`);

--
-- Indeks untuk tabel `d_program`
--
ALTER TABLE `d_program`
  ADD PRIMARY KEY (`kd_program`);

--
-- Indeks untuk tabel `d_ruangan`
--
ALTER TABLE `d_ruangan`
  ADD PRIMARY KEY (`kd_ruangan`);

--
-- Indeks untuk tabel `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indeks untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indeks untuk tabel `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indeks untuk tabel `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD PRIMARY KEY (`id_pendaftaran`);

--
-- Indeks untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_users`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id_absensi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `guru`
--
ALTER TABLE `guru`
  MODIFY `id_guru` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `pendaftaran`
--
ALTER TABLE `pendaftaran`
  MODIFY `id_pendaftaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
